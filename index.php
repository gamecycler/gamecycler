<?php include "base.php" ?>

<?php
$title = "Gamecycler | Trade Games With Other Gamers";
$description = "List your gaming wants and haves, match with other users, and trade games for $1";
$keywords = "gamers,games,used games,playstation,xbox,nintendo,sega,classic games,game collections";

?>

<?php include "header.php" ?>

<style>
	#hedmen { display: none; }
</style>

<body id="index">

<div class="content">

<div class="container" id="contind">
	<div class="row" id="controw">
		<div class="col-sm-12">
			<section class="module parallax parallax-1">
				<div class="container-parallax">
					<div class="col-sm-6">

						<div id="head-img"><img width="400px" src="images/gamecycler-light.gif" /><br /></div>
						<div id="head-desc1">TRADE GAMES<br />WITH OTHER GAMERS</div>
						<div id="head-desc2">-- don't throw money away at retail stores,<br />match up with other gamers, and be part of a community</div>
						<a href="login.php"><div id="login-box">login ></div></a><br />
						<div id="registerindex">no account? <a href="register.php">register here</a></div>

					</div> <!-- col-sm-6 left side info -->
					<div class="col-sm-6" align="right">

						<div id="mini-head"><ul><li><a href="users.php">users</a></li><li><a href="games.php">games</a></li><li><a href="about.php">about</a></li></ul></div>

					</div> <!-- col-sm-6 mini-header -->
				</div> <!-- container-parallax -->
			</section>
			<div class="col-sm-12" id="threebox">
				<div class="row">
					<div class="col-sm-4" style="padding:30px;">
						<div class="row">
							<div class="col-sm-12 threeboxtile">
								<div class="row">
									<div class="col-sm-6 text-center">
										<i class="fa fa-server fa-4x"></i><br />
										
									</div>
									<div class="col-sm-6 text-center">
										<i class="fa fa-tasks fa-4x"></i><br />
										
									</div>
									<div class="col-sm-12 text-center" style="margin-top:5px">
										<strong>FIND THE GAMES YOU WANT, LIST THE GAMES YOU HAVE</strong>
									</div>

									<div class="col-sm-12 text-center" style="margin-top: 12px;">
										<p>List your games for other gamers to find, and find games that other gamers have listed.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4" style="padding: 30px;">
						<div class="row">
							<div class="col-sm-12 threeboxtile2">
								<div class="row">
									<div class="col-sm-4 text-right">
										<i class="fa fa-users fa-3x"></i>
										
									</div>
									<div class="col-sm-4 text-center">
										<i class="fa fa-recycle fa-3x"></i>
										
									</div>
									<div class="col-sm-4 text-left">
										<i class="fa fa-gamepad fa-3x"></i>
										
									</div>
									<div class="col-sm-12 text-center" style="margin-top:15px">
										<strong>TRADE GAMES WITH OTHER USERS</strong>
									</div>

									<div class="col-sm-12 text-center" style="margin-top: 15px;">
										<p>Match up with other users, agree to which games to trade. Maybe even make some money.</p>
									</div>
								</div>
							</div>
						</div>

					</div>
					<div class="col-sm-4" style="padding: 30px;">
						<div class="row">
							<div class="col-sm-12 threeboxtile">
								<div class="row">
									<div class="col-sm-4 text-center">
										<i class="fa fa-user fa-4x"></i>
										
									</div>
									<div class="col-sm-4 text-center">
										<i class="fa fa-exchange fa-4x"></i>
										
									</div>
									<div class="col-sm-4 text-center">
										<i class="fa fa-user fa-4x"></i>
										
									</div>
									
									<div class="col-sm-12 text-center" style="margin-top: 5px;">
										<strong>SEND GAMES</strong>
									</div>

									<div class="col-sm-12 text-center" style="margin-top: 10px;">
										<p>After agreeing, send each other the games you've agreed to trade.</p>
									</div>
								</div>
							</div>
						</div>

					</div>


				</div> <!-- row -->
			</div> <!-- col-sm-12 threebox -->
			<div class="col-sm-11">
				<div class="row">
					<div class="col-sm-6 col-sm-offset-1 text-left" id="index-body1">				
						<h3>What Is Gamecycler?</h3>
						<p>Gamecycler is a website that was created to make gamer's lives <em>a whole lot <u>cheaper</u></em>.</p>
						<p>You know what I’m talking about. You go into your local game store to sell back your games, hoping to spend what you get back on the latest Fallout or Madden Football, and maybe even make some extra cash.</p>
						<p>You turn in your games and they hand you back a <em><strong>few measly dollars</strong></em>, even though you just paid $60 a month ago.</p>
						<p>It’s not your fault you stayed up every night beating level after level!</p>
						
						<h3>How Gamecycler Works?</h3>
						<p>Imagine finding a community of people who play games with the same <u>passion</u> you do.</p>
						<p>You tear through games like Yoshi farts out eggs, and after they're done you <em>can't wait to play more</em>.</p>
						<p>Gamecycler introduces you to other users who may, or may not, <strong>have the games that you want</strong>, while also letting them know that you <strong>have some of the games that they want</strong>.</p>
						<p>You make an offer, they accept, and you swap your games <small>(while maybe paying each other the difference of their values)</small>.</p>
						<p><strike><strong>For 1 DOLLAR!</strong>!</strike></p>
						<p>Since Gamecycler is in <span style="background: yellow; padding: 5px;">BETA</span> we're waiving our fees. First, to make Gamecycler work we need as many gamers as we can find. Plus, we need your help to work out all the kinks. Consider it our thanks for helping get Gamecycler up and running.</p>
					</div> <!-- text section -->
					<div class="col-sm-4 text-center" id="ingamesimg">
						<img style="width: 85%!important;" src="images/marbros.jpg">
						
						<img src="images/Kid_Icarus_NES_box_art.png">
						<img src="images/madden.jpeg">
						<img style="width: 85%!important;" src="images/gow2.jpg">
						<!-- <img src="images/civilization.jpg"> -->
						<img src="images/gt4.jpg">
						<img src="images/SonicTheHedgehog1.png">
						
						<img style="width: 85%!important;" src="images/fallout3.jpg">
					</div>
				</div>
			</div>


			<!-- <div class="col-sm-10 col-sm-offset-1" id="step">
				<p><u>Here's a pretty simple outline of how you'll be trading games on Gamecycler in no time:</u></p>
				<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 1:</h3>
					<p>Sign Up</p>
				</div> <!-- step 1 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 2:</h3>
					<p>List The Games You Want &amp; The Games You Have</p>
				</div> <!-- step 2 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 3:</h3>
					<p>Gamecycler Tells You With Other Users Have The Games You Want, Plus Want he Games You Have</p>
				</div> <!-- step 3 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 4:</h3>
					<p>Pick Which Other Gamer You'd Like To Trade With</p>
				</div> <!-- step 4 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 5:</h3>
					<p>Once They Confirm, Then You'll Receive a Notification</p>
				</div> <!-- step 5 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 6:</h3>
					<p>Pay the $2 Transaction Fee</p>
				</div> <!-- step 6 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 7:</h3>
					<p>Each Gamer Will Receive The Other's Mailing Address</p>
				</div> <!-- step 7 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 8:</h3>
					<p>Ship Out Your Games</p>
				</div> <!-- step 8 -->
			<!--	<div class="col-sm-4">
					<img src="images/step.jpg"><br />
					<h3>Step 9:</h3>
					<p>Open Your Games Up, Thank Your Lucky Stars That Gamecycler Exists, &amp; Spend The Next 12 Hours Getting Work <strong>DONE</strong>.</p>
				</div> <!-- step 9 -->
			</div> <!-- steps section -->
		</div> <!-- parallax img section -->
		</div> <!-- blue bar header -->
	</div> <!-- row -->
</div> <!-- container -->









<?php /*
//If Logged In
if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username']))
{
     ?>
		
		<h1>Thank you for logging in!</h2>
		<!-- <meta http-equiv="refresh" content="0;home.php">-->

	  <?php
}
// If Username and Password entered isn't empty
elseif(!empty($_POST['username']) && !empty($_POST['password']))
{
	$username = mysqli_real_escape_string($link, $_POST['username']);
    $password = mysqli_real_escape_string($link, $_POST['password']);
    
	// $mysqli = new mysqli("localhost", "vgseadmin", "cFemaXmSLtRrHtUf", "vgse2");
	// if ($mysqli->connect_errno) {
	//	echo "Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error;
	// }
	$dbh = new PDO('mysql:host=localhost;dbname=vgse2', 'vgseadmin', 'cFemaXmSLtRrHtUf');
	
	$sth = $dbh->prepare('SELECT hash FROM db_users WHERE username=:username LIMIT 1');
	
	$sth->bindParam(':username', $username);

	$sth->execute();
	
	$user = $sth->fetch(PDO::FETCH_OBJ);

	// Hashing the password with its hash as the salt returns the same hash
	if ( hash_equals($user->hash, crypt($password,$user->hash)) ) {
	  // Ok!
	
	echo "Username: " . $username;
	echo "Password: " . $password;
	
    $checklogin = mysqli_query($link, "SELECT * FROM db_users WHERE Username = '".$username."' AND Password = '".$password."'");
     
		if(mysqli_num_rows($checklogin) == 1)
		{
			$row = mysqli_fetch_array($checklogin);
			$email = $row['email'];
			$user_id = $row['user_id'];
			
			 
			$_SESSION["Username"] = $username;
			$_SESSION["EmailAddress"] = $email;
			$_SESSION["user_id"] = $user_id;
			$_SESSION["LoggedIn"] = 1;
			
			 
			echo "<h1>Success</h1>";
			echo "<p>We are now redirecting you to the member area.</p>";
			echo '<p>Username: ' . $_POST["username"] . ' Logged In: ' . $_SESSION["LoggedIn"] . '. Email: ' . $_SESSION["EmailAddress"] . '.</p>';
			// echo "<meta http-equiv='refresh' content='2;index.php' />";
		}
		else
		{
			echo "<h1>Error</h1>";
			echo "<p>Username: " . $_POST['username'] . " Password: " . $_POST['password'] . " Logged In: " . $_SESSION['LoggedIn'];
			echo "<p>Sorry, your account could not be found. Please <a href=\"home.php\">click here to try again</a>.</p>";
		}
	}
	else {
		echo "Try again!";
	
	
	}
}
	
// If not logged in, beginning login screen
else
{
    ?>

	<h1>Member Login</h1>
     
   <p>Thanks for visiting! Please either login below, or <a href="register.php">click here to register</a>.</p>
     
    <form method="post" action="login.php" name="loginform" id="loginform">
    <fieldset>
        <label for="username">Username:</label><input type="text" name="username" id="username" /><br />
        <label for="password">Password:</label><input type="password" name="password" id="password" /><br />
        <input type="submit" name="login" id="login" value="Login" />
    </fieldset>
    </form>
	
	
	
	<?php
}
?>
 
</div>

<!--
Hello ?php if(isset($_SESSION['username'])){echo ' '.htmlentities($_SESSION['username'], ENT_QUOTES, 'UTF-8');} ?>,<br />
Welcome on our website.<br />
You can <a href="users.php">see the list of users</a>.<br /><br />
?php
//If the user is logged, we display links to edit his infos, to see his pms and to log out
if(isset($_SESSION['username']))
{
?>
<a href="edit_infos.php">Edit my personnal informations</a><br />
<a href="logout.php">Logout</a>
?php
}
else
{
//Otherwise, we display a link to log in and to Sign up
?>
<a href="register.php">Sign up</a><br />
<a href="login.php">Log in</a>
?php 
} */
?>

 
</div>


<? include 'footer.php'; ?>