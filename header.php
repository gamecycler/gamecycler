<?php

$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

echo '

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta property="og:url" content="'.$actual_link.'" />
<meta property="og:title" content="'.$title.'" />
<meta property="og:description" content="'.$description.'" />
<meta property="og:image" content="http://www.gamecycler.com/gamecyclerfb.gif" />
<meta name="description" content="'.$description.'">
<meta name="keywords" content="'.$keywords.'">

<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>'.$title.'</title>

<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="style.css" rel="stylesheet">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,300,700" rel="stylesheet" type="text/css">

<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="js/modernizr.js"></script>
<script src="pagination/jquery.simplePagination.js"></script>
<script src="pagination/main.js"></script>

<!-- Font-Awesome -->
<link rel="stylesheet" href="css/font-awesome.min.css">
<script src="https://use.fontawesome.com/5ce101fd1f.js"></script>


<!-- CSS for Pagination -->
<link type="text/css" rel="stylesheet" href="pagination/simplePagination.css"/>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn\'t work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Javascript for Reject Click -->


</head>

';

?>

<?php include_once("analyticstracking.php") ?>