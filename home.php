<?php include 'base.php';

$title = "Gamecycler | Welcome to Gamecycler";
$description = "Home screen for Gamecycler Users";
$keywords = "Gamers, trade games, nintendo, xbox, playstation, find games, gaming, community";

include 'header.php'; ?>

<body>

<?php

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

	$user1 = $_SESSION['user_id'];

	$chkoff = mysqli_query($link, "SELECT * FROM game_offer WHERE user1='$user1' and user1accept='' and user2accept='YES'");
	$chkfet = mysqli_fetch_array($chkoff);

	if(count($chkfet) > 0) {
		
		?>
		 <meta http-equiv="refresh" content="0;tradestatus.php">
		<?php

	} else {

	$users = "SELECT user_id, username FROM db_users";
	$usersqy = mysqli_query($link, $users);

	while($usersfet = mysqli_fetch_array($usersqy)) {
		$userid = $usersfet['user_id'];
		$usersarr[$userid] = $usersfet;
	}

	$games = "SELECT game_id, title, console FROM db_games";
	$gamesqy = mysqli_query($link, $games);

	while($gamesfet = mysqli_fetch_array($gamesqy)) {
		$game_id = $gamesfet['game_id'];
		$gamesarr[$game_id] = $gamesfet;
	}

	// STAR RATING USER $a = new starrat1(); then $a->starrat1( -- rating variable from db -- )
	class starrat
		{
			function starrat1($rat)
			{
				if($rat == 1) {
	                echo "<span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 2 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 3 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 4 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 5 ) {   
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } else {
	            	echo "<i>No Rating Yet.</i>";
	            }

			}
		}

	?>
				
			<?php include 'nav.php'; ?>

			
					<div class="col-sm-12" id="welcome">
						<h1>Welcome, <?php echo $usersarr[$user1]['username']; ?>!</h1>
					</div>
					<div class="col-sm-12" id="adminann">
	<?php 
	// ANNOUNCEMENTS SECTION
						$announ = "SELECT * FROM adminannounce";
						$announqy = mysqli_query($link, $announ);

						while($announarr = mysqli_fetch_array($announqy)) {
							if($announarr['showannounce'] == "YES") {
								echo "<h3>Annoucement From Gamecycler:</h3>";
								print_r($announarr['announce']);
							}
						}

						mysqli_free_result($announqy);

	?>
					</div><!-- adminann announcements -->
					<div class="col-sm-8 col-sm-offset-2" id="searchfor">
						<div class="row">
							<div class="col-sm-12 text-center"><h3>Initiate A Trade By:</h3><br /></div>
							<div class="col-sm-4 text-center" id="sr-user">
								<a href="users.php"><i class="fa fa-users fa-3x" aria-hidden="true"></i><br /><div class="text-center">Users</div></a>
							</div><!-- sr-user -->
							<div class="col-sm-4 text-center" id="sr-games">
								<a href="games.php"><i class="fa fa-gamepad fa-3x" aria-hidden="true"></i><br /><div class="text-center">Games</div></a>
							</div><!-- sr-game -->
							<div class="col-sm-4 text-center" id="sr-match">
								<a href="comparegames.php"><i class="fa fa-bolt fa-3x" aria-hidden="true"></i><div class="text-center">Matches</div></a>
							</div><!-- sr-match -->
						</div><!-- row for search icons -->
					</div> <!-- col-sm-10 searchfor -->



						<!-- NEW TRADE -->
						<div class="col-sm-12" id="newtrade">
							<table class="table">
								<tbody>
									<tr class="thead"><td colspan="3">New Trade Offers</td></tr>
									<tr><th>From</th><th>Wants</th><th>Offering</th></tr>
	<?php
	// NEW TRADE SECTION
										$newtrade = "SELECT * FROM game_offer WHERE user2='".$user1."' AND user1accept='' AND user2accept=''";
										$newtradeqy = mysqli_query($link, $newtrade);

										if(mysqli_num_rows($newtradeqy) == 0) {
												echo "<tr><td align='center' colspan='3'>No New Trades</td></tr>";
											} 
											else
											{

											while($newtradearr = mysqli_fetch_array($newtradeqy)) {
											echo "<tr>
													<td>";
														$user2 = $newtradearr['user1'];
														print_r('<a href="profile.php?id='.$user2.'">'.$usersarr[$user2]['username'].'</a>');
											echo "	</td><!-- from -->
													<td>";
														$wantexp = explode(',', $newtradearr['want_games']);
														foreach($wantexp as $wantkey => $wantid) {
															print_r($gamesarr[$wantid]['title'].'<br />');
															if(++$a == 5) break;
														}
											echo "	</td><!-- wants -->
													<td>";
														$offexp = explode(',', $newtradearr['offer_games']);
														foreach($offexp as $offkey => $offid) {
															print_r($gamesarr[$offid]['title'].'<br />');
															if(++$b == 5) { break;}
														}
											echo "	</td><!-- offer -->
												</tr>";
												if(++$h == 5) { break; }
											} // while fetch

										} // else

										echo "<tr><td align='right' colspan='3'><a href='tradestatus.php'>Status of All Trades...</a></td></tr>";

										mysqli_free_result($newtradeqy);
	?>
								</tbody> 
							</table> <!-- trade offers section -->	
						</div><!-- col-sm-12 trade offers section -->

						<!-- NEW USERS -->
						<div class="col-sm-6" id="newusers">
							<table class="table">
								<tr class="thead"><td colspan="2">New Users</td></tr>
								<tbody>
									<tr><th>Name</th><th>Games In Their Collection</th></tr>
	<?php
										$newuser = "SELECT have.games, have.user_id, db_users.user_id, db_users.username FROM userhavegames as have, db_users WHERE have.user_id=db_users.user_id ORDER BY db_users.user_id DESC LIMIT 3";
										$newuserqy = mysqli_query($link, $newuser);

										while($newuserfet = mysqli_fetch_array($newuserqy)) {
											echo "	<tr>
														<td>";
															print_r('<a href="profile.php?id='.$newuserfet['user_id'].'">'.$newuserfet['username'].'</a>');
											echo "		</td><!-- name -->
														<td>";
															$libgame = explode(',', $newuserfet['games']);
															$c = 0;
															foreach($libgame as $libkey => $libid) {
																if(++$c < count($libgame)) {
																	print_r($gamesarr[$libid]['title'].', ');
																} elseif ($c == count($libgame)) {
																	print_r($gamesarr[$libid]['title']);
																}
															}
											echo "		</td><!-- collection -->
													</tr>";

										}

										echo "	<tr><td align='right' colspan='2'><a href='users.php'>All Users...</a></td></tr>";

										mysqli_free_result($newuserqy);
	?>
									
								</tbody>
							</table>
						</div><!-- col-sm-6 New Users -->

						<!-- MATCHES -->
						<div class="col-sm-6" id="matches">
							<table class="table">
								<tbody>
									<tr class="thead"><td colspan="2">Matches</td></tr>
									<tr><th>Name</th><th>Games They Have That You Want</th></tr>
	<?php
									$gamesmatch = "SELECT want.user_id as user1, have.user_id as user2, want.games as wantgames, have.games as havegames FROM userwantgames as want, userhavegames as have WHERE want.user_id='".$user1."' AND NOT want.user_id = have.user_id";
									$gamesmatchqy = mysqli_query($link, $gamesmatch);

									if(mysqli_num_rows($gamesmatchqy) == 0) {
										echo "<tr><td align='center' colspan='2'>No Matches</td></tr>";
									}
									else
									{

										while($gamesmatchfet = mysqli_fetch_array($gamesmatchqy)) {
											$u1warr = explode(',',$gamesmatchfet['wantgames']);
											$u2harr = explode(',',$gamesmatchfet['havegames']);
											$u1wu2hcomp = array_intersect($u1warr, $u2harr);

											if($u1wu2hcomp != NULL) {
												$matchgames[] = array(
														"user1" => $user1,
														"user2" => $gamesmatchfet['user2'],
														"matches" => $u1wu2hcomp,
														"count" => count($u1wu2hcomp)
													);
											} // if u1wu2hcomp null

										} // while gamesmatchfet

										if(count($matchgames) == 0) {
										echo '<tr><td align="center" colspan="2">No Matches</td></tr>';
										} else {

										foreach($matchgames as $key => $row) {
											$count[$key] = $row['count'];
										}

										array_multisort($count, SORT_DESC, $matchgames);

										foreach($matchgames as $matkey => $matgmid) {
											echo "	<tr>
														<td>";
															$user2 = $matgmid['user2'];
															print_r('<a href="profile.php?id='.$user2.'">'.$usersarr[$user2]['username'].'</a>');
											echo "		</td><!-- user2 -->
														<td>";
															$gamematarr = $matgmid['matches'];

															$f = 0;
															foreach($gamematarr as $keygmmat => $gmmatdata) {
																print_r($gamesarr[$gmmatdata]['title'].'<br />');
																if(++$f == 5) { break; }
															}
											echo "		</td><!-- matches -->
													</tr>";

											if(++$g == 5) { break; }
										}

										}

									} // else

									echo "<tr><td align='right' colspan='2'><a href='compareusers-ui.php'>All Matches...</a></td></tr>";

									mysqli_free_result($gamesmatchqy); 
	?>
								</tbody>
							</table>
						</div><!-- Matches -->

						<!-- REVIEWS -->
						<div class="col-sm-12" id="yourreview">
							<div class="thead">Your Reviews</div>
							<div class="col-sm-12" id='overratehm'><strong>Overall Rating:</strong>
	<?php
										$u1review = "
											SELECT r1.reviewer, r1.reviewee, r1.rating, r1.review, r1.timestamp, u1.user_id, u1.email, u1.username 
											FROM userreviews as r1 
											INNER JOIN db_users as u1 
											ON r1.reviewer=u1.user_id
											WHERE r1.reviewee='".$user1."'
											ORDER BY r1.timestamp DESC
											";

										$u1ovrat = "SELECT SUM(rating) as revsum, COUNT(rating) as revct FROM userreviews WHERE reviewee='".$user1."'";

										$u1reviewqy = mysqli_query($link, $u1review);
										$u1ovratqy = mysqli_query($link, $u1ovrat);

										if(mysqli_num_rows($u1ovratqy) == 0) {
											echo "No Rating Yet";
										} else {

											while($ovratfet = mysqli_fetch_array($u1ovratqy)) {
												$revsum = $ovratfet['revsum'];
												$revct = $ovratfet['revct'];

												if($revct == 0) {
													$revtot = 0;
												} else {
													$revtot = round(($revsum/$revct), 0, PHP_ROUND_HALF_UP);
												}

												$rev = new starrat();
												$rev->starrat1($revtot);

											} // while ovratfet
										} // else

										echo '</div>';

										
										if(mysqli_num_rows($u1reviewqy) == 0) {
											echo "No Reviews";
										} else {

											while($u1reviewfet = mysqli_fetch_array($u1reviewqy,MYSQLI_ASSOC)) {

												echo "<div class='row ureviewhome'>
														<div class='col-sm-2 text-center homeprofimg'>";
															$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
															$size1 = 175;	
															$email1 = $u1reviewfet['email'];
															
															$grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
															
															print_r('<img width="100%" src="'. $grav_url1 .'" alt="" />');
															print_r('<br /><strong><a href="profile.php?id='.$u1reviewfet['user_id'].'">'.$u1reviewfet['username'].'</strong></a>');
												echo "	</div><!-- username -->
														<div class='col-sm-10'>
															<div class='row'>
																<div class='col-sm-12 homerevrev'>";

																	print_r('<span style="font-size:2.5em; font-weight: bold;">" </span>'.$u1reviewfet['review'].'<span style="font-size:2.5em; font-weight: bold;"> "</span>');

												echo "			</div>
													  			<div class='col-sm-6 text-center'>";
																  	$reviewrat = $u1reviewfet['rating'];

																	$review = new starrat();
																	$review->starrat1($reviewrat);

												echo "			</div><!-- stars -->
													  		<div class='col-sm-6'>";
													  			print_r(date('m-d-Y',strtotime($u1reviewfet['timestamp'])));

												echo "		</div><!-- date -->
															
														</div>
													</div>
													</div><!-- review -->";

											if(++$j == 5) { break; }
															
											} // while u1reviewfet

										} // else

															

										echo "<div class='col-sm-12 text-right' style='margin-bottom: 30px;'><a href='reviews.php'>All Reviews...</a></div>";

										mysqli_free_result($u1reviewqy);
										mysqli_free_result($u1ovratqy);
	?>
								</div> <!-- Your reviews -->

						<!-- RECENT TRADES -->
						<div class="col-sm-12 col-md-12" id="reviews">
							<table class="table">
								<tbody>
									<tr class="thead"><td colspan="6">Some Recent Trades Need Your Review</td></tr>
									<tr><th>Date</th><th>Traded With</th><th colspan="3">Games Traded</th></tr>
	<?php
										 

										// IF IN GAME_OFFER BUT NOT IN USERREVIEWS

										$gameoff = "SELECT * FROM game_offer WHERE user1accept='YES' AND (user1='".$user1."' OR user2='".$user1."')";
										$userrev = "SELECT * FROM userreviews WHERE reviewer='".$user1."'";
										
										$gameoffqy = mysqli_query($link, $gameoff);
										$userrevqy = mysqli_query($link, $userrev);
										
										while($gameofffet =  mysqli_fetch_array($gameoffqy,MYSQLI_ASSOC)) {
											$gameoffarr[] = $gameofffet;
										}

										while($userrevfet = mysqli_fetch_array($userrevqy,MYSQLI_ASSOC)) {
											$userrevarr[] = $userrevfet;
										}
										
										if(count($gameoffarr) == 0) {

										} else {
										foreach($gameoffarr as $gokey => $godata) {
											foreach($userrevarr as $urkey => $urdata) {
												$godataid = $godata['id'];
												$urdatagoid = $urdata['game_offer_id'];

												if($godataid == $urdatagoid) {
													break;
												} elseif($godataid !== $urdatagoid) {
													
													echo "	<tr>
																<td>";
																	print_r(date('m-d-Y', strtotime($godata['finaldate'])));
													echo "		</td><!-- trade date-->
																<td>";
																	if($user1 == $godata['user1']){
																		$user2 = $godata['user2'];
																		'<a href="profile.php?id='.$user2.'">'.print_r($usersarr[$user2]['username'].'</a>');
																	} else {
																		$user2 = $godata['user1'];
																		'<a href="profile.php?id='.$user2.'">'.print_r($usersarr[$user2]['username'].'</a>');
																	}
													echo "		</td> <!-- trade partner -->
																<td>";
																	$wantgmexp = explode(',',$godata['want_games']);
																	foreach($wantgmexp as $wgkey => $wgid) {
																		print_r($gamesarr[$wgid]['title'].'<br />');
																	}
													echo "		</td> <!-- want games -->
																<td>For</td>
																<td>";
																	$offergmexp = explode(',',$godata['offer_games']);
																	foreach($offergmexp as $ogkey => $ogid) {
																		print_r($gamesarr[$ogid]['title'].'<br />');
																	}
													echo "		</td> <!-- offer games -->
															</tr>";
													
													if(++$k == 5) { break; }

													break;  
												}
											} // foreach games_offer
										} // foreach userreviews
									} // if array isn't 0

										echo "<tr><td colspan='5' align='right'><a href='reviews.php'>Leave Feedback...</a></td></tr>";

										mysqli_free_result($gameoffqy);
										mysqli_free_result($userrevqy);
	?>

								</tbody>
							</table>
						</div><!-- leave reviews -->

				</div><!-- row for info container sub divs -->
			</div><!-- col-sm-9 info container -->

		</div><!-- main row -->
	</div><!-- container -->
<?php
}

}
else
{
	?>
    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
    Please Log in!
    <?php
}

?>

<?php include 'footer.php' ?>