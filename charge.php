<?php include 'base.php';
  require_once('./config.php');

  $amount = $_SESSION['amount'];

  $token  = $_POST['stripeToken'];
  $email = $_POST['stripeEmail'];

  $customer = \Stripe\Customer::create(array(
      'email' => $email,
      'source'  => $token
  ));

  $charge = \Stripe\Charge::create(array(
      'customer' => $customer->id,
      'amount'   => $amount,
      'currency' => 'usd'
  ));

  echo '<meta http-equiv="refresh" content="0;thankyou.php">';
?>