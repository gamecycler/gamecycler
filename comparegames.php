<?php include 'base.php';

$title = "GameCycler | Pick The Person You'd Like To Trade Games With";
$description = "After declaring your library, select the person you'd like to trade games with.";
$keywords = "trading games, gamers, match, nintendo, playstation, xbox, gaming, library, collections";

include 'header.php'; ?>

<body>

<?php

// STAR RATING USER $a = new starrat1(); then $a->starrat1( -- rating variable from db -- )
	class starrat
		{
			function starrat1($rat)
			{
				if($rat == 1) {
	                echo "<span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 2 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 3 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 4 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 5 ) {   
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } else {
	            	echo "";
	            }

			}
		}

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

	if(!empty($_POST)) {

		// Games database
		// Query to pull up all games information from db_games
		$gamesdblist = "SELECT * FROM db_games";
		$gameslist = mysqli_query($link, $gamesdblist);

		// create game refernce table
		while($gamesarray = mysqli_fetch_array($gameslist,MYSQLI_ASSOC)) {
					$gamesarid = $gamesarray['game_id'];
					$gamestable[$gamesarid] = $gamesarray; // place entire gamewantarray into a numbered array to reference individual results
				} // put $gamelist query into a gamelistarray

		echo '<div id="user-screen">';

		// print_r($_POST);

		?>

		<!-- CHECKBOX ADD FUNCTION -->
		<script>

		$(document).ready(function() {
		    function recalculate() {
		        var sum = 0.00;

		        $("input[type=checkbox]:checked").each(function() {
		            sum += parseFloat($(this).attr("rel"));
		        });

		        $("#output").html(sum.toFixed(2));
		        $("#checkval").val(sum.toFixed(2));
		    }

		    $("input[type=checkbox]").change(function() {
		        recalculate().toFixed(2);

		    });

		});

		</script>


		<?php

		// put $_POST in array
		$postinfo = explode( ';',$_POST['game']);

		$user1id = $_SESSION['user_id'];
		$user2id = $postinfo[0];

		// user info pull
		$user1infoqy = mysqli_query($link, "SELECT * FROM db_users WHERE user_id='$user1id'");
		$user1info = mysqli_fetch_array($user1infoqy);

		$user2infoqy = mysqli_query($link, "SELECT * FROM db_users WHERE user_id='$user2id'");
		$user2info = mysqli_fetch_array($user2infoqy);

		// put u1wu2h with game info
		$u1wu2h = explode( ',',$postinfo[1]);
		$u1hu2w = explode( ',',$postinfo[2]);

		echo '<form method="POST" action="sendoffer.php" name="form">';

		// USER 1 WANT
		echo '<div class="container" id="compgmcont">
				<div class="row">
					<div class="col-sm-5 compusertile">
						<div class="row">';


			$default1 = "http://www.gamecycler.com/g-logo-1024x768.gif";
	        $size1 = 238;
	        $email = $user1info['email'];

	        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
	        
	        echo ' 	 	<div class="col-sm-6 col-sm-offset-3" id="compusimg" align="center">
	                    	<img src="'. $grav_url1 .'">
	                	</div><!-- col-sm-4 image -->';

	        echo '		<div class="col-sm-12 text-center gameshead"><h2><strong>';
	        print_r(		$user1info['username']);
	        echo '		</strong></h2></div><!-- col-sm-12 username -->
	        			<div class="col-sm-10 col-sm-offset-2">';

		foreach($u1wu2h as $title => $gameu1wu2h) {
			$i = 1;
			$gamevalue = $gamestable[$gameu1wu2h]['loose'];

			print_r('<input type="checkbox" name="'.$user1id.'" value="'.$gamestable[$gameu1wu2h]['game_id'].'" rel="'.$gamevalue.'"> <span style="font-size: 1.2em; padding-left: 5px;"><strong>'.$gamestable[$gameu1wu2h]['title'].' - '.$gamestable[$gameu1wu2h]['console'].' | $'.$gamevalue.'</strong></span><br />');
			$i++;
		}

		echo '			</div> <!-- col-sm-12 games-->
						</div> <!-- row -->
					</div> <!-- col-sm-5 -->
					<div class="col-sm-2 text-center">
						<strong>You can also enter another amount</strong><br /><br />
						Owe (-)/Asking (+):
						<input type="checkbox hidden" id="checkval" name="checkval" value="" rel="0" checked></input>
						<input type="submit" type="button" value="Send Offer">
					</div>
					<div class="col-sm-5">
				';

		// USER 2 WANT

		echo '<div class="container" id="compgmcont">
				<div class="row">
					<div class="col-sm-5 compusertile">
						<div class="row">';


			$default2 = "http://www.gamecycler.com/g-logo-1024x768.gif";
	        $size2 = 238;
	        $emai2 = $user2info['email'];

	        $grav_url2 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email2 ) ) ) . "?d=" . urlencode( $default2 ) . "&s=" . $size2;
	        
	        echo ' 	 	<div class="col-sm-6 col-sm-offset-3" id="compusimg" align="center">
	                    	<img src="'. $grav_url1 .'">
	                	</div><!-- col-sm-4 image -->';

	        echo '		<div class="col-sm-12 text-center gameshead"><h2><strong>';
	        print_r(		$user2info['username']);
	        echo '		</strong></h2></div><!-- col-sm-12 username -->
	        			<div class="col-sm-10 col-sm-offset-2">';

		foreach($u1hu2w as $title => $gameu1hu2w) {
			$i = 1;

			$gamevalue = $gamestable[$gameu1hu2w]['loose'];
			
			print_r('<input type="checkbox" name="'.$user2id.'" value="'.$gamestable[$gameu1hu2w]['game_id'].'" rel="-'.$gamevalue.'"> <span style="font-size: 1.2em; padding-left: 5px;"><strong>'.$gamestable[$gameu1hu2w]['title'].' - '.$gamestable[$gameu1wu2h]['console'].' | $'.$gamevalue.'</strong></span><br >');
			$i++;
		}

		echo '		</div> <!-- col-sm u1hu2w --> ';

		echo '		

					</form>
				</div> <!-- row -->
			</div> <!-- container -->

			</div>
			</div>
			</div>
			</div>
			</div>';

		?>


		<?php

	}
	else 
	{

		echo '<div id="user-screen">';

		####################
		## USER 1 HAVE LOOP
		####################

		// Global User Id User 1 Want
		$user_id = $_SESSION['user_id'];
		$username = $_SESSION['Username'];
						
		// Query based on User Id 1
		$user1havedb = "SELECT * FROM userhavegames WHERE user_id='" . $user_id . "'";
		$user1wantdb = "SELECT * FROM userwantgames WHERE user_id='" . $user_id . "'";

		// Query to pull up all games information from db_games
		$gamesdblist = "SELECT * FROM db_games";
		$gameslist = mysqli_query($link, $gamesdblist);

		// create game refernce table
		while($gamesarray = mysqli_fetch_array($gameslist,MYSQLI_ASSOC)) {
					$gamestabid = $gamesarray['game_id'];
					$gamestable[$gamestabid] = $gamesarray; // place entire gamewantarray into a numbered array to reference individual results
				} // put $gamelist query into a gamelistarray

		// Place Userwantgames results into an array
		$user1havequery = mysqli_query($link, $user1havedb);
		$user1havegames = mysqli_fetch_array($user1havequery, MYSQLI_ASSOC);

		// Display want games for user to see

		echo '
			<div class="container">
				<div class="row" id="compgm">
					<div class="col-sm-12 text-center"><h1 style="text-transform:uppercase; margin:30px 0; font-size: 2.1em;"><strong>Summary of '.$username.'\'s Games:</strong></h1></div>';

		// Place Userwantgames results into an array
		$user1wantquery = mysqli_query($link, $user1wantdb);
		$user1wantgames = mysqli_fetch_array($user1wantquery, MYSQLI_ASSOC);


		echo '		<div class="col-sm-5" style="margin-left: 50px;">
						<div class="row">
							<div class="col-sm-12 gameshead text-center" style="padding-top: 5px;"><h3>Want Games</h3></div>';
				
		foreach ($user1wantgames as $user1wantrow => $gameswantID) {
			if($user1wantrow != 'user_id') {
				$u1wgmexp = explode( ',',$gameswantID );
				foreach($u1wgmexp as $gmwIDarrhead => $gmwIDarrID) {
					if($gmwIDarrID != 0) {

				echo "<div class='col-sm-4 gmtile compgm'>
				<a href='gamedetail.php?game_id=".$gmwIDarrID."'>
						<div class='row'>
							<div class='col-sm-12 gmimage'>";

							if(empty($gamestable[$gmwIDarrID]['image'])) {
								echo '<img src="images/g-logo-big.gif">';
							} 
							else {
								echo '<img src="showfile.php?game_id='.$gmwIDarrID.'">';
							}

							echo "	</div> <!-- gmimage -->
									<div class='row col-sm-12 gminfo'>
										<div class='col-sm-12 gmtitle'>";
										print_r($gamestable[$gmwIDarrID]['title']);

						echo "		</div> <!-- title -->
									<div class='col-sm-12 gmconsole'>";
										print_r($gamestable[$gmwIDarrID]['console']);

						echo "		</div> <!-- console -->
									<div class='col-sm-12 gmyear'>";									
										print_r($gamestable[$gmwIDarrID]['yearreleased']);
						echo "		</div> <!-- yearreleased -->";
						
						echo "	</div> <!-- gminfo row -->
							</div> <!-- main row -->
							</a>
						</div> <!-- main div -->";
							 
					} // if gmwIDarrid not 0
				} // if not user_id
			} // if user1wantrow not user_id
		} // foreach user1wantgames

		echo '			</div> <!-- row -->
					</div> <!-- col-sm-6 wants section div -->';


		// HAVE GAMES
		echo '		<div class="col-sm-5 col-sm-offset-1">
						<div class="row">
							<div class="col-sm-12 gameshead text-center" style="padding-top: 5px;"><h3>Library</h3></div>';
				
						foreach ($user1havegames as $user1haverow => $gameshaveID) {
							if($user1haverow != 'user_id') {
								$u1hgmexp = explode( ',',$gameshaveID );
								foreach($u1hgmexp as $gmhIDarrhead => $gmhIDarrID) {
									if($gmhIDarrID != 0) {
										echo "<div class='col-sm-4 gmtile compgm'>
										<a href='gamedetail.php?game_id=".$gmhIDarrID."'>
												<div class='row'>
													<div class='col-sm-12 gmimage'>";

													if(empty($gamestable[$gmhIDarrID]['image'])) {
														echo '<img src="images/g-logo-big.gif">';
													} 
													else {
														echo '<img src="showfile.php?game_id='.$gmhIDarrID.'">';
													}

													echo "	</div> <!-- gmimage -->
															<div class='row col-sm-12 gminfo'>
																<div class='col-sm-12 gmtitle'>";
																print_r($gamestable[$gmhIDarrID]['title']);

												echo "		</div> <!-- title -->
															<div class='col-sm-12 gmconsole'>";
																print_r($gamestable[$gmhIDarrID]['console']);

												echo "		</div> <!-- console -->
															<div class='col-sm-12 gmyear'>";									
																print_r($gamestable[$gmhIDarrID]['yearreleased']);
												echo "		</div> <!-- yearreleased -->";
												
												echo "	</div> <!-- gminfo row -->
													</div> <!-- main row -->
													</a>
												</div> <!-- main div -->";
													 
											} // if gmwIDarrid not 0
										} // if not user_id
									} //
								} // foreach user1wantgames


		echo "	</div>";


		echo"	</div>
		

		<br /><br />
		"; 


		########################################################
		##   U1 WANT COMPARED TO ALL OTHER USERS HAVE LOOP    ##
		########################################################

		// Call entire userhavegames DB with no ID reference
		$alluserhavedb = "SELECT * FROM userhavegames";
		$alluserwantdb = "SELECT * FROM userwantgames";

		// Fetch arrays in userhavegames, assign each row into userhave[] variable, print upon each iteration

		$alluserhavequery = mysqli_query($link, $alluserhavedb);
		$alluserwantquery = mysqli_query($link, $alluserwantdb);

		// print_r($alluserhavearray);
		// echo "<br /><br />";

		while($alluserhavearray = mysqli_fetch_array($alluserhavequery,MYSQLI_ASSOC)) {
			$auharr[] = $alluserhavearray;
		}


		while($alluserwantarray = mysqli_fetch_array($alluserwantquery,MYSQLI_ASSOC)) {
			// print_r($alluserwantarray);
			// $auwid = $alluserwantarray['user_id'];
			$auwarr[] = $alluserwantarray;
		}

		foreach($auwarr as $key => $idgame) {
			$auwgmarr[] = array($idgame['user_id'] => $idgame['games']);
		}

		$counter = count($auwgmarr);

		foreach($auharr as $auharr2) {
				$user2id = $auharr2['user_id'];
				$auhg = explode( ",",$auharr2['games']);
				$compahu1w = array_intersect($auhg, $u1wgmexp);
				$countahu1w = count($compahu1w);
				$compahu1wimp = implode( ',',$compahu1w);
				
				// user1have to all user want
				for($i = 0; $i < $counter; $i++){
					if($auwarr[$i]['user_id'] == $user2id) {
						$auwg = explode( ",",$auwarr[$i]['games']);
						$compawu1h = array_intersect($auwg, $u1hgmexp);
						$countu1haw = count($compawu1h);
						$compawu1himp = implode( ',',$compawu1h);
					}
				}
				if($compahu1w && $compawu1h != NULL) {
					if($user_id != $user2id) {
						$matcharray[] = array(
								"user2id"		=> $user2id,
								"u1wantallhave" => $compahu1wimp,
								"countu1wah" 	=> $countahu1w,
								"u1haveallwant" => $compawu1himp,
								"countu1haw"	=> $countu1haw
								);
								}
							}
			}

			
		if(count($matcharray) == 0) {
			echo ' <div class="col-sm-12 text-center">
						<h1 style="margin: 30px 0; font-size: 2.1em;"><strong>YOU HAVE NO MATCHES</strong></h1>
						<a href="home.php"><i class="fa fa-chevron-left" style="margin-right: 8px;"></i>Go To Home Screen</a>
					</div>
				</div>
			</div>';

		} else {

		foreach($matcharray as $key => $row) {
			$countu1wah[$key] = $row['countu1wah'];
			$countu1haw[$key] = $row['countu1haw'];
		}
			
		array_multisort($countu1wah, SORT_DESC, $matcharray);

		

		// Create Comparison Fields

		echo '	<div class="col-sm-12 text-center">
					<h1 style="text-transform:uppercase; margin:30px 0; font-size: 2.1em;"><strong>'.$username.'\'s Matches:</strong></h1>
				</div>
				';

		echo '	<form action="comparegames.php" method="post">
				<div class="col-sm-12 text-center" style="margin-bottom: 15px;">Select Trade, Then Click <i class="fa fa-chevron-right" style="margin-right: 8px;"></i><input type="submit" value="Initiate Trade"></div>		';

		// print_r($matcharray);

		// Users Info DB
		$alluserinfo = "SELECT user_id, username FROM db_users";
		$alluserinfoqy = mysqli_query($link, $alluserinfo);

		while($alluserinfofet = mysqli_fetch_array($alluserinfoqy)) {
			$alluserid = $alluserinfofet['user_id'];
			$alluserarr[$alluserid] = $alluserinfofet;
		}

		$i = 0;
		foreach($matcharray as $key => $matchrow) {
		++$i;

		$user2 = $matchrow['user2id'];
		$user2rev = "SELECT count(rating) as count, sum(rating) as sum FROM userreviews WHERE reviewee='".$user2."'";
		$user2revqy = mysqli_query($link, $user2rev);
		$user2revfet = mysqli_fetch_array($user2revqy);

		$revct = $user2revfet['count'];
		$revsum = $user2revfet['sum'];


		// print_r($revct.' '.$revsum);

		$revtot = round($revsum/$revct,0,PHP_ROUND_HALF_UP);

		echo "	<div class='col-sm-12 gmcompuser'>
					<div class='row'>";
					
		print_r('		<div class="col-sm-1 matbttn text-right"><input type="radio" name="game" value="'.$matchrow['user2id'].';'.$matchrow['u1wantallhave'].';'.$matchrow['u1haveallwant'].'"></div>
						<div class="col-sm-11 text-center gameshead gmcprat"><h3>Match '.$i.': <a href="profile.php?id='.$user2.'" target="_blank" >'.$alluserarr[$user2]['username'].'</a> | Rating: ');

						if($revct == 0) {
							echo "<i>No rating yet</i>";
						} else {
							$rev = new starrat();
							$rev->starrat1($revtot);
						}

		echo 			"</h3></div>";

		// USER 1 WANT HALF
		echo "			<div class='col-sm-5 col-sm-offset-1'>
							<div class='row'>";

		echo "					<div class='col-sm-12 text-center'><strong>".$username." Wants From ".$alluserarr[$user2]['username']."</strong></div>";
								$u1wah = explode( ',',$matchrow['u1wantallhave']);


								foreach($u1wah as $row => $gameid4title) {
									
									echo "<div class='col-sm-6 gmtile matchgm'>
											<a href='gamedetail.php?game_id=".$gameid4title."'>
												<div class='row'>
													<div class='col-sm-12 gmimage'>";

													if(empty($gamestable[$gameid4title]['image'])) {
														echo '<img src="images/g-logo-big.gif">';
													} 
													else {
														echo '<img src="showfile.php?game_id='.$gameid4title.'">';
													}

											echo "	</div> <!-- gmimage -->
													<div class='row col-sm-12 gminfo'>
														<div class='col-sm-12 gmtitle'>";
															print_r($gamestable[$gameid4title]['title']." | <span style='font-size: 0.7em;'>$".$gamestable[$gameid4title]['loose']."</span>");

										echo "			</div> <!-- title -->
														<div class='col-sm-12 gmconsole'>";
															print_r($gamestable[$gameid4title]['console']);

										echo "			</div> <!-- console -->
														<div class='col-sm-12 gmyear'>";									
															print_r($gamestable[$gameid4title]['yearreleased']);
										echo "			</div> <!-- yearreleased -->";
											
										echo "		</div> <!-- gminfo row -->
												</div> <!-- main row -->

											</a> <!-- main link for game -->
										 </div>
										<!-- </div> <!-- main div -->";

									// print_r($gamestable[$gameid4title]['title'].'<br />');
								}

				echo '	  		</div> <!-- row -->
							</div> <!-- col-sm-6 wants section div -->';

				echo '		<div class="col-sm-1 text-center"><h2><strong><div id="matfor">FOR</div></strong></h2></div>';
								
		// USER 2 WANT HALF				
		echo "			<div class='col-sm-5'>
							<div class='row'>";

		echo "					<div class='col-sm-12 text-center'><strong>".$alluserarr[$user2]['username']." Wants From ".$username."</strong></div>";
								$u1haw5 = explode( ',',$matchrow['u1haveallwant']);
								foreach($u1haw5 as $row => $gameid4title2) {

									echo "<div class='col-sm-6 gmtile matchgm'>
											<a href='gamedetail.php?game_id=".$gameid4title2."'>
												<div class='row'>
													<div class='col-sm-12 gmimage'>";

													if(empty($gamestable[$gameid4title2]['image'])) {
														echo '<img src="images/g-logo-big.gif">';
													} 
													else {
														echo '<img src="showfile.php?game_id='.$gameid4title2.'">';
													}

											echo "	</div> <!-- gmimage -->
													<div class='row col-sm-12 gminfo'>
														<div class='col-sm-12 gmtitle'>";
															print_r($gamestable[$gameid4title2]['title']." | <span style='font-size: 0.7em;'>$".$gamestable[$gameid4title2]['loose']."</span>");

										echo "			</div> <!-- title -->
														<div class='col-sm-12 gmconsole'>";
															print_r($gamestable[$gameid4title2]['console']);

										echo "			</div> <!-- console -->
														<div class='col-sm-12 gmyear'>";									
															print_r($gamestable[$gameid4title2]['yearreleased']);
										echo "			</div> <!-- yearreleased -->";
											
										echo "		</div> <!-- gminfo -->
												</div> <!-- main row -->
											</a> <!-- main link for game -->
										</div> <! -- compg -->";
								} // foreach u1haw5

				echo '		</div> <!-- row -->
						</div> <!-- col-sm-6 wants section div -->

					</div> <!-- main row -->
				</div> <!-- foreach matcharray -->';

		
		} // for each matcharry

		echo '		
				</form>
					
					
				</div> <!-- row --->
			</div><!-- container -->';

		echo "		</div></div></div>
					</div> <!-- match section row -->
				</div> <!-- col-sm-6 match section -->";		
			

		



		echo '		</div>
				</div>
			';

		}
	}

	
	
}
else
{
	?>
	<meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
	<?php
}

?>

<?php include 'footer.php'; ?>