<?php include 'base.php'; ?>

<?php
$title = "Gamecycler | Games Listed on Gamecycler";
$description = "Search Our Games Database To See if Any User Has This Game";
$keywords = "video games, nintendo, playstation, xbox, xbox one, xbox360, atari, sega, genesis, gamers, trade games";

?>

<?php include 'header.php'; ?>


<body>

  
 <?php

include 'gameside.php';
		
			// Game Display
			
echo		'<div class="col-sm-9 gamessection">
				<div class="row">
			';

$gamesql = "SELECT game_id, title, console, yearreleased, image FROM db_games";
$consolekey = "SELECT * from consolekey";

$gamestable = mysqli_query($link, $gamesql);
$consolekeyquery = mysqli_query($link, $consolekey);


// Place DB Games info in arrays

while($gamesarray = mysqli_fetch_array($gamestable)) {
	$gameid = $gamesarray['game_id'];
	$gamestitle[$gameid] = $gamesarray;
}

while($consolekeyarray = mysqli_fetch_array($consolekeyquery,MYSQLI_ASSOC)) {
	$conkey[] = $consolekeyarray;
}


################################

   // SEARCH BOX RESULTS //

################################


if(!empty($_POST['gamesearch']) && empty($_POST['console']) && empty($_POST['allgames']) && empty($_GET['hx'])){ 
	// if(isset($_GET['go'])){ 
		if(preg_match("^/[A-Za-z]+/^", $_POST['gamesearch']) == FALSE){ 
			$name = mysqli_real_escape_string($link, $_POST['gamesearch']);
			$sql2 = "SELECT game_id, title, console, image, yearreleased, description, loose FROM db_games WHERE title LIKE '%" . $name . "%' OR description LIKE '%" . $name  ."%'";
			//-run  the query against the mysql query function 
			$result = mysqli_query($link, $sql2);
			//-create  while loop and loop through result set 
			echo '<div class="col-sm-12 reset">
						<a href="games.php"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Reset Search</a>
					</div> <!-- reset 12 -->
					<div class="col-sm-12 gameshead"><h2><strong>SEARCH RESULTS FOR "'.$name.'"</strong></h2></div>
							';
				

			while($row = mysqli_fetch_array($result)){ 
				$gametitle  = $row['title'];
				$gameid = $row['game_id'];
				$gameimg = $row['image'];
				$gameconsole = $row['console']; 
				$gameyear = $row['yearreleased']; 
				//-display the result of the array  

				echo "<div class='col-sm-4 gmtile'>
				<a href='gamedetail.php?game_id=".$gameid."'>
						<div class='row'>
							<div class='col-sm-12 gmimage pull-center'>";
						
						if(empty($gamestitle[$gameid]['image'])) {
							echo '<img src="images/g-logo-big.gif">';
						} 
						else {
							echo "<img src='showfile.php?game_id=".$gameid."'>";
						}
						
					echo "	</div> <!-- gmimage -->
							<div class='row col-sm-12 gminfo'>
								<div class='col-sm-12 text-center gmtitle'>";
									print_r('<a href="gamedetail.php?game_id='.$gameid.'">'.$gametitle.'</a></strong>');
					echo "		</div> <!-- title -->
								<div class='col-sm-6 gmconsole'>";
									print_r($gameconsole);
					echo "		</div> <!-- console -->
								<div class='col-sm-6 text-right gmyear'>";									
									print_r($gameyear);
					echo "		</div> <!-- yearreleased -->";

			echo "	</div> <!-- gminfo row -->
					</div> <!-- main row -->
					</a>
				</div> <!-- main div -->";
				
				
				
				}
			}
		else
		{ 
		echo  "<p>Please enter a search query</p>"; 
		}
	// }
	echo "</div> <!-- row -->
		</div> <!-- container -->";

		mysqli_free_result($consolekeyquery);
		mysqli_free_result($result);

}
elseif((isset($_POST['allgames']) && !isset($_POST['allusergames']) && empty($_POST['gamesearch'])) || (!empty($_GET)) ) // ALL GAMES
	/*******************************
			ALL GAMES SHOWN

	*******************************/

{

	// Pagination

	$rec_limit = 30;
	if(! $link ) {
		die('Could not connect: ' . mysqli_error());	
	}
	// mysqli_select_db('db_games');
			 
	/* Get total number of records */
	$sql = "SELECT count(game_id) as total FROM db_games";
	$retval = mysqli_query($link, $sql);
			 
	if(! $retval ) {
		die('Could not get data: ' . mysqli_error());
	}

	$row = mysqli_fetch_array($retval, MYSQLI_NUM);
	$rec_count = $row[0];
	
	if( isset($_GET['page'])) {
		$page = $_GET['page'] + 1;
		$offset = $rec_limit * $page ;
	}
	else
	{
		$page = 0;
		$offset = 0;
	}

	$left_rec = $rec_count - ($page * $rec_limit);
	
	// IF LETTER IS SELECTED
	if(isset($_GET['hx'])) { 
	
		$gmltfirst = $_GET['hx'];
				
		$sql = 'SELECT game_id, title, console, image, yearreleased, description, loose FROM db_games WHERE title LIKE "'.$gmltfirst.'%" LIMIT '.$offset.','.$rec_limit;
		$sqlcount = 'SELECT title, count(game_id) as total FROM db_games WHERE title LIKE "'.$gmltfirst.'%"'; 
	
	}
	else 
	{
		$sql = 'SELECT game_id, title, console, image, yearreleased, description, loose FROM db_games ORDER BY title ASC LIMIT '.$offset.','.$rec_limit;
		$sqlcount = 'SELECT count(game_id) as total FROM db_games';
	}
	
	$sqlcountqy = mysqli_query($link,$sqlcount);
	$data = mysqli_fetch_assoc($sqlcountqy);
	$gamecountletter = $data['total'];
	
	$retval = mysqli_query($link, $sql);
			 
	if(! $retval ) {
		die('Could not get data: ' . mysqli_error());
	}
	
	while($allgamefetarr = mysqli_fetch_array($retval)) {
		$allgmarrarr[] = $allgamefetarr;
	}
	
	
	echo "<div class='col-sm-12 reset'>
			<a href='games.php'><i class='fa fa-chevron-circle-left' aria-hidden='true'></i> Reset Search</a>
		</div> <!-- reset 12 -->
		<div class=\"col-sm-12 gameshead\"><h2><strong>ALL GAMES ON GAMECYCLER</strong></h2></div>
	
	<div class=\"col-sm-12\" id=\"game1ltsearch\" align=\"center\">
		<strong>FILTER BY LETTER:</strong>
		<form name=\"myform\" action=\"$_PHP_SELF\" method=\"get\">
			<input type='hidden' id='hx' name='hx' value=''>
				<a href=\"javascript: submitform('a')\">A</a> 
				<a href=\"javascript: submitform('b')\">B</a> 
				<a href=\"javascript: submitform('c')\">C</a> 
				<a href=\"javascript: submitform('d')\">D</a> 
				<a href=\"javascript: submitform('e')\">E</a> 
				<a href=\"javascript: submitform('f')\">F</a> 
				<a href=\"javascript: submitform('g')\">G</a> 
				<a href=\"javascript: submitform('h')\">H</a> 
				<a href=\"javascript: submitform('i')\">I</a> 
				<a href=\"javascript: submitform('j')\">J</a> 
				<a href=\"javascript: submitform('k')\">K</a> 
				<a href=\"javascript: submitform('l')\">L</a> 
				<a href=\"javascript: submitform('m')\">M</a> 
				<a href=\"javascript: submitform('n')\">N</a> 
				<a href=\"javascript: submitform('o')\">O</a> 
				<a href=\"javascript: submitform('p')\">P</a> 
				<a href=\"javascript: submitform('q')\">Q</a> 
				<a href=\"javascript: submitform('r')\">R</a> 
				<a href=\"javascript: submitform('s')\">S</a> 
				<a href=\"javascript: submitform('t')\">T</a> 
				<a href=\"javascript: submitform('u')\">U</a> 
				<a href=\"javascript: submitform('v')\">V</a> 
				<a href=\"javascript: submitform('w')\">W</a> 
				<a href=\"javascript: submitform('x')\">X</a> 
				<a href=\"javascript: submitform('y')\">Y</a> 
				<a href=\"javascript: submitform('z')\">Z</a> 
		</form>
	</div>";
	
	?>
	<script type="text/javascript">
		function submitform(val)
		{
		  $("#hx").val(val);
		  document.myform.submit();
		}
	</script>
	
<?php
	
	echo '</div>';
		// For All Games to be Displayed
		foreach($allgmarrarr as $key => $allgameinfo) {
			echo '<div class="col-sm-6 col-md-4 col-lg-4 gamegrid row-eq-height">
					<a href="gamedetail.php?game_id='.$allgameinfo['game_id'].'">
						<div class="row">
							<div class="col-sm-12 gmimage">';

					if(empty($allgameinfo['image'])) {
						echo '<img src="images/g-logo-big.gif">';
					} 
					else {
						echo '<img src="showfile.php?game_id='.$allgameinfo['game_id'].'">';
					}
								
					echo "	</div> <!-- gmimage -->
							<div class='row col-sm-12 gminfogm'>
								<div class='col-sm-12 text-center gmtitle'>";
									print_r($allgameinfo['title']);
					echo "		</div> <!-- title -->
								<div class='col-sm-6 gmconsole'>";
									print_r($allgameinfo['console']);
					echo "		</div> <!-- console -->
								<div class='col-sm-6 text-right gmyear'>";
									print_r($allgameinfo['yearreleased']);
					echo "		</div> <!-- yearreleased -->";

					echo "	</div> <!-- gminfo row -->
							</div> <!-- main row -->
						</div> <!-- main div -->";
	
	}
	echo '<div class="col-sm-12">';
	if(!empty($_GET['hx'])) {
		if( $page > 0 ) {
				$last = $page - 2;
				echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$last\">Last 30 Records</a> |";
				echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$page\">Next 30 Records</a><br />";

				$gamecountpage = $gamecountletter/$rec_limit;

				for($i = 0; $i < $gamecountpage-1; $i++) {
					$j = $i * $rec_limit;
					$k = $j + $rec_limit;
					echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$i\">$j-$k </a>";
				}
			 }else if( $page == 0 ) {
				echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$page\">Next 30 Records</a><br />";
				$gamecountpage = $gamecountletter/$rec_limit;

				for($i = 0; $i < $gamecountpage-1; $i++) {
					$j = $i * $rec_limit;
					$k = $j + $rec_limit;
					echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$i\">$j-$k </a>";
				}
			 }else if( $left_rec < $rec_limit ) {
				$last = $page - 2;
				echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$last\">Last 30 Records</a><br />";
				$gamecountpage = $gamecountletter/$rec_limit;

				for($i = 0; $i < $gamecountpage-1; $i++) {
					$j = $i * $rec_limit;
					$k = $j + $rec_limit;
					echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$i\">$j-$k </a>";
				}
			 }
	}
	else 
	{	
		$allreccount = round($rec_count/$rec_limit);
		
		if( $page > 0 ) {
				$last = $page - 2;
				echo "<a href = \"$_PHP_SELF?page=$last\">Last 30 Records</a> |";
				echo "<a href = \"$_PHP_SELF?page=$page\">Next 30 Records</a><br />";
				
				for($z = 0; $z < $rec_count-1; $z = $z + 1000 ){
					$y = $z + 1000;
					echo "$z-$y
					<input class=\"myButton\" type=\"button\" value=\"Groups+\"></input>
						<div class=\"containerconsole pageexp\">";
						if($z == 0) { echo "<a href = \"$_PHP_SELF?page=-1\">1-31 </a>"; }
					for($i = round(($z/$rec_limit),0,PHP_ROUND_HALF_DOWN); $i <= round(($y/$rec_limit),0,PHP_ROUND_HALF_DOWN) - 1 ; $i++){
						$j = ($i * $rec_limit) + 31;
						$k = $j + $rec_limit;
						echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$i\">$j-$k </a>";
					}
					echo "</div>";
				}
				
			 }else if( $page == 0 ) {
				echo "<a href = \"$_PHP_SELF?page=$page\">Next 30 Records</a><br />";
				
				for($z = 0; $z <= $rec_count-1; $z = $z + 1000 ){
					$y = $z + 1000;
					echo "$z-$y
					<input class=\"myButton\" type=\"button\" value=\"Groups+\"></input>
						<div class=\"containerconsole pageexp\">";
						if($z == 0) { echo "<a href = \"$_PHP_SELF?page=-1\">1-31 </a>"; }
					for($i = round(($z/$rec_limit),0,PHP_ROUND_HALF_DOWN) ; $i < round(($y/$rec_limit),0,PHP_ROUND_HALF_DOWN) - 1 ; $i++){
						$j = ($i * $rec_limit) + 31;
						$k = $j + $rec_limit;
						echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$i\">$j-$k </a>";
					}
					echo "</div>";
				}

			 }else if( $left_rec < $rec_limit ) {
				$last = $page - 2;
				echo "<a href = \"$_PHP_SELF?page=$last\">Last 30 Records</a>";
				
				for($z = 0; $z <= $rec_count-1; $z = $z + 1000 ){
					$y = $z + 1000;
					echo "$z-$y
					<input class=\"myButton\" type=\"button\" value=\"Groups+\"></input>
						<div class=\"containerconsole pageexp\">";
						if($z == 0) { echo "<a href = \"$_PHP_SELF?page=-1\">1-31 </a>"; }
					for($i = round(($z/$rec_limit),0,PHP_ROUND_HALF_DOWN); $i < round(($y/$rec_limit),0,PHP_ROUND_HALF_DOWN) - 1 ; $i++){
						$j = ($i * $rec_limit) + 31;
						$k = $j + $rec_limit;
						echo "<a href = \"$_PHP_SELF?hx=$gmltfirst&page=$i\">$j-$k </a>";
					}
					echo "</div>";
				}
			 }
	}
	echo '</div>';

	mysqli_free_result($retval);
	mysqli_free_result($sqlcountqy);
	
	mysqli_free_result($consolekeyquery);

	
} elseif(isset($_POST['allusergames']) && empty($_POST['gamesearch']) && !isset($_POST['console'])) 
	/****************************************
		
		IF ALL USER GAMES HAS BEEN CLICKED

	*****************************************/
{
		$alluser = "SELECT * FROM userhavegames";
		$alluserqy = mysqli_query($link, $alluser);

		while($alluserfet = mysqli_fetch_array($alluserqy)) {
			$augm = explode(",",$alluserfet['games']);
			foreach($augm as $key => $gmid) {
				if($gmid == 0) { continue; } else {
				$augm2[] = $gmid; }
			}
		}

		$augmun = array_unique($augm2);


		foreach($augmun as $gmval) {
			$allgmarr[] = array($gmval => $gamestitle[$gmval]['title']);
		}

		$allgmarrres = $allgmarr[1] + $allgmarr[0];

		for($i=0;$i < count($allgmarr);$i++) {
			$allgmarrres = $allgmarrres + $allgmarr[$i];
		}

		asort($allgmarrres);

		echo '<div class="container">
				<div class="row">';

		function resetsearch() {
			$_POST = array();
		}
		
		if (isset($_GET['reset'])) {
			resetsearch();
		}
		
		echo '<div class="col-sm-12 reset">
			<a href="games.php"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Reset Search</a>
		</div> <!-- reset 12 -->
		<div class="col-sm-12 gameshead alluserow">
			<h2><strong>ALL USER OWNED GAMES</strong></h2>
		</div>
				';

		foreach($allgmarrres as $gameid => $title) {
			echo '<a href="gamedetail.php?game_id='.$gameid.'">';
			echo "<div class='col-sm-4 gmtile'>
			<a href='gamedetail.php?game_id=".$gameid."'>
					<div class='row'>
						<div class='col-sm-12 gmimage'>";
			if(empty($gamestitle[$gameid]['image'])) {
				echo '<img src="images/g-logo-big.gif">';
			} 
			else {
				echo '<img src="showfile.php?game_id='.$gameid.'">';
			}

			echo "	</div> <!-- gmimage -->
					<div class='row col-sm-12 gminfo'>
						<div class='col-sm-12 text-center gmtitle'>";
							print_r($title);
			echo "		</div> <!-- title -->
						<div class='col-sm-6 gmconsole'>";
							print_r($gamestitle[$gameid]['console']);
			echo "		</div> <!-- console -->
						<div class='col-sm-6 text-right gmyear'>";
							print_r($gamestitle[$gameid]['yearreleased']);
			echo "		</div> <!-- yearreleased -->";

			echo "	</div> <!-- gminfo row -->
					</div> <!-- main row -->
				</div> <!-- main div -->";
		}

		echo '	</div></a> <!-- row -->
			</div> <!-- container -->';


		mysqli_free_result($alluserqy);
		mysqli_free_result($consolekeyquery);

} elseif (isset($_POST['console'])) 
	/*************************************
		
		IF CONSOLE HAS BEEN CLICKED

	*************************************/

	{
	
	function resetsearch() {
		$_POST = array();
	}
	
	if (isset($_GET['reset'])) {
		resetsearch();
	}

	$consolepostarr = $_POST['console'];
	foreach($consolepostarr as $key => $consolepost) {
		foreach($conkey as $key => $value) {
			$conval = $value['consolevalue'];
			$conname = $value['consolename'];
			$conarr[] = array($conval => $conname);
			}
		}

		foreach($conarr as $key => $arr) {
			if(!empty($arr[$consolepost])) {
				$console2 = $arr[$consolepost];
			}
		}

		echo '<div class="col-sm-12">
			<a href="games.php"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Reset Search</a>
		</div> <!-- reset 12 -->
		<div class="col-sm-12 gameshead alluserow">
			<h2><strong>'.$console2.' GAMES</strong></h2>
		</div> <!-- reset 12 -->

				';
	
	?>
	<script type="text/javascript">
		function submitform(val)
		{
		  $("#hx").val(val);
		  document.myform.submit();
		}
	</script>

	<?php

		for($i = 0; $i < count ($gamestitle); $i++) {
			if($gamestitle[$i]['console'] == $console2) {

				$game_id = $gamestitle[$i]['game_id'];
				
				echo "<div class='col-sm-4 gmtile'>
				<a href='gamedetail.php?game_id=".$gamestitle[$i]['game_id']."'>
						<div class='row'>
							<div class='col-sm-12 gmimage'>";
				


				if(empty($gamestitle[$i]['image'])) {
					echo '<img src="images/g-logo-big.gif">';
				} 
				else {
					echo '<img src="showfile.php?game_id='.$game_id.'">';
				}
				
				echo "	</div> <!-- gmimage -->
						<div class='row col-sm-12 gminfo'>
							<div class='col-sm-12 text-center gmtitle'>";
				print_r('<a href="gamedetail.php?game_id='.$gamestitle[$i]['game_id'].'">'.$gamestitle[$i]['title'].'</a>');
				echo "		</div> <!-- title -->
						<div class='col-sm-6 gmconsole'>";	
				print_r($gamestitle[$i]['console']);
				echo "		</div> <!-- console -->
						<div class='col-sm-6 text-right gmyear'>";
							print_r($gamestitle[$i]['yearreleased']);
				echo "	</div> <!-- yearreleased -->";

			echo "	</div> <!-- gminfo row -->
					</div> <!-- main row -->
					</a>
				</div> <!-- main div -->";
			}
		}
	// }
	// if( $page > 0 ) {
 //            $last = $page - 2;
 //            echo "<a href = \"$_PHP_SELF?page = $last\">Last 30 Records</a> |";
 //            echo "<a href = \"$_PHP_SELF?page = $page\">Next 30 Records</a>";
 //         }else if( $page == 0 ) {
 //            echo "<a href = \"$_PHP_SELF?page = $page\">Next 30 Records</a>";
 //         }else if( $left_rec < $rec_limit ) {
 //            $last = $page - 2;
 //            echo "<a href = \"$_PHP_SELF?page = $last\">Last 30 Records</a>";
 //         }
}
elseif (isset($_POST['want']))
#################################

  // USER WANT GAMES LISTED //

#################################

{

	if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {
		
		$user1 = $_SESSION['user_id'];
		$userwant = "SELECT * FROM userwantgames WHERE user_id='$user1'";
		$userwantqy = mysqli_query($link, $userwant);

		$userwantfet = mysqli_fetch_array($userwantqy);

 		$uwfetexp = explode(',',$userwantfet['games']);

 		echo '<div class="col-sm-12">
			<a href="games.php"><i class="fa fa-chevron-circle-left" aria-hidden="true"></i> Reset Search</a>
		</div> <!-- reset 12 -->
		<div class="col-sm-12 gameshead alluserow">
			<h2><strong>ALL WANTED GAMES</strong></h2>
		</div> <!-- reset 12 -->';

 		foreach($uwfetexp as $gameid) {
			echo "<div class='col-sm-4 gmtile'>
			<a href='gamedetail.php?game_id=".$gameid."'>
					<div class='row'>
						<div class='col-sm-12 gmimage'>";
			
			if(empty($gamestitle[$gameid]['image'])) {
				echo '<img src="images/g-logo-big.gif">';
			} 
			else {
				echo '<img src="showfile.php?game_id='.$gameid.'">';
			}

			echo "	</div> <!-- gmimage -->
					<div class='row col-sm-12 gminfo'>
						<div class='col-sm-12 text-center gmtitle'>";
							print_r($gamestitle[$gameid]['title']);
			echo "		</div> <!-- title -->
						<div class='col-sm-6 gmconsole'>";
							print_r($gamestitle[$gameid]['console']);
			echo "		</div> <!-- console -->
						<div class='col-sm-6 text-right gmyear'>";
							print_r($gamestitle[$gameid]['yearreleased']);
			echo "		</div> <!-- yearreleased -->";

			echo "	</div> <!-- gminfo row -->
					</div> <!-- main row -->
				</div> <!-- main div -->";
		} // foreach uwfetexp

		echo '	</div></a> <!-- row -->
			</div> <!-- container -->';
 		

 		mysqli_free_result($userwantqy);
 		mysqli_free_result($consolekeyquery);
 		
	} // if logged in
	else
	{
		?>
	    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
	    Please Log in!
	    <?php
	}


}
else 
	/*********************************** 
		DEFAULT START SCREEN
	***********************************/ 
{
	
	// MOST WANTED
	$mostwanted = "SELECT games FROM userwantgames";
	$mostwantedqy = mysqli_query($link,$mostwanted);
	
	while($mostwantedarray = mysqli_fetch_array($mostwantedqy)){
		$mostwantedgm[] = $mostwantedarray;
	}
	
	foreach($mostwantedgm as $key => $mwgm) {
		$mwgmlist = $mwgm['games'];
		$mwgmexp[] = explode( ',',$mwgmlist);
	}
	
	foreach($mwgmexp as $key => $game) {
		foreach($game as $key2 => $game2) {
			$keytitle[] = $game2;
		}
	}
	
	// echo '<div class="container">
	// 		<div class="row">';
	
	echo '
		<div class="col-sm-6">
			<div align="center" class="wantedhead">MOST WANTED GAMES</div>
	<table class="table">
			<tbody>
				<tr class="headrow">
					<td>Game</td><td>Console</td><td>Year</td><td>#</td>
				</tr>
			';
	
	$keytitleval = array_count_values($keytitle);
	arsort($keytitleval);
	foreach($keytitleval as $key => $count) {
		
		print_r('
				
				<tr>
					<td>'.'<a href="gamedetail.php?game_id='.$gamestitle[$key]['game_id'].'">'.$gamestitle[$key]['title'].'</td><td>'.$gamestitle[$key]['console'].'</td><td>'.$gamestitle[$key]['yearreleased'].'</td><td>'. $count.'</td>
				</tr>
					');
		
		if(++$i == 5) break;
	}
	
	echo '	</tbody>
		</table>
			</div> <!-- col-sm-6 most wanted -->';
			
			
	// MOST OWNED
	
	$mostowned = "SELECT games FROM userhavegames";
	$mostownedqy = mysqli_query($link, $mostowned);
	
	while($mostownedarray = mysqli_fetch_array($mostownedqy)){
		$mostownedgm[] = $mostownedarray;
	}
	
	foreach($mostownedgm as $key => $mogm) {
		$mogmlist = $mogm['games'];
		$mogmexp[] = explode( ',',$mogmlist);
	}
	
	foreach($mogmexp as $ownedkey => $ownedgame) {
		foreach($ownedgame as $ownedkey2 => $ownedgame2) {
			$ownedkeytitle[] = $ownedgame2;
		}
	}
	
	echo '
		<div class="col-sm-6">
			<div align="center" class="wantedhead">MOST OWNED GAMES</div>
	<table class="table">
			<tbody>
				<tr class="headrow">
					<td>Game</td><td>Console</td><td>Year</td><td>#</td>
				</tr>
			';
	
	$ownedkeytitleval = array_count_values($ownedkeytitle);
	arsort($ownedkeytitleval);
	
	foreach($ownedkeytitleval as $ownedkey => $ownedcount) {
		// $test = array_search($ownedkey, array_column($gamestitle, 'game_id'));
		print_r('
				
				<tr>
					<td>'.'<a href="gamedetail.php?game_id='.$gamestitle[$ownedkey]['game_id'].'">'.$gamestitle[$ownedkey]['title'].'</a></td><td>'.$gamestitle[$ownedkey]['console'].'</td><td>'.$gamestitle[$ownedkey]['yearreleased'].'</td><td>'. $ownedcount.'</td>
				</tr>
					');
		
		if(++$j == 5) break;
	}
	
	echo '	</tbody>
		</table>
			</div> <!-- col-sm-6 most owned -->';

	// RECENT SIGNED UP USERS

	$usersign = 'SELECT * FROM db_users';
	$usersignqy = mysqli_query($link, $usersign);

	while($usersignarr = mysqli_fetch_array($usersignqy)) {
		$usersignarr2[] = $usersignarr;
	}

	$userwant = 'SELECT * FROM userwantgames';
	$userwantqy = mysqli_query($link, $userwant);

	while($userwantfetch = mysqli_fetch_array($userwantqy)) {
		$userwantarr[] = $userwantfetch;
	}

	$useroff = 'SELECT * FROM userhavegames';
	$useroffqy = mysqli_query($link, $useroff);

	while($userofffetch = mysqli_fetch_array($useroffqy)) {
		$useroffarr[] = $userofffetch;
	}

	echo '<div class="col-sm-12">
			<div align="center" class="wantedhead">NEW USERS</div>
		<table class="table">
			<tbody>
				<tr><td></td><td>User</td><td>Location</td><td>Want</td><td>Offering</td></tr>
	';
		krsort($usersignarr2);
		foreach($usersignarr2 as $key3 => $usersigninfo) {

			echo '<tr><td>';
			// image
			$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
			$email1 = $usersignarr2['email'];
			$size1 = 40;

			$grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;

			print_r('<img id="userimg-sum" width="'.$size1.'" src="'. $grav_url1 .'">');

			echo '</td><td>';
			print_r('<a href="profile.php?id='.$usersigninfo['user_id'].'">'.$usersigninfo['username'].'</a>');
			echo '</td><td>';
			print_r($usersigninfo['city'].', '.$usersigninfo['state']);
			echo '</td><td>';
			// want
			foreach($userwantarr as $key => $userwantinfo) {
				if($userwantinfo['user_id'] == $usersigninfo['user_id']) {
					$userwantexp = explode(',',$userwantinfo['games']);
					$a = 0;
					foreach($userwantexp as $key => $userwantgm) {
						print_r('<a href="gamedetail.php?game_id='.$userwantgm.'">'.$gamestitle[$userwantgm]['title'].'</a><br />');
						
						if(++$a == 5) break;
					}
				}
			}

			echo '</td><td>';

			// offer
			foreach($useroffarr as $key => $useroffinfo) {
				if($useroffinfo['user_id'] == $usersigninfo['user_id']) {
					$useroffexp = explode(',',$useroffinfo['games']);
					$a = 0;
					foreach($useroffexp as $key => $useroffgm) {
						print_r('<a href="gamedetail.php?game_id='.$useroffgm.'">'.$gamestitle[$useroffgm]['title'].'</a><br />');
						
						if(++$a == 5) break;
					}
				}
			}

			echo '</td></tr>';

			if(++$l == 5) break;
		}

		echo '	</tbody>
			</table>
		</div> <!-- col-sm-12 user line -->

	';
			
	// RECENT TRADES
	
	$recenttrades = 'SELECT want_games, offer_games, user1, user2, user1accept, user2accept FROM game_offer WHERE user1accept="yes" and user2accept="yes"';
	$recenttradesqy = mysqli_query($link, $recenttrades);
	
	while($recenttradesarry = mysqli_fetch_array($recenttradesqy)) {
		$rectradarr[] = $recenttradesarry;
	}
	
	echo '<div class="col-sm-12 clearfix">
				<div align="center" class="wantedhead">RECENT TRADES</div>
			<table class="table">
				<tbody>
					<tr><td><strong>User</strong></td><td><strong>Traded</strong></td><td><strong>To</strong></td><td><strong>For</strong></td></tr>';

	echo '<tr>';
	
	foreach($rectradarr as $key => $gameinfo) {
		$user1 = $gameinfo['user1'];
		echo '<td>';			
		foreach($usersignarr2 as $key => $user1id) {
			if($user1 == $user1id['user_id']) {
				print_r('<a href="profile.php?id='.$user1.'">'.$user1id['username'].'</a>');
			}
		}

		echo '</td><td>';

		$gminfwantexp = explode( ',', $gameinfo['want_games']);
		foreach($gminfwantexp as $key1 => $gmwantid) {
			print_r('<a href="gamedetail.php?game_id='.$gmwantid.'">'.
					$gamestitle[$gmwantid]['title'].'</a><br />'
				);
			if(++$b == 5) break;
		}

		echo '</td><td>';

		$user2 = $gameinfo['user2'];
		foreach($usersignarr2 as $key => $user2id) {
			if($user2 == $user2id['user_id']) {
				print_r('<a href="profile.php?id='.$user2.'">'.$user2id['username'].'</a>');
			}
		}

		echo '</td><td>';

		$gminfhaveexp = explode( ',', $gameinfo['offer_games']);
		foreach($gminfhaveexp as $key2 => $gmoffid) {
			print_r('<a href="gamedetail.php?game_id='.$gmwantid.'">'.
					$gamestitle[$gmoffid]['title'].'</a><br />'
				);
			if(++$c == 5) break;
		}

		echo '</td></tr>';

		if(++$k == 5) break;
	}

	echo '</tbody>
			</table>
		</div> <!-- col-sm-6 recent trades table -->';



}

	echo '</div> <!-- row -->
	</div> <!-- container -->
		';
	


echo "<br /><br />";
			
echo		'	</div>
			</div> <!-- gamesdisplay col-sm-9 -->';
		
// echo 	'</div> <!-- row -->
// 	</div> <!-- container -->';


?>

<script>
	$('.myButton').click(function(){
    if ( this.value === 'collapse' ) {
        // if it's open close it
        open = false;
        this.value = 'expand';
        $(this).next("div.containerconsole").hide("slow");
    }
    else {
        // if it's close open it
        open = true;
        this.value = 'collapse';
        $(this).siblings("[value='collapse']").click();
        $(this).next("div.containerconsole").show("slow");
    }
	});
 </script>



<?php include 'footer.php'; ?>
