//When checkboxes/radios checked/unchecked, toggle background color 

$('.form-group').on('click','input[type=radio]',function() {
	$(this).closest('.form-group').find('.radio-inline, .radio').removeClass('checked')
	$(this).closest('.radio-inline, .radio').addClass('checked'); });
$('.form-group').on('click','input[type=checkbox]',function() {
		$(this).closest('.checkbox-inline, .checkbox').toggleClass('checked');
});