<?php include 'base.php';

$title = "Gamecycler | Conclusion of Game Trade";
$description = "After paying Gamecycler's Fees, we remove the games and offers from your databases";
$keywords = "trade games, gamers, list games, find games, nintendo, xbox, sega, playstation, sony";

include 'header.php'; ?>

<body>

<?php

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

	$id = $_SESSION['id'];

	/************************************************************* 

		REMOVE GAMES FROM USER1 AND USER2's WANT AND HAVE DBs 

	**************************************************************/

	// get trade details
	$gettrade = mysqli_query($link, "SELECT * FROM game_offer WHERE id='".$id."'");
	$gettraderow = mysqli_fetch_array($gettrade);

	// assign user1want and user2have by trade row and games
	$user1want = $gettraderow['user1'];
	$user2have = $gettraderow['user2'];
	$user1gowantgm = $gettraderow['want_games'];
	$user2gohavegm = $gettraderow['offer_games'];

	// get user1games to remove
	$user1wantgames = mysqli_query($link, "SELECT * FROM userwantgames WHERE user_id='".$user1want."'");
	$u1wgmfet = mysqli_fetch_array($user1wantgames);
	$user1havegames = mysqli_query($link, "SELECT * FROM userhavegames WHERE user_id='".$user1want."'");
	$u1hgmfet = mysqli_fetch_array($user1havegames);

	mysqli_free_result($user1wantgame);
	mysqli_free_result($user1havegames);

	// get user2games to remove

	$user2wantgames = mysqli_query($link, "SELECT * FROM userwantgames WHERE user_id='".$user2have."'");
	$u2wgmfet = mysqli_fetch_array($user2wantgames);
	$user2havegames = mysqli_query($link, "SELECT * FROM userhavegames WHERE user_id='".$user2have."'");
	$u2hgmfet = mysqli_fetch_array($user2havegames);

	mysqli_free_result($user2wantgames);
	mysqli_free_result($user2havegames);

	// call user1-want from game_offer table (want_games) and explode user 2 have games
	$u1gowgm = explode(',',$user1gowantgm);
	$u1wgm = explode(',',$u1wgmfet['games']);

	// remove user1gowantgm from user2havegames
	$u2hgm = explode(',',$u2hgmfet['games']);

	// see where user1go and want diff
	$u1wgmul = implode(',',array_diff($u1wgm, $u1gowgm));

	// where user1go and user2have diff
	$u2hgmul = implode(',',array_diff($u2hgm, $u1gowgm));

	// update user1want games and user2havegames
	mysqli_query($link, "UPDATE userwantgames SET games='".$u1wgmul."' WHERE user_id='".$user1want."'");
	mysqli_query($link, "UPDATE userhavegames SET games='".$u2hgmul."' WHERE user_id='".$user2have."'");

	// call user2-offer from game_offer table (offer_games) and explode user 2 want games
	$u2gowgm = explode(',',$user2gohavegm);
	$u2wgm = explode(',',$u2wgmfet['games']);

	// explode user1have
	$u1hgm = explode(',',$u1hgmfet['games']);

	// user2-offer and user2 want diff
	$u2wgmul = implode(',',array_diff($u2wgm, $u2gowgm));

	// user2-offer and user1 have diff
	$u1hgmul = implode(',',array_diff($u1hgm, $u2gowgm));

	// update user2want games and user1have games
	mysqli_query($link, "UPDATE userwantgames SET games='".$u2wgmul."' WHERE user_id='".$user2have."'");
	mysqli_query($link, "UPDATE userhavegames SET games='".$u1hgmul."' WHERE user_id='".$user1want."'");

	// set trade_offer to YES / YES and update finaldate
	mysqli_query($link, "UPDATE game_offer SET user1accept='YES', user2accept='YES', finaldate=now() WHERE id='".$id."'");

	/********************************************

		CHECK OTHER ROWS FOR GAMES AND CANCEL

	********************************************/

	// REFERENCE 
	// $user1want = $gettraderow['user1'];
	// $user2have = $gettraderow['user2'];
	// $user1gowantgm = $gettraderow['want_games'];
	// $u1gowgm = explode(',',$user1gowantgm);
	// $user2gohavegm = $gettraderow['offer_games'];
	// $u2gowgm = explode(',',$user2gohavegm);


	$gameoffer = mysqli_query($link, "
		SELECT * FROM game_offer 
		WHERE id <> $id
		AND user1accept = ''
		AND ( user1 IN ('$user1want', '$user2have') OR user2 IN ('$user1want', '$user2have') )
 		");

		// ( (user1='".$user1want."' OR user2='".$user1want."' ) OR (user1='".$user2have."' OR user2='".$user2have."') ) AND id!='".$id."'");

	// go through each trade
	while($gofet = mysqli_fetch_array($gameoffer)) {
		$gameoffarr[] = $gofet;

		$goid =	$gofet['id'];
		$u1gofet = $gofet['user1'];
		$u2gofet = $gofet['user2'];
		$u1wantgm = $gofet['want_games'];
		$u2wantgm = $gofet['offer_games'];

		// WHERE USER1 IS COMPARED TO OTHER USER1 AND USER2 IS COMPARED TO OTHER USER2

		// determine if user1 in trade is same as user1 in while loop
		if($user1want == $u1gofet) {
			// explode u1gofet games
			$u1gofetexp = explode(',',$u1wantgm);

			// if intersect with u1wantgm
			if(array_intersect($u1gowgm, $u1wgm) != NULL) {
				mysqli_query($link, "UPDATE game_offer SET user1accept='NO', user2accept='NO' WHERE id=$goid");	
			}
		}

		if($user2have == $u2gofet) {
			//explode u2gofet games
			$u2gofetexp = explode(',',$u2wantgm);

			//if intersect with u2havegm
			if(array_intersect($u2gofetexp, $u2gowgm) != NULL ) {
				mysqli_query($link, "UPDATE game_offer SET user1accept='NO', user2accept='NO' WHERE id= $goid");
			}
		}

		// IF USER1 IS IN USER2 AND USER2 IS IN USER1

		// determine if user1 in trade is same as user1 in while loop
		if($user1want == $u2gofet) {
			// explode u1gofet games
			$u2gofetexp = explode(',',$u2wantgm);

			// if intersect with u1wantgm
			if(array_intersect($u2gofetex, $u1wgm) != NULL) {
				mysqli_query($link, "UPDATE game_offer SET user1accept='NO', user2accept='NO' WHERE id=$goid");	
			}
		}

		if($user2have == $u1gofet) {
			//explode u2gofet games
			$u1gofetexp = explode(',',$u1wantgm);

			//if intersect with u2havegm
			if(array_intersect($u1gofetexp, $u2gowgm) != NULL) {
				mysqli_query($link, "UPDATE game_offer SET user1accept='NO', user2accept='NO' WHERE id=$goid");
			}
		}

	}


	include 'nav.php';

	?>

	<div class='col-sm-12' id='thankyou'>
		<div style="text-align: center;">
		<img src='images/gamecycler-500x375.gif' align='center'>
		</div>
		<div id="tyhead" class="gameshead text-center" style="padding-bottom: 5px; margin: 15px 0;">
			<h2>Your trade is complete</h2>
		</div>
		<div id="tybody">	
			<p>You will be receiving shipping information shortly.</p>

			<p>Since Gamecycler is in BETA mode, there may be a slight delay in payment and shipping information as we double and triple check everything to make sure Gamecycler handled your trade properly.</p>
			<p>Thank you for being one of the first users of Gamecycler. It's because of gamers like you that will allow us to keep bettering our platform and keep inabiling you to feed your gaming needs.</p> 
			<p><a href="home.php"><i class="glyphicon glyphicon-backward" style="margin-left: 7px; margin-top: 15px; margin-right: 7px;"></i>Back to Home<a>
		</div>
	</div><!-- thankyou -->

	</div>
	</div>
	</div>
	</div>

	<?php
		require 'PHPMailer/PHPMailerAutoload.php';

		$mail = new PHPMailer;

		//Enable SMTP debugging
		// 0 = off (for production use)
		// 1 = client messages
		// 2 = client and server messages
		$mail->SMTPDebug = 2;
		//Ask for HTML-friendly debug output
		$mail->Debugoutput = 'html';                              // Enable verbose debug output

		// $mail->isSMTP();                                      // Set mailer to use SMTP
		$mail->Host = 'mail.gamecycler.com';  // Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username = 'info@gamecycler.com';                 // SMTP username
		$mail->Password = 'Eckpfs96$$';                           // SMTP password
		$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
		$mail->Port = 587;                                    // TCP port to connect to

		$mail->setFrom('info@gamecycler.com', 'Info');
		$mail->addAddress('gamecycler@gmail.com', 'Gamecycler');     // Add a recipient
		// $mail->addAddress('ellen@example.com');               // Name is optional
		// $mail->addReplyTo('gamecycler@gmail.com', 'Gamecycler.com');
		//$mail->addCC('cc@example.com');
		//$mail->addBCC('bcc@example.com');

		//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
		//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
		$mail->isHTML(true);                                  // Set email format to HTML

		$mail->Subject = 'Completed Trade';
		$mail->Body    = $_SESSION['id'].' is completed. Games '.$u1gowgm.' and '.$u2gowgm;
		// $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

		if(!$mail->send()) {
		    // echo 'Message could not be sent.';
		    // echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
		    // echo 'Message has been sent';
		}

		// echo "Thank you for your feedback.";

		echo '<meta http-equiv="refresh" content="9;home.php">';


	unset($_SESSION['id']);
	unset($_SESSION['u1wu2h']);
	unset($_SESSION['u1hu2w']);
	unset($_SESSION['haveuser']);

}
else
{
	?>
    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
    Please Log in!
    <?php
}


 include 'footer.php'; ?>