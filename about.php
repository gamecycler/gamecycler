<? include 'base.php';

$title = "Gamecycler | About Gamecycler";
$description = "What is Gamecycler all about?";
$keywords = "trading,about,business,startup,games,gamers,shared economy,trading games";

include 'header.php'; ?>

<body>

<div class="container">
	<div class="row">
		<div class="col-sm-8 col-sm-offset-2 text-center" id="aboutmain">
		<img src="images/gamecycler-500x375.gif">
		<div class="gameshead" style="padding-bottom: 5px; margin-top: 10px;">
			<h2 class="text-center">Thank You For Using Gamecycler!</h2>
		</div>
		<div class="abouttext text-left">
		<p>Growing up in the 1980s, I was able to witness the rise of the home gaming system.</p>
		<p>The kids in my neighborhood in Miami, Florida, would spend hot afternoons running to each other's apartments finding which game console we wanted to play for the day. Whether it was the Nintendo my downstairs neighbor had or those times we jumped fences to get to the neighborhood across the street to play Altered Beast on a Sega Master System -- video games were an important part of my childhood.</p>
		<p>Even though, my family couldn't quite afford the more recent systems my brother and I worked the hell out of our Atari 2600, even though I was envious of everyone else owning SNES or Sega Dreamcasts.</p>
		<p>Around 2000, I finally had enough money to buy my own game system, a Playstation 2. I can shamefully admit that I was never quite the gamer that my nerdiness demanded, but nonetheless, I tried tearing through game after game like a good little gamer should.</p>
		<p>This led to having quite a collection, but also a lot of games that I'd beaten and weren't doing more than taking up space in my entertainment center.</p>
		<p>Time after time I would haul my games back to the store to be bought back, only to be left disappointed that I would walk away with nothing but a small pittance to be used to buy other games that were ten times more expensive. It didn't <i>feel</i> fair.</p>
		<p>That's when I came up with the idea for Gamecycler. The premise being that games could be traded between gamers, and that the valuation system that used game stores use to buy back games could be used as a way to subjectively let gamers know how much each game would be worth to each other.</p>
		<p>Of course, back then it was nothing but an idea.</p>
		<p>Three months ago, I decided to build Gamecycler from scratch. I didn't have much of a programming background, but I figured why not?</p>
		<p>What started off as a simple project is now a (hopefully) fully functioning web application. It only took dedication and work, but I can comfortably say that it's something that anyone could've done with the right amount of will and desire.</p>
		<p>For all the gamers that happen across this site, I sincerely hope that you enjoy it, and it helps keep a few extra bucks in your pocket while bringing more games into your lives.</p>
		<p>Sincerely,</p>
		<h2>Ric Delgado</h2>

		<strong>email:</strong> ric [at] gamecycler.com
		</div>



		</div> <!-- about main -->
	</div> <!-- row -->
</div> <!-- container -->

<?php include 'footer.php'; ?>