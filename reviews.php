<? include 'base.php';

$title = "";
$description = "";
$keywords = "";

include 'header.php'; ?>



<body>

<?php

// STAR RATING USER $a = new starrat1(); then $a->starrat1( -- rating variable from db -- )
	class starrat
		{
			function starrat1($rat)
			{
				if($rat == 1) {
	                echo "<span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 2 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 3 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 4 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 5 ) {   
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } else {
	            	echo "Uh oh.";
	            }

			}
		}

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

	
	$review = "SELECT m1.review_id, m1.game_offer_id, m1.reviewer, m1.reviewee, m1.rating, m1.review, m1.timestamp, m2.id, m2.user1, m2.user2, m2.want_games, m2.offer_games, m2.user1value, m2.user2value, m2.finaldate, m2.user1accept, m2.user2accept, u1.user_id as u1id, u1.username as u1username, u1.email as u1email, u2.user_id as u2id, u2.username as u2username, u2.email as u2email FROM userreviews as m1, game_offer as m2, db_users as u1, db_users as u2 WHERE u1.user_id=m1.reviewer AND u2.user_id=m1.reviewee AND m1.game_offer_id=m2.id ORDER BY m2.finaldate DESC";
	//$review = "SELECT u1.user_id as user1, u1.username as user1un, u1.email as u1email, u2.user_id as user2, u2.username as user2un, u2.email as u2email, m2.id as goid, m2.user1 as gou1, m2.user2 as gou2, m2.want_games as gowant, m2.offer_games as gooff, m1.review_id as urrevid, m1.game_offer_id as urgmoffid, m1.reviewer as reviewer, m1.reviewee as reviewee, m1.rating as rating, m1.review as review, m1.timestamp as commtime FROM userreviews as m1, game_offer as m2, db_users as u1, db_users as u2 WHERE user1=reviewer AND user2=reviewee AND urgmoffid=goid";
	 // AND urgmoffid=goid
	//  
	$reviewqy = mysqli_query($link, $review);

	$user1 = $_SESSION['user_id'];

	while($reviewfet = mysqli_fetch_array($reviewqy,MYSQLI_ASSOC)) {
		$reviewarr[] = $reviewfet;
	}

	// echo "<pre>";
	// print_r($reviewarr);
	// echo "</pre>";

	$rating = mysqli_query($link, "SELECT SUM(rating) as sum, COUNT(rating) as count FROM userreviews WHERE reviewee='".$user1."'");
	$ratingdata = mysqli_fetch_array($rating);
	$ratingsum = $ratingdata['sum'];
	$ratingcount = $ratingdata['count'];

	$ratingtot = round($ratingsum/$ratingcount,0,PHP_ROUND_HALF_UP);

	$gamesqy = mysqli_query($link, "SELECT game_id, title FROM db_games");
	while($gamesfet = mysqli_fetch_array($gamesqy)) {
		$gamesfetid = $gamesfet['game_id'];
		$gamestable[$gamesfetid] = $gamesfet;
	}

	?>
	<script>
	$('#myTabs a').click(function (e) {
	  e.preventDefault()
	  $(this).tab('show')
	})
	</script>

	<?php include 'nav.php'; ?>

	<div class="col-sm-12 reviewhead"><h2><strong>REVIEWS</strong></h2></div>

	<div class="container">
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#yourfeedback" aria-controls="yourfeeback" role="tab" data-toggle="tab">Your Feedback</a></li>
			<li role="presentation"><a href="#yourcomments" aria-controls="yourcomments" role="tab" data-toggle="tab">Your Comments</a></li>
			<li role="presentation"><a href="#leavefeedback" aria-controls="leavefeeback" role="tab" data-toggle="tab">Leave Feedback</a></li>
		</ul>
	    	

	    	<div class="row tab-content">
			<div role="tabpanel" class="tab-pane fade in active" id="yourfeedback">
			
	<?php

	echo '<div class="overallstar"><strong>Overall Rating: </strong>';

	$rev = new starrat();
	$rev->starrat1($ratingtot);

	echo '</div>';

	foreach($reviewarr as $revkey => $revdata) {
		if($revdata['reviewee'] == $user1) {

		echo '	<div class="col-sm-12 reviewent">
			 		<div class="row">
			 			<div class="col-sm-2 revproimg text-center">';
			 				// GRAVATAR CODE
						        $default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
						        $size1 = 120;
						        $email1 = $revdata['u1email'];

						        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
        
        echo '				<a href="profile.php?id='.$revdata['u1id'].'"><img src="'. $grav_url1 .'"></a><br />';
        print_r('			<h3><strong><a href="profile.php?id='.$revdata['u1id'].'">'.$revdata['u1username'].'</a></strong></h3>');
                		

		echo '			</div> <!-- profile pic -->
						<div class="col-sm-10"> <!-- container for all info -->
							<div class="row">
								<div class="col-sm-2">';

						// print_r($revdata);
							
									

			echo '				</div> <!-- stars -->
								<div class="col-sm-2">';

									
		echo '					</div> <!-- username -->
								<div class="col-sm-4 text-right">';

									
		echo '					</div> <!-- date of review -->
								<div class="col-sm-12 revreview">
									<div class="col-sm-5">';

										$wantgmexp = explode(',',$revdata['want_games']);
										foreach($wantgmexp as $wantgmid) {
											print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$wantgmid]['title'].'<br />');
										}

		echo '						</div> <!-- trade 1 game -->
									<div class="col-sm-2"><h3><strong>FOR</strong></h3></div>
									<div class="col-sm-5">';

										$offergmexp = explode(',',$revdata['offer_games']);
										foreach($offergmexp as $offergmid) {
											print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$offergmid]['title'].'<br />');
										}

		echo '						</div> <!-- trade 2 game -->
								</div> <!-- games trade area -->
								<div class="col-sm-12 revreview"><strong>Rating: </strong>';

									$rating = $revdata['rating'];

									$rev = new starrat();
									$rev->starrat1($rating);

		print_r('					<br /><strong>Review: </strong>'.$revdata['review'].' - <i style="font-size: .8em;">'.date('m-d-Y',strtotime($revdata['timestamp'])).'</i>');

		echo '					</div> <!-- review -->
							</div> <!-- row for info -->
						</div> <!-- col-sm-10 for all info -->
					</div> <!-- main row -->
				</div> <!-- entire review section -->';

		} // if reviewee is user1

	} // foreach $reviewarr

	// echo '</div></div> <!-- container and row -->';

	?>
				
			</div>


			<div role="tabpanel" class="tab-pane fade in" id="yourcomments">
			<div class="col-sm-12" style="padding-left: 0;">
				<div class="overallstar"><strong>Reviews You've Made</strong></div>
					<div class="row commleftrow">
						<!-- 
						
						</div>  col-sm-12 header -->
					
			<?php	foreach($reviewarr as $revkey => $revdata) {

						if($revdata['reviewer'] == $user1) {
							$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
					        $size1 = 120;
					        $email1 = $revdata['u2email'];

					        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
        			
    		echo '	<div class="col-sm-12 commtile">
    					<div class="row">
		    				<div class="col-sm-2 commleft text-center">
		        				<a href="profile.php?id='.$revdata['u2id'].'"><img src="'. $grav_url1 .'"></a><br />
		        				<h3><strong>'.$revdata['u2username'].'</strong></h3>
		        			</div>
		        			<div class="col-sm-10" style="padding-top: 30px;">
		        				<div class="row">
			        				<div class="col-sm-5" style="padding-left: 5px">';
				        				$gamewtexp = explode( ',',$revdata['want_games']);
										foreach($gamewtexp as $gamewtid) {
											print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gamewtid]['title']."<br />");
										}
			echo '					</div>
									<div class="col-sm-2">
										<h3><strong>FOR</strong></h3>
									</div>
									<div class="col-sm-5">';
										$gameoffexp = explode( ',',$revdata['offer_games']);
										foreach($gameoffexp as $gameoffid) {
											print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title']."<br />");
										}
        	echo '					</div>
		        					<div class="col-sm-6 commst"><strong>Rating: </strong>';
				        				$ratingtot = $revdata['rating'];
				        				if($ratingtot == 1) {
												echo "<span class='glyphicon glyphicon-star'></span>";
											} elseif($ratingtot == 2) {
												echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
											} elseif($ratingtot == 3) {
												echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
											} elseif($ratingtot == 4) {
												echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
											} else {
												echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
											}

			echo '					</div>
									<div class="col-sm-6 commst">';
			
			echo '					</div>
									<div class="col-sm-12">		
										<strong>Review:</strong> ';
										
			print_r(						$revdata['review'] .' - <i style="font-size: .8em;">'.date('m-d-Y',strtotime($revdata['finaldate'])).'</i>');
        	echo '					</div> <!-- col-sm-12 rating section -->
        						</div> <!-- row -->
        					</div> <!-- col-sm-10 rev info section -->
        				</div><!-- row -->
        			</div> <!-- col-sm-12 -->';
			
						} // if reviewee is user1
					} // foreach reviewarr
				?>
				</div> <!-- main row wrap -->
			</div> <!-- col-sm-12 main section wrap -->
				
		</div>

		<div role="tabpanel" class="tab-pane fade in" id="leavefeedback">	

	<?php
			$gameoff = "SELECT * FROM game_offer WHERE user1accept='YES' AND (user1='".$user1."' OR user2='".$user1."')";
			$userrev = "SELECT * FROM userreviews WHERE reviewer='".$user1."'";
			
			$gameoffqy = mysqli_query($link, $gameoff);
			$userrevqy = mysqli_query($link, $userrev);
			
			while($gameofffet =  mysqli_fetch_array($gameoffqy,MYSQLI_ASSOC)) {
				$gmoffid = $gameofffet['id'];
				$gameoffarr[$gmoffid] = $gameofffet;
			}

			while($userrevfet = mysqli_fetch_array($userrevqy,MYSQLI_ASSOC)) {
				$uofid = $userrevfet['game_offer_id'];
				$userrevarr[$uofid] = $userrevfet;
			}
			
			$users = "SELECT user_id, username, email FROM db_users";
			$usersqy = mysqli_query($link, $users);

			while($usersfet = mysqli_fetch_array($usersqy)) {
				$usersid = $usersfet['user_id'];
				$usersarr[$usersid] = $usersfet;
			}

	echo '<div class="overallstar" style="margin-bottom: 15px;"><strong>Recent Trades That Need a Review</strong></div>';

	foreach($userrevarr as $urkey => $urdata) {
			$urdatid = $urdata['game_offer_id'];
			$compur[] = $urdata['game_offer_id'];
		}

	foreach($gameoffarr as $gokey => $godata) {
			$godatid = $godata['id'];
			$compgo[$godatid] = $godata['id'];
		}

		$usercomp = array_diff($compgo, $compur);

		foreach($usercomp as $rowid) {
			$godata = $gameoffarr[$rowid];
			if($user1 == $godata['user1']){
				$user2 = $godata['user2'];
			} elseif ($user1 == $godata['user2']) {
				$user2 = $godata['user1'];
			}

			$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
			$size1 = 120;
			$email1 = $usersarr[$user2]['email'];

			$grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
 	
echo '			<div class="col-sm-12 commtile"> <!-- leave feedback container -->
					
					<div class="row"> <!-- leave feedback row -->';

 	echo '				<div class="col-sm-2 commleft text-center"> <!-- prof image and username -->
 							<a href="profile.php?id='.$godata['user2'].'"><img src="'. $grav_url1 .'"><br />
 							<h3><strong>';
 							
								print_r($usersarr[$user2]['username']);
							
	echo '					</a></strong></h3>
 						</div>
 						<div class="col-sm-10"> <!-- table for games listing -->
 							<div class="row"> <!-- row container for games listing -->
 								<div class="col-sm-12"><strong>Trade Date: </strong> <!-- timestamp -->';
 									print_r(date('m-d-Y', strtotime($godata['finaldate'])));
 	echo '						</div>
 								<div class="col-sm-5"> <!-- want games -->';
 									$wantgmexp = explode(',',$godata['want_games']);
									foreach($wantgmexp as $wgkey => $wgid) {
										print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$wgid]['title'].'<br />');
									}
 	echo '						</div> <!-- want games -->
 								<div class="col-sm-2"><h3><strong>FOR</strong></h3></div>
 								<div class="col-sm-5"> <!-- offer games -->';
 									$offergmexp = explode(',',$godata['offer_games']);
									foreach($offergmexp as $ogkey => $ogid) {
										print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$ogid]['title'].'<br />');
									}
 	echo '						</div> <!-- offer games -->
 							</div> <!-- row container for games listing -->
 						</div> <!-- table for games listing -->
 						<div class="col-sm-12"> <!-- stars and feedback -->
 							<div class="row"> <!-- row for stars and feedback -->
 								<div class="col-sm-3"> <!-- star select -->';
 									$urdataid = $godata['id'];	?>
						
							<form action="revsub.php" method="post">
							<strong>Rating:</strong><br />
								<input type="radio" name="rating[<?php echo $urdataid; ?>]" value="1"><span class='glyphicon glyphicon-star'></span><br />
								<input type="radio" name="rating[<?php echo $urdataid; ?>]" value="2"><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><br />
								<input type="radio" name="rating[<?php echo $urdataid; ?>]" value="3"><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><br />
								<input type="radio" name="rating[<?php echo $urdataid; ?>]" value="4"><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><br />
								<input type="radio" name="rating[<?php echo $urdataid; ?>]" value="5" checked><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>
	<?php
	echo '						</div> <!-- star select -->
								<div class="col-sm-6"> <!-- review entry -->
									<strong>Review:</strong><br />
									<textarea style="width: 100%; height: 100px;" name="review['.$urdataid.";".$user2.']" required></textarea>
								</div> <!-- review entry -->
								<div class="col-sm-3"> <!-- submit -->
									<input type="submit" value="Submit">
									</form>
	 							</div> <!-- submit -->
							</div> <!-- row for stars and feedback -->
 						</div> <!-- stars and feedback row -->
 					</div> <!-- leave feedback row -->
 				</div> <!-- leave feedback container -->';
			
				// break;

				// echo "</div>";
					
			//	} // foreach games_offer
			} // foreach userreviews
	?>
							

	<?php

	
	mysqli_free_result($gamesqy);

// }

	echo "</div></div></div></div></div></div></div>";

}
else
{
	?>
    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
    Please Log in!
    <?php
}


	?>

<?php include 'footer.php'; ?>