<?php include 'base.php'; 

$title = "Gamecycler | All of Gamecycler's Users";
$description = "All of users on Gamecycler";
$keywords = "video games, nintendo, playstation, xbox, xbox one, xbox360, atari, sega, genesis, gamers, trade games";

include 'header.php';

?>

<body>

<?php

// STAR RATING USER $a = new starrat1(); then $a->starrat1( -- rating variable from db -- )
	class starrat
		{
			function starrat1($rat)
			{
				if($rat == 1) {
	                echo "<span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 2 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 3 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 4 ) {
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } elseif($rat == 5 ) {   
	                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
	            } else {
	            	echo "<i>No Rating Yet</i>";
	            }

			}
		}

$user1 = $_SESSION['user_id'];

## SECTION FOR TITLE LISTING
// Call entire userhavegames DB with no ID reference
$alluserhavedb = "SELECT * FROM userhavegames";
$alluserwantdb = "SELECT * FROM userwantgames";

// User 1 have and want games
$u1havedb = "SELECT games FROM userhavegames WHERE user_id='".$user1."'";
$u1wantdb = "SELECT games FROM userwantgames WHERE user_id='".$user1."'";

$u1haveqry = mysqli_query($link, $u1havedb);
$u1wantqry = mysqli_query($link, $u1wantdb);

$u1havearr = mysqli_fetch_array($u1haveqry);
$u1wantarr = mysqli_fetch_array($u1wantqry);

$u1havegmid = $u1havearr['games'];
$u1wantgmid = $u1wantarr['games'];

$u1havegmarr = explode( ',', $u1havegmid);
$u1wantgmarr = explode( ',', $u1wantgmid);

// Fetch arrays in userhavegames, assign each row into userhave[] variable, print upon each iteration

$alluserhavequery = mysqli_query($link, $alluserhavedb);
$alluserwantquery = mysqli_query($link, $alluserwantdb);

while($alluserhavearray = mysqli_fetch_array($alluserhavequery,MYSQLI_ASSOC)) {
	$auhuid = $alluserhavearray['user_id'];
	$auharr[$auhuid] = $alluserhavearray;
}

while($alluserwantarray = mysqli_fetch_array($alluserwantquery,MYSQLI_ASSOC)) {
	$auwuid = $alluserwantarray['user_id'];
	$auwarr[$auwuid] = $alluserwantarray;
}

// Query to pull up all games information from db_games
$gamesdblist = "SELECT * FROM db_games";
$gameslist = mysqli_query($link, $gamesdblist);

// create game refernce table
while($gamesarray = mysqli_fetch_array($gameslist,MYSQLI_ASSOC)) {
			$gamestable[] = $gamesarray; // place entire gamewantarray into a numbered array to reference individual results
		} // put $gamelist query into a gamelistarray

## END SECTION

echo '
	<div class="container" id="usertable">
		<div class="row">
			<div class="col-sm-12" id="mainuserwrap">
				<div class="row">
					<div class="col-sm-12 text-center"><h1><strong>All Users on Gamecycler</strong></h1></div>
	';

$req = mysqli_query($link, 'select user_id, firstname, lastname, username, email, city, state from db_users');
while($dnn = mysqli_fetch_array($req))
{
	
if($dnn['user_id'] == $_SESSION['user_id'] || $dnn['user_id'] == "1") {
	
}
else {
// GRAVATAR CODE

// Gravtar Image Settings
$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
$size1 = 175;	
$email1 = $dnn['email'];
$lastn = substr($dnn['lastname'], 0, 1);

$grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;

$user2 = $dnn['user_id'];

echo '		<div class="col-sm-2 userbox">
				<div class="row">
					<a href="profile.php?id='.$user2.'"><div class="col-sm-10 col-sm-offset-1 pull-center" id="usersumgrav">
						<img src="'. $grav_url1 .'" alt="" />
					</div> <!-- image block -->
					<div class="col-sm-12 text-center gameshead">
						<h4>'.$dnn['username'].'</a><br /></h4></div>
					<div class="col-sm-12 text-center">';

						$user2rev = mysqli_query($link, "SELECT sum(rating) as sum, count(rating) as count from userreviews where reviewee='$user2'");
						$u2rating = mysqli_fetch_array($user2rev);
						$revsum = $u2rating['sum'];
						$revct = $u2rating['count'];


						if($revct == 0) {
							$revtot = 0;
						} else {
							$revtot = round($revsum/$revct,0,PHP_ROUND_HALF_UP);
						}

						$rev = new starrat();
						$rev->starrat1($revtot);

echo 						'<br />'.$dnn["city"].', '.$dnn["state"].'<br /><br />
					</div> <!-- info block -->
				</div> <!-- row -->
				<div class="row">
					<div class="col-lg-12 col-md-12">
				';

				########################################################
				##   U1 WANT COMPARED TO ALL OTHER USERS HAVE LOOP    ##
				########################################################

				if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

					$user2un = $dnn['user_id'];
					
					// User 1 Want and User 2 Have
					foreach($auharr as $title => $auhtabarr) {
						if($auhtabarr['user_id'] == $user2un){
							$auhtgameid = $auhtabarr['games'];			
						}
					}
					//print_r($auhtgameid);
					$auhexp = explode( ",",$auhtgameid);
					$u1wu2h = array_intersect($u1wantgmarr,$auhexp);
					//print_r($auhtgameid);
					if($auhtgameid != "0") {
						
						if(!empty($u1wu2h)) {
							echo "<strong>You Want<br /></strong>";
							foreach($u1wu2h as $title => $gameauhexpid){
								print_r($gamestable[$gameauhexpid]['title'].'<br />');
							} 
							
							// User 2 Want to User 1 Have
							foreach($auwarr as $title => $auwtabarr) {
								if($auwtabarr['user_id'] == $user2un){
									$auwtgameid = $auwtabarr['games'];
								}
							}
							
							$auwexp = explode( ",",$auwtgameid);
							$u2wu1h = array_intersect($auwexp,$u1havegmarr);
							if($auwtgameid != "0"){
								echo "<br /><br />
									<strong>".$dnn['username']." Wants</strong><br />";
								foreach($u2wu1h as $title => $gameauwexpid){
									print_r($gamestable[$gameauwexpid]['title'].'<br />');
								}
							}
							
						} // if there is no results from the u1w2uh comparison
						else 
						{
							echo '<i>'.$dnn['username'].' has no games that you want.</i>';
						}

					} // if all users have table arr game id is 0

				} // if not logged in 
				else 
				{ }

				
				
			echo '</div> <!-- games-list -->
				</div> <!-- row -->
			</div> <!-- top div -->';


	}		
}

echo '				</div> <!-- second row -->
				</div> <!-- col-sm-12 main wrap -->
			</div> <!-- row -->
		</div> <!-- container -->';
	

?>


<?php include 'footer.php'; ?>