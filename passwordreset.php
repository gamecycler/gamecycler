<?php include 'base.php';

$title = "Gamecycler | Password Reset";
$description = "Reset Password";
$keywords = "password reset";

include 'header.php'; ?>

<body style="margin-top: 60px;">

<?php


if(isset($_POST['password'])) {

	$userid = $_SESSION['user_id'];
	$password = $_POST['password'];

	 // A higher "cost" is more secure but consumes more processing power
	$cost = 10;

	// Create a random salt
	$salt = strtr(base64_encode(mcrypt_create_iv(16, MCRYPT_DEV_URANDOM)), '+', '.');

	// Prefix information about the hash so PHP knows how to verify it later.
	// "$2a$" Means we're using the Blowfish algorithm. The following two digits are the cost parameter.
	$salt = sprintf("$2a$%02d$", $cost) . $salt;

	// Value:
	// $2a$10$eImiTXuWVxfM37uY4JANjQ==

	// Hash the password with the salt
	$hash = crypt($password, $salt);

	$updatepword = mysqli_query($link, "UPDATE db_users SET hash='$hash' WHERE user_id='$userid'");

	echo '<h2>Your password has been updated.</h2>
		<meta http-equiv="refresh" content="7;login.php">';

}

elseif(isset($_GET['reset'])) {

	$reset = $_GET['reset'];
	$email = $_GET['email'];

	$recoverychkemail = mysqli_query($link, "SELECT user_id, email FROM db_users WHERE email='".$email."'");
	$recoverychk = mysqli_query($link, "SELECT * FROM db_users WHERE recovery='".$reset."'");

	if(mysqli_num_rows[$recoverychkemail] > 1) {
		echo "You have more than one account registered to this email. Please contact support to resolve this issue before we can proceed: info [at] gamecycler.com.";
	} else {
?>
		<script>
			function checkPasswordMatch() {
			    var password = $("#password").val();
			    var confirmPassword = $("#passwordconfirm").val();

			    if (password != confirmPassword) {
			    	$("#buttonActivate").prop("disabled", true);
			        $("#divCheckPasswordMatch").html("Passwords do not match!");
			    	
			    	// return false;
			    	}
			    else {
			    	$("#buttonActivate").prop("disabled", false);
			        $("#divCheckPasswordMatch").html("Passwords match.");
			    	
			    	// return true;
			    	}
			}

			$(document).ready(function () {
			   $("#txtConfirmPassword").keyup(checkPasswordMatch);
			});
			</script>
<?php
		$row = mysqli_fetch_array($recoverychk);
		$userid = $row['user_id'];
		$_SESSION['user_id'] = $userid;

		echo "<div class='container'>
				<div class='row'>
					<div class='col-sm-12'>
						<form action='passwordreset.php' method='post'>
							
							<label for='password'>Enter New Password:</label><input type='password' name='password' id='password' required><br />
							<label for='passwordconfirm'>Confirm New Password:</label><input type='password' name='passwordconfirm' id='passwordconfirm' onChange='checkPasswordMatch();' required>
							<div class='registrationFormAlert' id='divCheckPasswordMatch'></div>
							<button id='buttonActivate' type='submit' disabled='disabled'>Submit New Password</button>
						</form>
					</div> 
				</div>
			</div>
					";
	}




} elseif(isset($_GET['email'])) {

	function generateRandomString($length = 15) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	$random = generateRandomString();
	$email = $_GET['email'];

	$recovery = mysqli_query($link, "UPDATE db_users SET recovery='".$random."' WHERE email='".$email."'");

	require 'PHPMailer/PHPMailerAutoload.php';

	$mail = new PHPMailer;

	//Enable SMTP debugging
	// 0 = off (for production use)
	// 1 = client messages
	// 2 = client and server messages
	$mail->SMTPDebug = 2;
	//Ask for HTML-friendly debug output
	$mail->Debugoutput = 'html';                              // Enable verbose debug output

	// $mail->isSMTP();                                      // Set mailer to use SMTP
	$mail->Host = 'mail.gamecycler.com';  // Specify main and backup SMTP servers
	$mail->SMTPAuth = true;                               // Enable SMTP authentication
	$mail->Username = 'info@gamecycler.com';                 // SMTP username
	$mail->Password = 'Eckpfs96$$';                           // SMTP password
	$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
	$mail->Port = 587;                                    // TCP port to connect to

	$mail->setFrom('info@gamecycler.com', 'Password Reset');
	$mail->addAddress($email, '');     // Add a recipient
	// $mail->addAddress('ellen@example.com');               // Name is optional
	// $mail->addReplyTo('gamecycler@gmail.com', 'Gamecycler.com');
	//$mail->addCC('cc@example.com');
	//$mail->addBCC('bcc@example.com');

	//$mail->addAttachment('/var/tmp/file.tar.gz');         // Add attachments
	//$mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name
	$mail->isHTML(true);                                  // Set email format to HTML

	$mail->Subject = 'Password Reset';
	$mail->Body    = '
			<p>Don\'t worry, everyone forgets their password every now and then. Follow the link below to reset your password:</p>

			<p>https://www.gamecycler.com/passwordreset.php?reset='.$random.'&email='.$email.'</p>';

	$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

	if(!$mail->send()) {
	    echo 'Message could not be sent.';
	    echo 'Mailer Error: ' . $mail->ErrorInfo;
	} else {
	    echo 'Email has been sent';
	}

	echo '<h2>You should be receiving an email to reset your password shortly. Check your inbox, and possibly your spam if you don\'t see it in the next few minutes. Otherwise contact us at info [at] gamecycler.com.</h2>
		<meta http-equiv="refresh" content="7;index.php">';

} else {

	

echo "<div class='container'>
		<div class='row'>
			<div class='col-sm-12'>

		<form action='passwordreset.php' type='get'>
			Enter The Email Your Account is Registered To:<br />
			<input type='text' name='email' required />
			<input type='submit' name='submit' value='Send Reset Email'>
		</form>
			
			</div>
		</div>
	</div>";


}



include 'footer.php';
?>