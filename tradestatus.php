<?php include 'base.php';

$title = "Gamecycler | Status of All Trades";
$description = "Check the status of all your trades.";
$keywords = "game trading, trades, playstation, xbox, nintendo, atari, gamers, game players, video games";

include 'header.php';

?>

<!-- <script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">Stripe.setPublishableKey('pk_test_tw9Wne9N7lozN1ud8nGDjS8Y');</script> -->
<body>

<?php

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

		include 'nav.php';

	// GAME OFFER ARRAY
	$gamestatus = "SELECT * FROM game_offer";
	$gamestatusqy = mysqli_query($link, $gamestatus);

	// USER INFO ARRAY
	$userinfo = "SELECT * FROM db_users";
	$userinfoqy = mysqli_query($link, $userinfo);

	while($userinfoarr = mysqli_fetch_array($userinfoqy)) {
		$usrefid = $userinfoarr['user_id'];
		$userarr[$usrefid] = $userinfoarr;
	}

	// GAMES ARRAY
	$games = "SELECT game_id, title, console, yearreleased, loose, image FROM db_games";
	$gamesqy = mysqli_query($link, $games);

	while($gamesarr = mysqli_fetch_array($gamesqy)) {
		$gamesid = $gamesarr['game_id'];
		$gamestable[$gamesid] = $gamesarr;
	}

	while($gamestatusarr = mysqli_fetch_array($gamestatusqy)) {
		$user1acc = $gamestatusarr['user1accept'];
		$user2acc = $gamestatusarr['user2accept'];
		$user1 = $gamestatusarr['user1'];
		$user2 = $gamestatusarr['user2'];
		$user = $_SESSION['user_id'];
		// print_r('user1acc: '.$user1acc.'| user2acc: '.$user2acc.'| user1: '.$user1.'| user2: '.$user2.'| $user: '.$user.'<br />');
		// NEW OFFER
		if($user2 == $user && $user1acc == NULL && $user2acc == NULL) {
			$newoff[] = $gamestatusarr;
		}
		// PENDING TRADE
		elseif($user2 == $user && $user1acc == NULL && $user2acc == "YES") {
			$pendtrade[] = $gamestatusarr;
		}
		// TRIGGER PENDING SCREEN
		elseif($user1 == $user && $user1acc == NULL && $user2acc == "YES") {
			$trigger[] = $gamestatusarr;
		}
		// SENT OFFERS
		elseif($user1 == $user && $user1acc == NULL && $user2acc == NULL) {
			$sentoff[] = $gamestatusarr;
		}
		// ACCEPTED OFFERS
		elseif(($user1 == $user || $user2 == $user) && $user1acc == "YES") {
			$acceptoff[] = $gamestatusarr;
		}
		// REJECTED OFFERS
		elseif(($user1 == $user || $user2 == $user) && $user1acc == NULL && $user2acc == "NO" ) {
			$rejoff[] = $gamestatusarr;
		}
		// CANCELLED OFFERS
		elseif (($user1 == $user || $user2 == $user) && $user1acc == "NO" && $user2acc == "NO" ) {
			$canceloff[] = $gamestatusarr;
		}
	}


?>
								
<?php


	if(count($trigger) > 0) {
		echo "<div class='container'>
				<div class='row'>";
					foreach($trigger as $triggkey => $triggdata) {
						$user2 = $triggdata['user2'];
			echo "		<div class='col-sm-12 gameshead' style='padding: 5px 7px 0 15px;'>
							<h1>".$userarr[$user2]['username']." has accepted your trade!</h1>
						</div>
						<div class='col-sm-5'>
							<div class='row'>";


								$user1wantexp = explode(',',$triggdata['want_games']);
								foreach($user1wantexp as $u1wexpkey => $u1wgame) {
									// Show Game Image
									$gameimg = $gamestable[$u1wgame]['image'];
									
									echo "<div class='col-sm-6'>
											<div class='col-sm-12 yesyesimg'>";
									if(empty($gameimg)){
										echo '<img width="100%" src="images/glogogm.jpg">';
									} 
									else {
										echo '<img width="100%" src="showfile.php?game_id='.$u1wgame.'">';
									}
									
									echo "	</div> <!-- yesyesimg -->";
									// Show Title, Year, Console, Value
									print_r("<div class='col-sm-12 text-center'>
										<strong>".$gamestable[$u1wgame]['title']."</strong>(".$gamestable[$u1wgame]['yearreleased'].")<br />".$gamestable[$u1wgame]['console']." | $".$gamestable[$u1wgame]['loose']."<br />");
									
									echo "	</div> <!-- yesyesimg -->
										</div> <!-- col-sm-4 tile -->";

								}

			echo "				</div> <!-- row -->
							</div><!-- col-sm-5 user1want/user2offer -->
						<div class='col-sm-2 text-center'>
							<strong>For:</strong>
						</div>
						<div class='col-sm-5'>
							<div class='row'>";

							$user1offerexp = explode(',',$triggdata['offer_games']);
							foreach($user1offerexp as $u1ogame) {
							echo "<div class='col-sm-6'>
									<div class='col-sm-12 yesyesimg'>";
									if(empty($gameimg)){
										echo '<img width="100%" src="images/glogogm.jpg">';
									} 
									else {
										echo '<img width="100%" src="showfile.php?game_id='.$u1ogame.'">';
								}

								// Show Title, Year, Console, Value

								echo "	</div> <!-- yesyesimg -->";
								// Show Title, Year, Console, Value
								print_r("<div class='col-sm-12 text-center'>
									<strong>".$gamestable[$u1ogame]['title']."</strong>(".$gamestable[$u1ogame]['yearreleased'].")<br />".$gamestable[$u1ogame]['console']." | $".$gamestable[$u1ogame]['loose']."<br />");
								
								echo "	</div> <!-- yesyesimg -->
									</div> <!-- col-sm-4 tile -->";

							}
			echo "			</div> <!-- row-->
						</div><!-- col-sm-5 user1offher/user2want -->
						<div class='col-sm-12 text-center'>
							<div class='row'>";
					
					if($triggdata['user1value'] > 0) {
						print_r("	<div class='col-sm-12'>
										".$userarr[$user2]['username']." agreed to pay: ".$triggdata['user1value'].
									"</div>");
								} elseif($triggdata['user1value'] == 0) {
						print_r("	<div class='col-sm-12'>
									</div>	
										");
								} else {
						print_r("		<div class='col-sm-12'>
										You owe ".$userarr[$user2]['username'].": <strong>$".$triggdata['user2value'].
									"</strong></div>");
								}
						
						$user2 = $triggdata['user2'];
						$_SESSION['haveuser'] = $triggdata['user2'];
						$_SESSION['u2email'] = $userarr[$user2]['email'];
						$_SESSION['u1wu2h'] = $user1wantexp;
						$_SESSION['u1hu2w'] = $user1offerexp;
						$_SESSION['id'] = $triggdata['id'];

						echo "		<div class='col-sm-12' style='padding-top: 30px;'>";
						if($triggdata['user1value'] < 0) {
							$gamevaltot = ($triggdata['user2value'] + 1.00) * 1;
							print_r("<strike>Please pay: <strong>$".$gamevaltot."</strong> (includes Gamecycler $1 fee)</strike> While Gamecycler is in BETA we're dropping our fees as a thank you for helping us work out the kinks.<br /><br />");
						} elseif($triggdata['user1value'] == 0) {
							print_r("To Finish The Trade Click The Link Below to Pay Gamecycler's Trade Fee");

						} else {
							
							echo "User 2 has already paid ".$triggdata['user1value']." and is waiting for your confirmation.";
							//$gamevaltot = ($triggdata['user1value'] - 2.00) * -1;
							//print_r("Please pay:<br />
							//			Owed to ".$triggdata['usertrade'].": $".$gamevaltot."<br />
							//			Gamecycler Fee: $2<br /><br />");
						}
						echo "		</div>";


						echo "		<div class='col-sm-2 col-sm-offset-4'>
											";

						if($triggdata['user1value'] > 0) {
							$dataam = $triggdata['user1value'] * 100;
						} elseif($triggdata['user1value'] < 0) {
							$dataam = $triggdata['user2value'] * 100;
						}

						$_SESSION['amount'] = $dataam;

						?>

						
						
						<?php require_once('./config.php'); ?>

						<form action="charge.php" method="POST">
						  <script
						    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
						    data-key="<?php echo $stripe['publishable_key']; ?>"
						    data-amount="<?php echo $dataam; ?>"
						    data-name="GAMECYCLER.COM"
						    data-description="Fees &amp; Payments"
						    data-image="images/g-logo-square.gif"
						    data-zip-code="true"
						    data-locale="auto">
						  </script>
						</form>

						<!-- <form action="" method="POST">
						  <script
						    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
						    data-key="pk_test_tw9Wne9N7lozN1ud8nGDjS8Y"
						    data-amount="999"
						    data-name="GAMECYCLER.COM"
						    data-description="Widget"
						    data-image="https://s3.amazonaws.com/stripe-uploads/acct_185zDALWUQjBgyzwmerchant-icon-1463955906070-g-logo-square.gif"
						    data-locale="auto">
						  </script>
						</form> -->



									<!--	<form name="_xclick" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
										<input type="hidden" name="cmd" value="_xclick">
										<input type="hidden" name="business" value="ricdelgado80-facilitator@gmail.com">
										<input type="hidden" name="return" value="http://localhost/thankyou.php">
										<input type="hidden" name="no_shipping" value="1">
										<input type="hidden" name="item_name" value="Transaction Fee">
										<input type="hidden" name="amount" value="? echo $gamevaltot; ?>">
										<input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
										</form> -->

									</div>
									<div class="col-sm-6 text-left">
										<form action="reject.php" method="post" id="reject">
											<input type="checkbox" name="accept" value="<?php echo $triggdata['id']; ?>" rel="reject" hidden checked>
											<input type="submit" value="Reject">
									

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php

						}





		echo "		</div><!-- row -->
			</div><!-- container -->";
	} else {

?>
	<script>
		$('#myTabs a').click(function (e) {
		  e.preventDefault()
		  $(this).tab('show')
		})
	</script>


		<!-- <div class='container'> -->
			<?php print_r("<div class='col-sm-12'><h1 style='font-size: 2.4em; text-transform: uppercase;'><strong>Trade Status for ".$_SESSION['Username']."</strong></h1></div>"); ?>
			
				<div class='col-sm-12'>
					
						<ul class="nav nav-tabs" role="tablist">
							<li role="presentation" class="active"><a href="#newtradeoff" aria-controls="newtradeoff" role="tab" data-toggle="tab">New Trade Offers (<?php echo count($newoff); ?>)</a></li>
							<li role="presentation"><a href="#pendingtrades" aria-controls="pendingtrades" role="tab" data-toggle="tab">Pending Trades (<?php echo count($pendtrade); ?>)</a></li>
							<li role="presentation"><a href="#sentoffer" aria-controls="sentoffer" role="tab" data-toggle="tab">Sent Offers (<?php echo count($sentoff); ?>)</a></li>
							<li role="presentation"><a href="#acceptedtrades" aria-controls="acceptedtrades" role="tab" data-toggle="tab">Accepted Trades (<?php echo count($acceptoff); ?>)</a></li>
							<li role="presentation"><a href="#rejtrade" aria-controls="rejtrade" role="tab" data-toggle="tab">Rejected Trades (<?php echo count($rejoff); ?>)</a></li>
							<li role="presentation"><a href="#canceltrade" aria-controls="canceltrade" role="tab" data-toggle="tab">Canceled Trades (<?php echo count($canceloff); ?>)</a></li>
						</ul>
	    	

				    	<div class="row tab-content">

							<div role="tabpanel" class="tab-pane fade in active" id="newtradeoff">
								<div class='col-sm-12'>								
									<div class="row">
										<div class="col-sm-12 gameshead" style="margin-top: 15px 0;">
											<h2><strong>New Trade Offers(<?php echo count($newoff); ?>)</strong></h2>
										</div>
				<?php
									if(count($newoff) > 0) {
										foreach($newoff as $newoffdata) {
											$fromuserid = $newoffdata['user1'];
											$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
									        $size1 = 120;
									        $email1 = $userarr[$fromuserid]['email'];

									        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
				?>					    <div class="col-sm-12 tradetile">
										<div class="row">    
											<div class="col-sm-2 text-center">
												<a href="profile.php?id=<? echo $fromuserid; ?>"><img width="100%" src="<?php echo $grav_url1 ?>"></a><br />
												<?php print_r('<strong>'.$userarr[$fromuserid]['username'].'</strong>'); ?>
											</div>

											<div class="col-sm-10">
												<?php echo '<strong>Date of Offer:</strong> '. date('m-d-Y', strtotime($newoffdata['timestamp'])); ?>
												<div class="row">
													<div class="col-sm-5">
														<strong><p><?php echo $userarr[$fromuserid]['username']; ?> Wants</p></strong>
				<?php
														
														$gamewantexp = explode( ',',$newoffdata['want_games']);

														foreach($gamewantexp as $gameid) {
															print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameid]['title'].' - '.$gamestable[$gameid]['console'].'<br />');
														}
				?>
													</div> <!-- col-sm-5 want game section -->
													<div class="col-sm-2 text-center">
														<h3><strong>FOR</strong></h3>
				<?php										
															if($newoffdata['user1value'] > 0) {
																print_r('Asking for: '.$newoffdata['user1value']);
															} elseif ($newoffdata['user1value'] < 0) {
																print_r('Offering: '.$newoffdata['user2value']);
															} elseif ($newoffdata['user1value'] == 0) {
																print_r('$0');
															} else {
																echo "Houston, we have a problem.";
															}
				?>
													</div>
													<div class="col-sm-5">
														<strong><p><?php echo $userarr[$user1]['username']; ?> Offers</p></strong>
				<?php
														$gameoffexp = explode( ',',$newoffdata['offer_games']);
														foreach($gameoffexp as $gameoffid) {
															print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title'].' - '.$gamestable[$gameoffid]['console'].'<br />');
														}
				?>
													</div> <!-- col-sm-5 offer games section -->
												</div> <!-- row info section -->
											</div> <!-- col-sm-10 info section -->
											<div class="col-sm-12 text-center" style="margin-top:15px">
											<div class="row">

												
				<?php
												if($newoffdata['user2value'] < 0 ) {

														$dataam = $newoffdata['user1value'] * 100;
														$_SESSION['amount'] = $dataam;

														$_SESSION['user2'] = $fromuserid;
														$_SESSION['haveuser'] = $user1;
														$_SESSION['u1wu2h'] = $gamewantexp;
														$_SESSION['u1hu2w'] = $gameoffexp;
														$_SESSION['id'] = $newoffdata['id'];

													echo '<div class="col-sm-12" style="margin-bottom: 15px;">
												<h3><strong>Please pay '.$userarr[$fromuserid]['username'].': '.$newoffdata['user1value'].'</strong></h3><strike>+ $1 Gamecycler\'s Trade Fee</strike> <small>(Gamecycler is in BETA, and to thank you we\'re waiving our fees)</small>
												</div>';

												echo '	<div class="col-sm-2 col-sm-offset-4">';

														require_once('./config.php'); ?>

														<form action="charge.php" method="POST">
														  <script
														    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
														    data-key="<?php echo $stripe['publishable_key']; ?>"
														    data-amount="<?php echo $dataam; ?>"
														    data-name="GAMECYCLER.COM"
														    data-description="Fees &amp; Payments"
														    data-image="images/g-logo-square.gif"
														    data-zip-code="true"
														    data-locale="auto">
														  </script>
														</form>

														To Accept
				<?php									} elseif($newoffdata['user1value'] == 0) {
														$_SESSION['id'] = $newoffdata['id'];

												echo '	<div class="col-sm-2 col-sm-offset-4">';
				?>									<form action="thankyou.php" method="post" id="accept">
														<input type="checkbox" name="accept" value="<?php echo $newoffdata['id']; ?>" rel="accept" hidden checked>
														<input type="submit" value="Accept">
													</form>
										<?php
														} else {

												echo '	<div class="col-sm-2 col-sm-offset-4">';
				?>									<form action="accept.php" method="post" id="accept">
														<input type="checkbox" name="accept" value="<?php echo $newoffdata['id']; ?>" rel="accept" hidden checked>
														<input type="submit" value="Accept">
													</form>
										<?php	
												} ?>

												</div>
												<div class="col-sm-2">
													<form action="reject.php" method="post" id="reject">
														<input type="checkbox" name="accept" value="<?php echo $newoffdata['id']; ?>" rel="reject" hidden checked>
														<input type="submit" value="Reject">
													</form>
												</div>
											</div> <!-- row -->
											</div> <!-- col-sm-12 -->

										</div>
										</div>
				<?php
										} // foreach newoff
									} // if new off is greater than 0
									else
									{
				?>
										<div class="col-sm-12 text-center">
											<h2><strong>No New Offers</strong></h2>
										</div>
				<?php
									}

				?>
									</div> <!-- row -->
								</div> <!-- col-sm-12 newtrade off window -->
							</div> <!-- new trade off panel #1 -->

							<!--*********************************

								PENDING TRADES
			
							**********************************-->


							<div role="tabpanel" class="tab-pane fade in" id="pendingtrades">
								<div class='col-sm-12'>								
									<div class="row">
										<div class="col-sm-12 gameshead" style="margin-top: 15px 0;">
											<h2><strong>Pending Trades(<?php echo count($pendtrade); ?>)</strong></h2>
										</div>
				<?php
									if(count($pendtrade) > 0) {
										foreach($pendtrade as $pendoffkey => $pendoffdata) {
										// From
										$fromuserid = $pendoffdata['user1'];
										$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
								        $size1 = 120;
								        $email1 = $userarr[$fromuserid]['email'];

								        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
			?>					    
									<div class="col-sm-12 tradetile">
									<div class="row"> 
										<div class="col-sm-2 text-center">
											<a href="profile.php?id=<? echo $fromuserid; ?>"><img width="100%" src="<?php echo $grav_url1 ?>"></a><br />
											<?php print_r('<strong>'.$userarr[$fromuserid]['username'].'</strong>'); ?>
										</div>

										<div class="col-sm-10">
											<?php echo '<strong>Date of Offer:</strong> '. date('m-d-Y', strtotime($pendoffdata['timestamp'])); ?>
											<div class="row">
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Wants</p></strong> -->
			<?php
													
													$gamewantexp = explode( ',',$pendoffdata['want_games']);

													foreach($gamewantexp as $gameid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameid]['title'].' - '.$gamestable[$gameid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 want game section -->
												<div class="col-sm-2 text-center">
													<h3><strong>FOR</strong></h3>
			<?php										
														if($pendoffdata['user1value'] > 0) {
															print_r('Asking for: '.$pendoffdata['user1value']);
														} elseif ($pendoffdata['user1value'] < 0) {
															print_r('Offering: '.$pendoffdata['user2value']);
														} elseif ($pendoffdata['user1value'] == 0) {
															print_r('$0');
														} else {
															echo "Houston, we have a problem.";
														}
			?>
												</div>
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$user1]['username']; ?> Offers</p></strong> -->
			<?php
													$gameoffexp = explode( ',',$pendoffdata['offer_games']);
													foreach($gameoffexp as $gameoffid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title'].' - '.$gamestable[$gameoffid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 offer games section -->
											</div> <!-- row info section -->
										</div> <!-- col-sm-10 info section -->
									</div>
									</div>	
			<?php
									} // foreach newoff
								} // if new off is greater than 0
								else
								{
			?>
									<div class="col-sm-12 text-center">
										<h2><strong>No Pending Offers</strong></h2>
									</div>
			<?php
								}

			?>
								</div> <!-- row -->
							</div> <!-- col-sm-12 newtrade off window -->
						</div> <!-- new trade off panel #1 -->


<?php

	/**********************************

				SENT TRADES

	**********************************/
?>

							<div role="tabpanel" class="tab-pane fade in" id="sentoffer">
								<div class='col-sm-12'>								
									<div class="row">
										<div class="col-sm-12 gameshead" style="margin-top: 15px 0;">
											<h2><strong>Sent Offers(<?php echo count($sentoff); ?>)</strong></h2>
										</div>
<?php
										if(count($sentoff) > 0) {
										foreach($sentoff as $sentoffkey => $sentoffdata) {
											$fromuserid = $sentoffdata['user2'];
											$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
									        $size1 = 120;
									        $email1 = $userarr[$fromuserid]['email'];

									        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
				?>					     
										<div class="col-sm-12 tradetile">
										<div class="row">   
											<div class="col-sm-2 text-center">
												<a href="profile.php?id=<? echo $fromuserid; ?>"><img width="100%" src="<?php echo $grav_url1 ?>"></a><br />
												<?php print_r('<strong>'.$userarr[$fromuserid]['username'].'</strong>'); ?>
											</div>

											<div class="col-sm-10">
												<?php echo '<strong>Date of Offer:</strong> '. date('m-d-Y', strtotime($sentoffdata['timestamp'])); ?>
												<div class="row">
													<div class="col-sm-5">
														<!-- <strong><p>?php echo $userarr[$user1]['username']; ?> Wants</p></strong> -->
				<?php
														
														$gamewantexp = explode( ',',$sentoffdata['want_games']);

														foreach($gamewantexp as $gameid) {
															print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameid]['title'].' - '.$gamestable[$gameid]['console'].'<br />');
														}
				?>
													</div> <!-- col-sm-5 want game section -->
													<div class="col-sm-2 text-center">
														<h3><strong>FOR</strong></h3>
				<?php										
															if($sentoffdata['user1value'] > 0) {
																print_r('Asking for: '.$sentoffdata['user1value']);
															} elseif ($sentoffdata['user1value'] < 0) {
																print_r('Offering: '.$sentoffdata['user2value']);
															} elseif ($sentoffdata['user1value'] == 0) {
																print_r('$0');
															} else {
																echo "Houston, we have a problem.";
															}
				?>
													</div>
													<div class="col-sm-5">
														<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Offers</p></strong> -->
				<?php
														$gameoffexp = explode( ',',$sentoffdata['offer_games']);
														foreach($gameoffexp as $gameoffid) {
															print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title'].' - '.$gamestable[$gameoffid]['console'].'<br />');
														}
				?>
													</div> <!-- col-sm-5 offer games section -->
												</div> <!-- row info section -->
											</div> <!-- col-sm-10 info section -->
										</div>
										</div>	
				<?php
										} // foreach newoff
									} // if new off is greater than 0
									else
									{
				?>
										<div class="col-sm-12 text-center">
											<h2><strong>No Pending Offers</strong></h2>
										</div>
				<?php
									}

				?>
									</div> <!-- row -->
								</div> <!-- col-sm-12 newtrade off window -->
							</div> <!-- new trade off panel #1 -->
<?php

	/*****************************

			ACCEPTED TRADES
	
	*****************************/

?>

							<div role="tabpanel" class="tab-pane fade in" id="acceptedtrades">
								<div class='col-sm-12'>								
									<div class="row">
										<div class="col-sm-12 gameshead" style="margin-top: 15px 0;">
											<h2><strong>Accepted Trades(<?php echo count($acceptoff); ?>)</strong></h2>
										</div>
				<?php
									if(count($acceptoff) > 0) {
										foreach($acceptoff as $acceptoffdata) {
										// From
										if($acceptoffdata['user1'] == $user1) {
											$fromuserid = $acceptoffdata['user2'];	
										} else {
											$fromuserid = $acceptoffdata['user1'];
										}
										
										$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
								        $size1 = 120;
								        $email1 = $userarr[$fromuserid]['email'];

								        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;

			?>					    
									<div class="col-sm-12 tradetile">
									<div class="row"> 
										<div class="col-sm-2 text-center">
											<a href="profile.php?id=<? echo $fromuserid; ?>"><img width="100%" src="<?php echo $grav_url1 ?>"></a><br />
											<?php print_r('<strong>'.$userarr[$fromuserid]['username'].'</strong>'); ?>
										</div>

										<div class="col-sm-10">
											<?php echo '<strong>Date of Offer:</strong> '. date('m-d-Y', strtotime($acceptoffdata['timestamp'])); ?>
											<div class="row">
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$user1]['username']; ?> Wants</p></strong> -->
			<?php
													
													$gamewantexp = explode( ',',$acceptoffdata['want_games']);

													foreach($gamewantexp as $gameid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameid]['title'].' - '.$gamestable[$gameid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 want game section -->
												<div class="col-sm-2 text-center">
													<h3><strong>FOR</strong></h3>
			<?php										
														if($acceptoffdata['user1value'] > 0) {
															print_r('Asking for: '.$acceptoffdata['user1value']);
														} elseif ($acceptoffdata['user1value'] < 0) {
															print_r('Offering: '.$acceptoffdata['user2value']);
														} elseif ($acceptoffdata['user1value'] == 0) {
															print_r('$0');
														} else {
															echo "Houston, we have a problem.";
														}
			?>
												</div>
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Offers</p></strong> -->
			<?php
													$gameoffexp = explode( ',',$acceptoffdata['offer_games']);
													foreach($gameoffexp as $gameoffid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title'].' - '.$gamestable[$gameoffid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 offer games section -->
											</div> <!-- row info section -->
										</div> <!-- col-sm-10 info section -->
									</div>
									</div>	
			<?php
									} // foreach newoff
								} // if new off is greater than 0
								else
								{
			?>
									<div class="col-sm-12 text-center">
										<h2><strong>No Accepted Trades</strong></h2>
									</div>
			<?php
								}

			?>
								</div> <!-- row -->
							</div> <!-- col-sm-12 newtrade off window -->
						</div> <!-- new trade off panel #1 -->


	<!--*********************************

				REJECTED TRADES
			
	**********************************-->


							<div role="tabpanel" class="tab-pane fade in" id="rejtrade">
								<div class='col-sm-12'>								
									<div class="row">
										<div class="col-sm-12 gameshead" style="margin-top: 15px 0;">
											<h2><strong>Rejected Trades(<?php echo count($pendtrade); ?>)</strong></h2>
										</div>
				<?php
									if(count($rejoff) > 0) {
										foreach($rejoff as $rejoffdata) {
										// From
										$fromuserid = $rejoffdata['user1'];
										$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
								        $size1 = 120;
								        $email1 = $userarr[$fromuserid]['email'];

								        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
			?>					    
									<div class="col-sm-12 tradetile">
									<div class="row"> 
										<div class="col-sm-2 text-center">
											<a href="profile.php?id=<? echo $fromuserid; ?>"><img width="100%" src="<?php echo $grav_url1 ?>"></a><br />
											<?php print_r('<strong>'.$userarr[$fromuserid]['username'].'</strong>'); ?>
										</div>

										<div class="col-sm-10">
											<?php echo '<strong>Date of Offer:</strong> '. date('m-d-Y', strtotime($rejoffdata['timestamp'])); ?>
											<div class="row">
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Wants</p></strong> -->
			<?php
													
													$gamewantexp = explode( ',',$rejoffdata['want_games']);

													foreach($gamewantexp as $gameid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameid]['title'].' - '.$gamestable[$gameid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 want game section -->
												<div class="col-sm-2 text-center">
													<h3><strong>FOR</strong></h3>
			<?php										
														if($rejoffdata['user1value'] > 0) {
															print_r('Asking for: '.$rejoffdata['user1value']);
														} elseif ($rejoffdata['user1value'] < 0) {
															print_r('Offering: '.$rejoffdata['user2value']);
														} elseif ($rejoffdata['user1value'] == 0) {
															print_r('$0');
														} else {
															echo "Houston, we have a problem.";
														}
			?>
												</div>
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Offers</p></strong> -->
			<?php
													$gameoffexp = explode( ',',$rejoffdata['offer_games']);
													foreach($gameoffexp as $gameoffid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title'].' - '.$gamestable[$gameoffid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 offer games section -->
											</div> <!-- row info section -->
										</div> <!-- col-sm-10 info section -->
									</div>
									</div>	
			<?php
									} // foreach newoff
								} // if new off is greater than 0
								else
								{
			?>
									<div class="col-sm-12 text-center">
										<h2><strong>No Rejected Trades</strong></h2>
									</div>
			<?php
								}

			?>
								</div> <!-- row -->
							</div> <!-- col-sm-12 newtrade off window -->
						</div> <!-- new trade off panel #1 -->


	<!--*********************************

				CANCELED TRADES
			
	**********************************-->


							<div role="tabpanel" class="tab-pane fade in" id="canceltrade">
								<div class='col-sm-12'>								
									<div class="row">
										<div class="col-sm-12 gameshead" style="margin-top: 15px 0;">
											<h2><strong>Canceled Trades(<?php echo count($canceloff); ?>)</strong></h2>
										</div>
				<?php
									if(count($canceloff) > 0) {
										foreach($canceloff as $canceloffdata) {
										// From
										$fromuserid = $canceloffdata['user2'];
										$default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
								        $size1 = 120;
								        $email1 = $userarr[$fromuserid]['email'];

								        $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;
			?>					    
									<div class="col-sm-12 tradetile">
									<div class="row"> 
										<div class="col-sm-2 text-center">
											<a href="profile.php?id=<? echo $fromuserid; ?>"><img width="100%" src="<?php echo $grav_url1 ?>"></a><br />
											<?php print_r('<strong>'.$userarr[$fromuserid]['username'].'</strong>'); ?>
										</div>

										<div class="col-sm-10">
											<?php echo '<strong>Date of Offer:</strong> '. date('m-d-Y', strtotime($canceloffdata['timestamp'])); ?>
											<div class="row">
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Wants</p></strong> -->
			<?php
													
													$gamewantexp = explode( ',',$canceloffdata['want_games']);

													foreach($gamewantexp as $gameid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameid]['title'].' - '.$gamestable[$gameid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 want game section -->
												<div class="col-sm-2 text-center">
													<h3><strong>FOR</strong></h3>
			<?php										
														if($canceloffdata['user1value'] > 0) {
															print_r('Asking for: '.$canceloffdata['user1value']);
														} elseif ($canceloffdata['user1value'] < 0) {
															print_r('Offering: '.$canceloffdata['user2value']);
														} elseif ($canceloffdata['user1value'] == 0) {
															print_r('$0');
														} else {
															echo "Houston, we have a problem.";
														}
			?>
												</div>
												<div class="col-sm-5">
													<!-- <strong><p>?php echo $userarr[$fromuserid]['username']; ?> Offers</p></strong> -->
			<?php
													$gameoffexp = explode( ',',$canceloffdata['offer_games']);
													foreach($gameoffexp as $gameoffid) {
														print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$gameoffid]['title'].' - '.$gamestable[$gameoffid]['console'].'<br />');
													}
			?>
												</div> <!-- col-sm-5 offer games section -->
											</div> <!-- row info section -->
										</div> <!-- col-sm-10 info section -->
									</div>
									</div>	
			<?php
									} // foreach newoff
								} // if new off is greater than 0
								else
								{
			?>
									<div class="col-sm-12 text-center">
										<h2><strong>No Canceled Trades</strong></h2>
									</div>
			<?php
								}

			?>
								</div> <!-- row -->
							</div> <!-- col-sm-12 newtrade off window -->
						</div> <!-- new trade off panel #1 -->

				</div><!-- col-sm-12 for tables -->
			</div><!-- row -->
		</div><!-- container -->
		</div>
		</div>
		</div>
		

<?php
	mysqli_free_result($gamestatusqy);
	mysqli_free_result($userinfoqy);
	mysqli_free_result($gamesqy);


}
}
else
{
	?>
    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
    Please Log in!
    <?php
}


	?>



<?php include "footer.php"; ?>