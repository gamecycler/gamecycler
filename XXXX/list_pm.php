<?php include 'base.php'; 

$title = "Gamecycler | View Your Offers";
$description = "Incoming trade offers from other users, and the status of your treades.";
$keywords = "gamers, trading, trade games, user offers, playstation, xbox, xbox360, nintendo";

include 'header.php'; ?>

<body>

<div class="content">
<?php
//We check if the user is logged
if(isset($_SESSION["Username"]))


// Establish game table
$gamesdblist = "SELECT * FROM db_games";
$gameslist = mysqli_query($link, $gamesdblist);

// create game refernce table
while($gamesarray = mysqli_fetch_array($gameslist,MYSQLI_ASSOC)) {
			$gamestable[] = $gamesarray; // place entire gamewantarray into a numbered array to reference individual results
		} // put $gamelist query into a gamelistarray


// Step 1. User1 selects Want & Have Games, compare-ui shows matching users. User 1 selects user 2, then submit sends msg through pm to user 2

//USER 2 ACCEPTED
$requ2acc = mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.user2 as usertrade, m1.want_games as want_games, m1.offer_games as offer_games, m1.user1value, m1.user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user1="'.$_SESSION['user_id'].'" and m1.user1accept="" and m1.user2accept="YES" and db_users.user_id=m1.user1)
	
	and m1.id2="1" group by m1.id order by m1.id desc'
	);

//NEW OFFERS
$reqnew = mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.want_games as want_games, m1.offer_games as offer_games, m1.user1value, m1.user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user2="'.$_SESSION['user_id'].'" and m1.user2accept="" and db_users.user_id=m1.user1)
	
	and m1.id2="1" group by m1.id order by m1.id desc'
	);
	
//SENT TRADES
$reqsent = mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.want_games as want_games, m1.offer_games as offer_games, m1.user1value, m1.user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user1="'.$_SESSION['user_id'].'" and m1.user2accept="" and db_users.user_id=m1.user2)
	
	and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc'
	); 

//PENDING TRADES
$reqpend=mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.want_games as want_games, m1.offer_games as offer_games, m1.user1value, m1.user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user2="'.$_SESSION['user_id'].'" and m1.user1accept="" and m1.user2accept="YES" and db_users.user_id=m1.user1)
	
	and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc'
	);

//ACCEPTED TRADES
$reqacc=mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.want_games as want_games, m1.offer_games as offer_games, m1.user1value, m1.user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user1="'.$_SESSION['user_id'].'" OR m1.user2="'.$_SESSION['user_id'].'" and m1.user1accept="YES" and m1.user2accept="YES" and db_users.user_id=m1.user1)
	
	and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc'
	);

//REJECTED TRADES
$reqrej=mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.want_games as want_games, m1.offer_games as offer_games, m1.user1value, m1.user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user2="'.$_SESSION['user_id'].'" and m1.user2accept="NO" and db_users.user_id=m1.user1)
	
	and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc'
	);

$reqcancel=mysqli_query($link, '
	select
	m1.id, m1.timestamp, count(m2.id) as reps, db_users.user_id as userid, db_users.username, m1.user1 as userreq, m1.want_games as want_games, m1.offer_games as offer_games, user1value, user2value
	from
	game_offer as m1, game_offer as m2, db_users
	where
		
	(m1.user1="'.$_SESSION['user_id'].'" OR m1.user2="'.$SESSION['user_id'].'") and (m1.user1accept="NO" and db_users.user_id=m1.user1)
	
	and m1.id2="1" and m2.id=m1.id group by m1.id order by m1.id desc'
	);

?>
<?php
if(mysqli_num_rows($requ2acc)>0) {
	echo '<div class="container">
				<div class="row">';
		while($dn = mysqli_fetch_array($requ2acc)) {
			print_r($dn);
			echo '		<div class="col-sm-12">';
			print_r("		USER ".$dn['usertrade']." has accepted your trade!<br /><br />");
			echo '		</div>
					
						<div class="col-sm-4">';
			$u1wu2h = explode( ",",$dn['want_games']);
			foreach($u1wu2h as $title => $gameid) {
				$gameid = $gameid - 1;
				print_r($gamestable[$gameid]['title'] . " - ".$gamestable[$gameid]['console'] . "<br />");
			}
			echo '		</div>
						<div class="col-sm-2 col-sm-offset-2">
						<p>For:</p>
						</div>
						<div class="col-sm-4">';
			$u1hu2w = explode( ",",$dn['offer_games']);
			foreach($u1hu2w as $title2 => $gameoffid) {
				$gameoffid = $gameoffid - 1;
				print_r($gamestable[$gameoffid]['title'] ." - ".$gamestable[$gameoffid]['console'] . "<br />");
			}

			echo "		</div>";

			if($dn['user1value'] >= 0) {
			print_r("	<div class='col-sm-12'>
							User ".$dn['usertrade']." agreed to pay: ".$dn['user2value'].
						"</div>");
					} else {
			print_r("		<div class='col-sm-12'>
							You owe User ".$dn['usertrade'].": ".$dn['user2value'].
						"</div>");
					}
			
			$_SESSION['haveuser'] = $dn['usertrade'];
			$_SESSION['u1wu2h'] = $u1wu2h;
			$_SESSION['u1hu2w'] = $u1hu2w;
			$_SESSION['id'] = $dn['id'];

			echo "<br /><br />";
			print_r($_SESSION);
			echo "<br /><br />";
			

			echo "		<div class='col-sm-12'>";
			if($dn['user1value'] >= 0) {
				$gamevaltot = $dn['user1value'] + 2.00;
				print_r("Please pay: ".$gamevaltot."(includes Gamecycler $2 fee)<br /><br />");
			} else {
				$gamevaltot = ($dn['user1value'] - 2.00) * -1;
				print_r("Please pay:<br />
							Owed to ".$dn['usertrade'].": $".$gamevaltot."<br />
							Gamecycler Fee: $2<br /><br />");
			}
			echo "		</div>";


			echo "		<div class='col-sm-12'>
							This link will then take you to Paypal to pay:<br /><br />";
			?>

							<form name="_xclick" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
							<input type="hidden" name="cmd" value="_xclick">
							<input type="hidden" name="business" value="ricdelgado80-facilitator@gmail.com">
							<input type="hidden" name="return" value="http://localhost/thankyou.php">
							<input type="hidden" name="no_shipping" value="1">
							<input type="hidden" name="item_name" value="Transaction Fee">
							<input type="hidden" name="amount" value="<? echo $gamevaltot; ?>">
							<input type="image" src="http://www.paypal.com/en_US/i/btn/x-click-but01.gif" border="0" name="submit" alt="Make payments with PayPal - it's fast, free and secure!">
							</form>

						</div>


			<!--
				<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top">
				<input type="hidden" name="cmd" value="_s-xclick">
				<input type="hidden" name="hosted_button_id" value="7QMAGBZVPG8QA">
				<input type="hidden" name="amount" value="<?php // echo $gamevaltot; ?>">
				<input type="image" src="https://www.sandbox.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
				<img alt="" border="0" src="https://www.sandbox.paypal.com/en_US/i/scr/pixel.gif" width="1" height="1">
				</form>
			-->

		<?php



	}

	echo '	</div> <!-- row -->
		</div> <!-- container -->	';

}
else {
?>
Here's a status of your trades <?php echo $_SESSION['user_id'] . " " . $_SESSION["Username"] ?>:<br />

<!-- NEW TRADES SECTION -->

<h3>New Offers(<?php echo intval(mysqli_num_rows($reqnew)); ?>):</h3>
<table style="border-collapse: separate; border-spacing: 40px 10px;">
        <tr>
        <th>From</th>
        <th>Games Want</th>
        <th>Games Offered</th>
        <th>Value Difference</th>
        <th>Date of Offer</th>
		<th>Status</th>
		<th>Reject</th>
		<th>Accept</th>
    </tr>
<?php
//We display the list of unread offers
while($dn1 = mysqli_fetch_array($reqnew))
{
?>
        <tr>
        <!--FROM --><td class="left"><a href="read_pm.php?id=<?php echo $dn1['id']; ?>"><?php echo htmlentities($dn1['username'], ENT_QUOTES, 'UTF-8'); ?></a></td>
		<!--GAMES WANT --><td><?php 
		foreach(explode( ',', $dn1['want_games']) as $keywant => $gamewantid1) {
			$gamewantid1 = $gamewantid1 - 1;
			print_r($gamestable[$gamewantid1]['title']." - ".$gamestable[$gamewantid1]['console']."<br />");
		 } 
		?>
		</td>
		<!--GAMES OFFERED --><td><?php
		foreach(explode( ',', $dn1['offer_games']) as $keyoff => $gameoffid1) {
			$gameoffid1 = $gameoffid1 - 1;
			print_r($gamestable[$gameoffid1]['title']." - ".$gamestable[$gameoffid1]['console']."<br />");
		 }
		 ?>
		</td>
		<!-- VALUE DIFF -->
		<td>
		<?php
		if($dn1['user2value'] > 0) {
			print_r('Offering: $'.$dn1['user2value']);
		} else {
			print_r('Asking For: $'.$dn1['user1value']);
		}

		?>
		</td>
        <!--DATE OF OFFER --><td><?php

		$timest = strtotime($dn1['timestamp']);
		// echo $timest;
		echo date("n-j-Y", $timest);

        ?></td>
		<!--STATUS --><td>New Offer</td>
		<!-- ACCEPT OR REJECT BUTTONS -->
		<form action='' method='POST'>
			
		<!--REJECT --><td><button onclick="reject('<?php echo $dn1['id']; ?>');"><img width="35px" src="/images/reject.jpg"></button></td>
		<script>
			function reject(id)
				{

					$.ajax({
						url: 'reject.php',
						type: 'POST',
						data: { 'id': id, 'reject': 'No' },                   
						success: function(data)
									{
										alert(data)                                    
									}
					});
				}
			</script>

		<!--ACCEPT --><td>

		<? if($dn1['user2value'] < 0) {
			?>
		<button onclick="accept('<?php echo $dn1['id']; ?>')"><img width="40px" src="/images/accept.jpg"></button></td>
		
		<script>
			function accept(id)
				{

					$.ajax({
						url: 'sendmoney.php',
						type: 'POST',
						data: { 'id': id, 'accept': 'No' },                   
						success: function(data)
									{
										alert(data)                                    
									}
					});
				}
			</script>	

			<?php
		} else {

		?>
		<button onclick="accept('<?php echo $dn1['id']; ?>');"><img width="40px" src="/images/accept.jpg"></button></td>
		
		<script>
			function accept(id)
				{

					$.ajax({
						url: 'accept.php',
						type: 'POST',
						data: { 'id': id, 'accept': 'No' },                   
						success: function(data)
									{
										alert(data)                                    
									}
					});
				}
			</script>
			<?php
			}
			?>
			</td>
		
    </tr>
<?php
}
//If there is no unread message we notice it
if(intval(mysqli_num_rows($reqnew))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>

<!-- END NEW OFFERS SECTION -->

<!-- PENDING TRADES SECTION -->

<h3>Pending Trades(<?php echo intval(mysqli_num_rows($reqpend)); ?>):</h3>
<table style="border-collapse: separate; border-spacing: 40px 10px;">
        <tr>
        <th>From</th>
        <th>Games Want</th>
        <th>Games Offered</th>
        <th>Value Difference</th>
        <th>Date of Offer</th>
		<th>Status</th>
    </tr>
<?php
//We display the list of unread messages
while($dn2 = mysqli_fetch_array($reqpend))
{
?>
        <tr>
        <!--FROM --><td class="left"><a href="read_pm.php?id=<?php echo $dn2['id']; ?>"><?php echo htmlentities($dn2['username'], ENT_QUOTES, 'UTF-8'); ?></a></td>
		<!--GAMES WANT --><td><?php 
		foreach(explode( ',', $dn2['want_games']) as $keywant => $gamewantid1) {
			$gamewantid1 = $gamewantid1 - 1;
			print_r($gamestable[$gamewantid1]['title']."<br />");
		 } 
		?>
		</td>
		<!--GAMES OFFERED --><td><?php
		foreach(explode( ',', $dn2['offer_games']) as $offarray => $gameoffid1) {
			$gameoffid1 = $gameoffid1 - 1;
			print_r($gamestable[$gameoffid1]['title']."<br />");
		 }
		 ?>
		</td>
		<!-- VALUE DIFF -->
		<td><?php echo "$".$dn2['user2value']; ?>
        <!--DATE OF OFFER --><td><?php echo date('n-j-Y' ,strtotime($dn2['timestamp'])); ?></td>
		<!--STATUS --><td>Accepted, Pending</td>
			
    </tr>
	
<?php
}

//If there is no unread message we notice it
if(intval(mysqli_num_rows($reqpend))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>

<!-- END PENDING TRADES SECTION -->

<!-- SENT OFFERS SECTION -->
	
	<h3>Sent Offers(<?php echo intval(mysqli_num_rows($reqsent)); ?>):</h3>
<table style="border-collapse: separate; border-spacing: 40px 10px;">
        <tr>
        <th>To</th>
        <th>Games Want</th>
        <th>Games Offered</th>
        <th>Price Diff</th>
        <th>Date of Offer</th>
		<th>Status</th>
    </tr>
<?php
//We display the list of unread messages
while($dn3 = mysqli_fetch_array($reqsent))
{
?>
        <tr>
        <!--FROM --><td class="left"><a href="read_pm.php?id=<?php echo $dn3['id']; ?>"><?php echo htmlentities($dn3['username'], ENT_QUOTES, 'UTF-8'); ?></a></td>
		<!--GAMES WANT --><td><?php 
		foreach(explode( ',', $dn3['offer_games']) as $keywant => $gamewantid1) {
			$gamewantid1 = $gamewantid1 - 1;
			print_r($gamestable[$gamewantid1]['title']."<br />");
		 } 
		?>
		</td>
		<!--GAMES OFFERED --><td><?php
		foreach(explode( ',', $dn3['want_games']) as $keyoff => $gameoffid1) {
			$gameoffid1 = $gameoffid1 - 1;
			print_r($gamestable[$gameoffid1]['title']."<br />");
		 }
		 ?>
		 <td>
		 <!-- GAME DIFF -->
		 <?php echo $dn3['user1value']; ?>
		</td>
        <!--DATE OF OFFER --><td><?php echo date('n-j-Y' ,strtotime($dn3['timestamp'])); ?></td>
		<!--STATUS --><td>Pending</td>
		
		
    </tr>
<?php
}
//If there is no unread message we notice it
if(intval(mysqli_num_rows($reqsent))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>

<!-- END SENT OFFERS -->

<!-- ACCEPTED TRADES -->

<h3>Accepted Trades(<?php echo intval(mysqli_num_rows($reqacc)); ?>):</h3>
<table style="border-collapse: separate; border-spacing: 40px 10px;">
        <tr>
        <th>To</th>
        <th>Games Want</th>
        <th>Games Offered</th>
        <th>Date of Offer</th>
		<th>Status</th>
    </tr>
<?php
//We display the list of unread messages
while($dn4 = mysqli_fetch_array($reqacc))
{
?>
        <tr>
        <!--FROM --><td class="left"><a href="read_pm.php?id=<?php echo $dn4['id']; ?>"><?php echo htmlentities($dn4['username'], ENT_QUOTES, 'UTF-8'); ?></a></td>
		<!--GAMES WANT --><td><?php 
		foreach(explode( ',', $dn4['offer_games']) as $keywant => $gamewantid1) {
			$gamewantid1 = $gamewantid1 - 1;
			print_r($gamestable[$gamewantid1]['title']."<br />");
		 } 
		?>
		</td>
		<!--GAMES OFFERED --><td><?php
		foreach(explode( ',', $dn4['want_games']) as $keyoff => $gameoffid1) {
			$gameoffid1 = $gameoffid1 - 1;
			print_r($gamestable[$gameoffidarray]['title']."<br />");
		 }
		 ?>
		</td>
        <!--DATE OF OFFER --><td><?php echo date('n-j-Y', strtotime($dn4['timestamp'])); ?></td>
		<!--STATUS --><td>Accepted</td>
		
		
    </tr>
<?php
}
//If there is no unread message we notice it
if(intval(mysqli_num_rows($reqacc))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>

<!-- END ACCEPT OFFERS -->

<!-- REJECTED OFFERS -->

<h3>Rejected Offers(<?php echo intval(mysqli_num_rows($reqrej)); ?>):</h3>
<table style="border-collapse: separate; border-spacing: 40px 10px;">
        <tr>
        <th>To</th>
        <th>Games Want</th>
        <th>Games Offered</th>
        <th>Date of Offer</th>
		<th>Status</th>
    </tr>
<?php
//We display the list of unread messages
while($dn5 = mysqli_fetch_array($reqrej))
{
?>
        <tr>
        <!--FROM --><td class="left"><a href="read_pm.php?id=<?php echo $dn4['id']; ?>"><?php echo htmlentities($dn5['username'], ENT_QUOTES, 'UTF-8'); ?></a></td>
		<!--GAMES WANT --><td><?php 
		foreach(explode( ',', $dn5['offer_games']) as $keywant => $gamewantid1) {
			$gamewantid1 = $gamewantid1 - 1;
			print_r($gamestable[$gamewantid1]['title']."<br />");
		 } 
		?>
		</td>
		<!--GAMES OFFERED --><td><?php
		foreach(explode( ',', $dn5['want_games']) as $keyoff => $gameoffid1) {
			$gameoffid1 = $gameoffid1 - 1;
			print_r($gamestable[$gameoffid1]['title']."<br />");
		 }
		 ?>
		</td>
        <!--DATE OF OFFER --><td><?php echo date('d/m/Y' ,$dn5['timestamp']); ?></td>
		<!--STATUS --><td>Rejected</td>
		
		
    </tr>
<?php
}
//If there is no unread message we notice it
if(intval(mysqli_num_rows($reqrej))==0)
{
?>
        <tr>
        <td colspan="4" class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>

<?php
}
?>
 
<?php include "footer.php"; ?>