<?php include 'base.php' ?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Select Games A Player Wants</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<?php 

	$userid = $_SESSION['user_id'];
	$username = $_SESSION['Username'];

	$sql = "SELECT * FROM db_games ORDER by console";
	$result = mysqli_query($link, $sql);
	
	if (mysqli_num_rows($result) > 0) {
		echo '<form action="gamesupload-want.php" id="gameselect" class="form-horizontal" method="post">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<h2>Welcome '.$userid.' '.$username.', Please Select The Games You Want:</h2>
						</div>
					
					';
			// output data of each row
			while($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
				echo '<div class="col-md-4">
					<label class="checkbox" for="' . $row["game_id"] . '">
						<input name="game[' . $row["game_id"] . ']" id="' . $row["game_id"] . '" value="' . $row["game_id"] . '" type="checkbox" />' .
							$row["game_id"]." -- ".
							$row["title"].'<br />' .
							$row["console"].'<br />' .
							$row["yearreleased"].'<br />' .
							$row["description"].'<br />' .
							$row["image"] .
						'</label>
						</div>';
			}
	}
	echo '				</div>	
					<div="col-md-12">
						<input type="submit" value="Submit">
					</div>
				</div>
			</div>
		
		</form>';
	
	?>
	
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
	<script src="js/highlight.js"></script>
  </body>
</html>