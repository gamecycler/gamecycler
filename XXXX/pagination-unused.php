<?php
// Pagination

$rec_limit = 30;
if(! $link ) {
	die('Could not connect: ' . mysqli_error());	
}
// mysqli_select_db('db_games');
		 
/* Get total number of records */
$sql = "SELECT count(game_id) FROM db_games";
$retval = mysqli_query($link, $sql);
         
if(! $retval ) {
	die('Could not get data: ' . mysqli_error());
}

$row = mysqli_fetch_array($retval, MYSQLI_NUM);
$rec_count = $row[0];


if( isset($_GET{'page'})) {
	$page = $_GET{'page'} + 1;
	$offset = $rec_limit * $page ;
}
else
{
	$page = 0;
	$offset = 0;
}

$left_rec = $rec_count - ($page * $rec_limit);
$sql = 'SELECT game_id, title, console, image, yearreleased, description, loose FROM db_games ORDER BY title ASC LIMIT '.$offset.','.$rec_limit;
            
$retval = mysqli_query($link, $sql);
         
if(! $retval ) {
	die('Could not get data: ' . mysqli_error());
}
?>