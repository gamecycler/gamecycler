<?php include 'base.php';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Edit my personnal informations</title>
    </head>
    <body>
        <div class="header">
                <a href="<?php echo $url_home; ?>"><img src="<?php echo $design; ?>/images/logo.png" alt="Members Area" /></a>
            </div>
<?php
//We check if the user is logged
if(isset($_SESSION["Username"]))
{
        //We check if the form has been sent
        if(isset($_POST['username'], $_POST['password'], $_POST['passverif'], $_POST['email']))
        {
                //We remove slashes depending on the configuration
                if(get_magic_quotes_gpc())
                {
                        $_POST['username'] = stripslashes($_POST['username']);
                        $_POST['password'] = stripslashes($_POST['password']);
                        $_POST['passverif'] = stripslashes($_POST['passverif']);
                        $_POST['email'] = stripslashes($_POST['email']);
                        
                }
                //We check if the two passwords are identical
                if($_POST['password']==$_POST['passverif'])
                {
                        //We check if the password has 6 or more characters
                        if(strlen($_POST['password'])>=6)
                        {
                                //We check if the email form is valid
                                if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email']))
                                {
                                        //We protect the variables
                                        $username = mysqli_real_escape_string($link, $_POST["username"]);
                                        $password = mysqli_real_escape_string($link, $_POST["password"]);
                                        $email = mysqli_real_escape_string($link, $_POST["email"]);
                                        
                                        //We check if there is no other user using the same username
                                        $dn = mysqli_fetch_array(mysqli_query($link, 'select count(*) as nb from db_users where username="'.$pseudo.'"'));
                                        //We check if the username changed and if it is available
                                        if($dn['nb']==0 or $_POST['username']==$_SESSION['username'])
                                        {
                                                //We edit the user informations
                                                if(mysqli_query($link, 'update db_users set username="'.$pseudo.'", password="'.$password.'", email="'.$email.'", img="'.$img.'", firstname="'.$firstname.'", lastname="'.$lastname.'", age="'.$age.'", location="'.$location.'", about="'.$about.'" where username="'.$_SESSION['username'].'"'))
                                                {
                                                        //We dont display the form
                                                        $form = false;
                                                        //We delete the old sessions so the user need to log again
                                                        unset($_SESSION['username'], $_SESSION['user_id']);
?>
<div class="message">Your informations have successfuly been updated. You need to log again.<br />
<a href="login.php">Log in</a></div>
<?php
                                                }
                                                else
                                                {
                                                        //Otherwise, we say that an error occured
                                                        $form = true;
                                                        $message = 'An error occurred while updating your informations.';
                                                }
                                        }
                                        else
                                        {
                                                //Otherwise, we say the username is not available
                                                $form = true;
                                                $message = 'The username you want to use is not available, please choose another one.';
                                        }
                                }
                                else
                                {
                                        //Otherwise, we say the email is not valid
                                        $form = true;
                                        $message = 'The email you entered is not valid.';
                                }
                        }
                        else
                        {
                                //Otherwise, we say the password is too short
                                $form = true;
                                $message = 'Your password must contain at least 6 characters.';
                        }
                 }
                 else
                 {
                        //Otherwise, we say the passwords are not identical
                        $form = true;
                       $message = 'The passwords you entered are not identical.';
                }
        }
        else
        {
                $form = true;
        }
        if($form)
        {
                //We display a message if necessary
                if(isset($message))
                {
                        echo '<strong>'.$message.'</strong>';
                }
                //If the form has already been sent, we display the same values
                if(isset($_POST['username'],$_POST['password'],$_POST['email']))
                {
                        $pseudo = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');
                        if($_POST['password']==$_POST['passverif'])
                        {
                                $password = htmlentities($_POST['password'], ENT_QUOTES, 'UTF-8');
                        }
                        else
                        {
                                $password = '';
                        }
                        $email = htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');
                        $img = htmlentities($_POST['img'], ENT_QUOTES, 'UTF-8');
                }
                else
                {
                        //otherwise, we display the values of the database
                        $dnn = mysqli_fetch_array(mysqli_query($link, 'select username,firstname,lastname,location,about,age,password,email,img from db_users where user_id="'.$_SESSION['user_id'].'"'));
                        $username = htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8');
                        $password = htmlentities($dnn['password'], ENT_QUOTES, 'UTF-8');
                        $email = htmlentities($dnn['email'], ENT_QUOTES, 'UTF-8');
                        $firstname = htmlentities($dnn['firstname'], ENT_QUOTES, 'UTF-8');
						$lastname = htmlentities($dnn['lastname'], ENT_QUOTES, 'UTF-8');
						$location = htmlentities($dnn['location'], ENT_QUOTES, 'UTF-8');
						$about = htmlentities($dnn['about'], ENT_QUOTES, 'UTF-8');
						$age = htmlentities($dnn['age'], ENT_QUOTES, 'UTF-8');
                }
                //We display the form
?>
<div class="content">
    <form action="edit_infos.php" method="post">
        You can edit your informations:<br />
        <div class="center">
				<label for="username">Username</label>
				<input type="text" name="username" id="username" value="<?php echo $username; ?>" />
            </p>
			<p>
				<label for="email">Email:</label>
				<input type="text" name="email" id="email" value="<?php echo $email; ?>" />
            </p>
			<p>
				<label for="firstName">First Name:</label>
				<input type="text" name="firstname" id="firstname" value="<?php echo $firstname; ?>" />
			</p>
			<p>
				<label for="lastName">Last Name:</label>
				<input type="text" name="lastname" id="lastname" value="<?php echo $lastname; ?>" />
			</p>
			<p>
				<label for="location">Location:</label>
				<input type="text" name="location" id="location" value="<?php echo $location; ?>" />
			</p>
			<p>
				<label for="about">About:</label>
				<input type="text" name="about" id="about" value="<?php echo $about; ?>" />
			</p>
			<p>
				<label for="age">Age:</label>
				<input type="number" name="age" id="age" value="<?php echo $age; ?>" />
			</p>
			<p>
				<label for="password">Password<span class="small">(6 characters min.)</span></label>
				<input type="password" name="password" id="password" value="<?php echo $password; ?>" />
            </p>
			<p>
				<label for="passverif">Verify Password<span class="small">(6 characters min.)</span></label>
				<input type="passverif" name="passverif" id="passverif" />
			</p>
			
            <input type="submit" value="Send" />
        </div>
    </form>
</div>
<?php
        }
}
else
{
?>
<div class="message">To access this page, you must be logged.<br />
<a href="login.php">Log in</a></div>
<?php
}
?>

<? include "footer.php"; ?>