<?php include 'base.php';

$title = "Gamecycler | Your Library";
$description = "The library of your games.";
$keywords = "games,library,games,trading games,nintendo,xbox,playstation,retro,gamers";

include 'header.php'; ?>

<body>

<script>

	$('#filter').keyup(function() {

		var rex = new RegExp($(this).val(), 'i');
		$('.searchable div').hide();
		$('.searchable div').filter(function() {
			return rex.test($(this).children().text());
		}).show();

	})

</script>

<script type="text/javascript">
$(window).load(function() {
	$(".loader").fadeOut("slow");
})
</script>
		

<?php 

$user1 = $_SESSION['user_id'];

$updategames = mysqli_query($link, "SELECT * FROM userhavegames WHERE user_id='$user1'");
$updategmfet = mysqli_fetch_array($updategames,MYSQLI_ASSOC);

$updatewantgames = mysqli_query($link, "SELECT * FROM userwantgames WHERE user_id='$user1'");
$updatewantgmfet = mysqli_fetch_array($updatewantgames,MYSQLI_ASSOC);

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

	if(isset($_POST['gameadd'])) { // add games

		include 'nav.php';

		$imppost = implode(',', $_POST['gameadd']); 



		if(empty($updategmfet['games'])) {
			$combpost = $imppost;
		} else {
			$combpost = $imppost.','.$updategmfet['games'];
		}

		$updategameul = "UPDATE userhavegames SET games='".$combpost."' WHERE user_id='$user1'";
		$udategamesulqy = mysqli_query($link, $updategameul);

		unset($_POST['gameadd']);
		
		mysqli_free_result($updategames);
		mysqli_free_result($updategamesulqy);

		echo '<div class="col-sm-8 col-sm-offset-2 centerblock text-center"><img width="100%" src="images/gamecycler-cycle.gif"><br />Updating Your Library...
		</div>';

		?>
    	<meta http-equiv="refresh" content="0;library.php">
    	<?php

	}

	elseif(isset($_POST['gamewantadd'])) { // add games

		include 'nav.php';

		$imppost = implode(',', $_POST['gamewantadd']);

		if(empty($updatewantgmfet['games'])) {
			$combpost = $imppost;
		} else {
			$combpost = $imppost.','.$updatewantgmfet['games'];
		}

		$updategameul = "UPDATE userwantgames SET games='".$combpost."' WHERE user_id='$user1'";
		$udategamesulqy = mysqli_query($link, $updategameul);

		unset($_POST['gamewantadd']);
		
		mysqli_free_result($updatewantgames);
		mysqli_free_result($updategamesulqy);

		echo '<div class="col-sm-8 col-sm-offset-2 centerblock text-center"><img width="100%" src="images/gamecycler-cycle.gif"><br />Updating Your Library...
		</div>';

		?>
    	<meta http-equiv="refresh" content="0;library.php">
    	<?php

	}
	elseif(isset($_POST['gamerem'])) 
	{

		// remove games

		include 'nav.php';

		// print_r($_POST);

		$upgmexp = explode(',',$updategmfet['games']);

		$upgmdiff = array_diff($upgmexp, $_POST['gamerem']);

		$upgmimp = implode(',', $upgmdiff);

		$updategameul = "UPDATE userhavegames SET games='".$upgmimp."' WHERE user_id='$user1'";
		$updategameulqy = mysqli_query($link, $updategameul);

		unset($_POST['gamerem']);

		mysqli_free_result($updategames);
		mysqli_free_result($updategameulqy);

		echo '<div class="col-sm-8 col-sm-offset-2 centerblock text-center"><img width="100%" src="images/gamecycler-cycle.gif"><br />Updating Your Library...
		</div>';

		?>
    	<meta http-equiv="refresh" content="0;library.php">
    	<?php


	} // if post isset

	elseif(isset($_POST['gamewantrem'])) 
	{

		// remove games

		include 'nav.php';

		// print_r($_POST);

		$upgmexp = explode(',',$updatewantgmfet['games']);

		$upgmdiff = array_diff($upgmexp, $_POST['gamerem']);

		$upgmimp = implode(',', $upgmdiff);

		print_r($upgmexp); echo "<br />";
		print_r($upgmdiff); echo "<br />";
		print_r($upgmimp);echo "<br />";

		$updategameul = "UPDATE userwantgames SET games='".$upgmimp."' WHERE user_id='$user1'";
		$updategameulqy = mysqli_query($link, $updategameul);

		unset($_POST['gamewantrem']);

		mysqli_free_result($updatewantgames);
		mysqli_free_result($updategameulqy);

		echo '<div class="col-sm-8 col-sm-offset-2 centerblock text-center"><img width="100%" src="images/gamecycler-cycle.gif"><br />Updating Your Library...
		</div>';

		?>
    	<!-- <meta http-equiv="refresh" content="0;library.php"> -->
    	<?php


	} // if post isset

	elseif(isset($_GET['editlibrary'])) {

		##########################

		    // REMOVE GAMES

		##########################

		// list to select games to remove

		include 'nav.php'; 

		echo '<div class="loader"></div>';

		$user1 = $_SESSION['user_id'];

		$games = "SELECT * FROM db_games";
		$gamesqy = mysqli_query($link, $games);

		while($gamesfet = mysqli_fetch_array($gamesqy)) {
			$gamesid = $gamesfet['game_id'];
			$gamesarr[$gamesid] = $gamesfet;
		}

		$usergames = "SELECT * FROM userhavegames WHERE user_id='$user1'";
		$usergamesqy = mysqli_query($link, $usergames);

		$usergamesarr = mysqli_fetch_array($usergamesqy);

		$usergameexp = explode(',',$usergamesarr['games']);

		echo '	<div class="col-sm-12"><h1 style="margin-top: 0px; padding-left: 20px; font-size: 2.4em;"><strong>LIBRARY</strong></h1></div>
				<div class="col-sm-12 gameshead lib"><h2><strong>REMOVE GAMES FROM LIBRARY</strong></h2></div>
					<form action="library.php" method="post">';
		if($usergameexp[0] == NULL) {
			echo "<div class='col-sm-12'><h2>YOUR LIBRARY IS EMPTY</h2></div>";
		} else {

		foreach($usergameexp as $gameid) {
			echo "<div class='col-sm-4 gmtile'>
					<!-- <a href='gamedetail.php?game_id=".$gameid."'> -->
							<div class='row'>
								<div class='col-sm-12 gmimage'>";
							
						if(empty($gamesarr[$gameid]['image'])) {
							echo '<img src="images/g-logo-big.gif">';
						} 
						else {
							echo '<img src="showfile.php?game_id='.$gameid.'">';
						}
							
						echo "	</div> <!-- gmimage -->
								<div class='row col-sm-12 gminfo'>
									<div class='col-sm-12 text-center gmtitle'>";
										print_r($gamesarr[$gameid]['title']);
						echo "		</div> <!-- title -->
									<div class='col-sm-6 gmconsole'>";
										print_r($gamesarr[$gameid]['console']);
						echo "		</div> <!-- console -->
									<div class='col-sm-6 text-right gmyear'>";									
										print_r($gamesarr[$gameid]['yearreleased']);
						echo "		</div> <!-- yearreleased -->";

				echo "	
						</div> <!-- gminfo row -->
						<div class='col-sm-12 text-center'><input type='checkbox' name='gamerem[]' value='".$gameid."'></div>
						</div> <!-- main row -->
						<!-- </a> -->
						
					</div> <!-- main div -->
					";

			}

			}

			echo '<div class="col-sm-12"><input type="submit" value="Remove Games"></div>
				</div>
			';

			#########################

				// ADD GAMES

			#########################


			$gamesdb = "SELECT * FROM db_games ORDER BY console ASC, title ASC";
    // $gamesdbqy = mysqli_query($link, $gamesdb);

    $distinctconsole = "SELECT DISTINCT console FROM db_games ORDER BY console ASC";
    $distinctconqy = mysqli_query($link, $distinctconsole);

	$consoledb = "SELECT * FROM consolekey";
 	$consoledbqy = mysqli_query($link, $consoledb);

 	while($consoledbfet = mysqli_fetch_array($consoledbqy)) {
 		$consolearr[] = $consoledbfet;
 	}

 	$alphaarray = array('0',
 		'1','2','3','4','5','6','7','8','9',
 		'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
 	

?>

	<style>
	div.console {
	    display:none;}


	</style>

	<script type="text/javascript">
	function call(){
		var consoleclass = document.getElementsByClassName('console');
	    for (var i = 0; i < consoleclass.length; i++) {
	    	consoleclass[i].style.display='none';
	    }
	    var response = document.getElementById('Select').value;
	    document.getElementById(response).style.display='block';

	}
	</script>


<!-- THIS IS THE STRUCTURE FOR THE JS 
	<select name="Select" id="Select" onChange="call()">
	<option></option>
	<option>A</option>
	<option>B</option>
	<option>C</option>
	</select>
	<div class="console" id="A">Content for  id "1" Goes Here</div>
	<div class="console" id="B">Content for  id "2" Goes Here</div>
	<div class="console" id="C">Content for  id "3" Goes Here</div>

	-->

		<div class="col-sm-12 gameshead libadd"><h2><strong>ADD GAMES TO LIBRARY</strong></h2></div>
		<div class="col-sm-6" style="padding: 20px 0;">
			<form action="library.php" id="addgames" method="post">

			<select name="Select" id="Select" onChange="call()">
				<option></option>
		<?php 
			while($distconarr = mysqli_fetch_array($distinctconqy)) {
				$gmcon = $distconarr['console'];
				echo "<option>".$gmcon."</option>";
				$gmconarr[] = $distconarr;
			}

			echo "</select>
		</div><!-- col-sm-6 select options -->";

		echo '<div class="col-sm-6 text-right" style="padding: 20px 40px 20px 0;">
					<input type="submit" value="Add Games">
				</div>';

			
			foreach($gmconarr as $conkey => $connameinfo) {
				$gmcon1 = $connameinfo['console'];
			
				$gamesdb = "SELECT * FROM db_games WHERE console='".$gmcon1."' ORDER BY console ASC, title ASC";
				$gamesdbqy = mysqli_query($link, $gamesdb);
					
				// Set div to match game from drop down
	echo '	<div class="col-sm-12 console" id="'.$gmcon1.'">
				<div class="row">';

				echo '<div id="topgo"><a href="#top"><i class="fa fa-chevron-circle-up" style="margin-right:5px" aria-hidden="true"></i>Top</a></div>';
				echo '<div id="alphamenu" class="col-sm-8 col-sm-offset-2 text-center" align="center"><strong>Jump To A Letter</strong><br />';
						

					$alphanavmenarr = array('0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
					foreach($alphanavmenarr as $alpha) {
						echo "<div class='alphamenuit'><a href='#".$gmcon1.$alpha."'>".$alpha."</a></div>";
					}

					echo '</div>';

					echo '</div><!-- <a name="top"></a> -->';
				// establish qualifying if to display game IF matches $gmcon, go through each game in db_games
				foreach($alphaarray as $alpha) {
					$gamesdbqy = mysqli_query($link, $gamesdb);
					echo "<a name='".$gmcon1.$alpha."'></a>";
						while($gamestable = mysqli_fetch_assoc($gamesdbqy)) {
							$firstlt = strtolower(substr($gamestable['title'],0,1));
							if($gamestable['console'] == $gmcon1 && $firstlt == $alpha) {
					echo "	<div class='col-sm-4 gmtile lib'>
								<!-- <a href='gamedetail.php?game_id=".$gameid."'> -->
									<div class='row'>
										<div class='col-sm-12 gmimage'>";
																
											// image
											if(empty($gamestable['image'])) {
												echo '<img src="images/g-logo-big.gif">';
											} 
											else {
												echo '<img src="showfile.php?game_id='.$gamestable['game_id'].'">';
											}

										echo "	</div> <!-- gmimage -->
											<div class='row col-sm-12 gminfo'>
												<div class='col-sm-12 text-center gmtitle'>";
													print_r($gamestable['title']);

									echo "		</div> <!-- title -->
												<div class='col-sm-6 gmconsole'>";
													print_r($gamestable['console']);

									echo "		</div> <!-- console -->
												<div class='col-sm-6 text-right gmyear'>";									
													print_r($gamestable['yearreleased']);

									echo "		</div> <!-- yearreleased -->";

									echo "		</div> <!-- gminfo row -->
												<div class='col-sm-12 text-center'><input type='checkbox' name='gameadd[]' value='".$gamestable['game_id']."'></div>
												</div> <!-- main row -->
												</a>
											
										</div> <!-- main div -->
										

								";


						} // If gamestable = gmcon1 

					} // While gamestable 
			}  // Foreach Alpha

			
				
			
	echo '	<!-- </div>  row -->
		</div> <!-- end div for section by console -->';
			}
		// }

		
	echo '		</form>';
			
	echo '</div><!-- col-sm-12 sec div -->';




	} 

	elseif(isset($_GET['editwants'])) {

		##########################

		    // REMOVE WANTS GAMES

		##########################

		// list to select games to remove

		include 'nav.php'; 

		echo '<div class="loader"></div>';

		$user1 = $_SESSION['user_id'];

		$games = "SELECT * FROM db_games";
		$gamesqy = mysqli_query($link, $games);

		while($gamesfet = mysqli_fetch_array($gamesqy)) {
			$gamesid = $gamesfet['game_id'];
			$gamesarr[$gamesid] = $gamesfet;
		}

		$usergames = "SELECT * FROM userwantgames WHERE user_id='$user1'";
		$usergamesqy = mysqli_query($link, $usergames);

		$usergamesarr = mysqli_fetch_array($usergamesqy);

		$usergameexp = explode(',',$usergamesarr['games']);

		echo '	<div class="col-sm-12"><h1 style="margin-top: 0px; padding-left: 20px; font-size: 2.4em;"><strong>WANT GAMES</strong></h1></div>
				<div class="col-sm-12 gameshead lib"><h2><strong>REMOVE GAMES FROM WANTS</strong></h2></div>
					<form action="library.php" method="post">';
		foreach($usergameexp as $gameid) {
			echo "<div class='col-sm-4 gmtile'>
					<!-- <a href='gamedetail.php?game_id=".$gameid."'> -->
							<div class='row'>
								<div class='col-sm-12 gmimage'>";
							
						if(empty($gamesarr[$gameid]['image'])) {
							echo '<img src="images/g-logo-big.gif">';
						} 
						else {
							echo '<img src="showfile.php?game_id='.$gameid.'">';
						}
							
						echo "	</div> <!-- gmimage -->
								<div class='row col-sm-12 gminfo'>
									<div class='col-sm-12 text-center gmtitle'>";
										print_r($gamesarr[$gameid]['title']);
						echo "		</div> <!-- title -->
									<div class='col-sm-6 gmconsole'>";
										print_r($gamesarr[$gameid]['console']);
						echo "		</div> <!-- console -->
									<div class='col-sm-6 text-right gmyear'>";									
										print_r($gamesarr[$gameid]['yearreleased']);
						echo "		</div> <!-- yearreleased -->";

				echo "	
						</div> <!-- gminfo row -->
						<div class='col-sm-12 text-center'><input type='checkbox' name='gamewantrem[]' value='".$gameid."'></div>
						</div> <!-- main row -->
						<!-- </a> -->
						
					</div> <!-- main div -->
					";

			}

			echo '<div class="col-sm-12"><input type="submit" value="Remove Games"></div>
				</div>
			';

			#########################

				// ADD WANT GAMES

			#########################


			$gamesdb = "SELECT * FROM db_games ORDER BY console ASC, title ASC";
    // $gamesdbqy = mysqli_query($link, $gamesdb);

    $distinctconsole = "SELECT DISTINCT console FROM db_games ORDER BY console ASC";
    $distinctconqy = mysqli_query($link, $distinctconsole);

	$consoledb = "SELECT * FROM consolekey";
 	$consoledbqy = mysqli_query($link, $consoledb);

 	while($consoledbfet = mysqli_fetch_array($consoledbqy)) {
 		$consolearr[] = $consoledbfet;
 	}

 	$alphaarray = array('0',
 		'1','2','3','4','5','6','7','8','9',
 		'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
 	

?>

	<style>
	div.console {
	    display:none;}


	</style>

	<script type="text/javascript">
	function call(){
		var consoleclass = document.getElementsByClassName('console');
	    for (var i = 0; i < consoleclass.length; i++) {
	    	consoleclass[i].style.display='none';
	    }
	    var response = document.getElementById('Select').value;
	    document.getElementById(response).style.display='block';

	}
	</script>

	<div class="loader"></div>

<!-- THIS IS THE STRUCTURE FOR THE JS 
	<select name="Select" id="Select" onChange="call()">
	<option></option>
	<option>A</option>
	<option>B</option>
	<option>C</option>
	</select>
	<div class="console" id="A">Content for  id "1" Goes Here</div>
	<div class="console" id="B">Content for  id "2" Goes Here</div>
	<div class="console" id="C">Content for  id "3" Goes Here</div>

	-->

		<div class="col-sm-12 gameshead libadd"><h2><strong>ADD GAMES TO WANTS</strong></h2></div>
		<div class="col-sm-6" style="padding: 20px 0;">
			<form action="library.php" id="addgames" method="post">

			<select name="Select" id="Select" onChange="call()">
				<option></option>
		<?php 
			while($distconarr = mysqli_fetch_array($distinctconqy)) {
				$gmcon = $distconarr['console'];
				echo "<option>".$gmcon."</option>";
				$gmconarr[] = $distconarr;
			}

			echo "</select>
		</div><!-- col-sm-6 select options -->";

		echo '<div class="col-sm-6 text-right" style="padding: 20px 40px 20px 0;">
					<input type="submit" value="Add Games">
				</div>';

			
			foreach($gmconarr as $conkey => $connameinfo) {
				$gmcon1 = $connameinfo['console'];
			
				$gamesdb = "SELECT * FROM db_games WHERE console='".$gmcon1."' ORDER BY console ASC, title ASC";
				$gamesdbqy = mysqli_query($link, $gamesdb);
					
				// Set div to match game from drop down
	echo '	<div class="col-sm-12 console" id="'.$gmcon1.'">
				<div class="row">';

				echo '<div id="topgo"><a href="#top"><i class="fa fa-chevron-circle-up" style="margin-right:5px" aria-hidden="true"></i>Top</a></div>';
				echo '<div id="alphamenu" class="col-sm-8 col-sm-offset-2 text-center" align="center"><strong>Jump To A Letter</strong><br />';
						

					$alphanavmenarr = array('0','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z');
					foreach($alphanavmenarr as $alpha) {
						echo "<div class='alphamenuit'><a href='#".$gmcon1.$alpha."'>".$alpha."</a></div>";
					}

					echo '</div>';

					echo '</div><!-- <a name="top"></a> -->';
				// establish qualifying if to display game IF matches $gmcon, go through each game in db_games
				foreach($alphaarray as $alpha) {
					$gamesdbqy = mysqli_query($link, $gamesdb);
					echo "<a name='".$gmcon1.$alpha."'></a>";
						while($gamestable = mysqli_fetch_assoc($gamesdbqy)) {
							$firstlt = strtolower(substr($gamestable['title'],0,1));
							if($gamestable['console'] == $gmcon1 && $firstlt == $alpha) {
					echo "	<div class='col-sm-4 gmtile lib'>
								<!-- <a href='gamedetail.php?game_id=".$gameid."'> -->
									<div class='row'>
										<div class='col-sm-12 gmimage'>";
																
											// image
											if(empty($gamestable['image'])) {
												echo '<img src="images/g-logo-big.gif">';
											} 
											else {
												echo '<img src="showfile.php?game_id='.$gamestable['game_id'].'">';
											}

										echo "	</div> <!-- gmimage -->
											<div class='row col-sm-12 gminfo'>
												<div class='col-sm-12 text-center gmtitle'>";
													print_r($gamestable['title']);

									echo "		</div> <!-- title -->
												<div class='col-sm-6 gmconsole'>";
													print_r($gamestable['console']);

									echo "		</div> <!-- console -->
												<div class='col-sm-6 text-right gmyear'>";									
													print_r($gamestable['yearreleased']);

									echo "		</div> <!-- yearreleased -->";

									echo "		</div> <!-- gminfo row -->
												<div class='col-sm-12 text-center'><input type='checkbox' name='gamewantadd[]' value='".$gamestable['game_id']."'></div>
												</div> <!-- main row -->
												</a>
											
										</div> <!-- main div -->
										

								";


						} // If gamestable = gmcon1 

					} // While gamestable 
			}  // Foreach Alpha

			
				
			
	echo '	<!-- </div>  row -->
		</div> <!-- end div for section by console -->';
			}
		// }

		
	echo '		</form>';
			
	echo '</div><!-- col-sm-12 sec div -->';




	}

	else { // empty get main screen


		include 'nav.php'; 

		$user1 = $_SESSION['user_id'];

		$games = "SELECT * FROM db_games";
		$gamesqy = mysqli_query($link, $games);

		while($gamesfet = mysqli_fetch_array($gamesqy)) {
			$gamesid = $gamesfet['game_id'];
			$gamesarr[$gamesid] = $gamesfet;
		}

		$usergames = "SELECT * FROM userhavegames WHERE user_id='$user1'";
		$usergamesqy = mysqli_query($link, $usergames);

		$usergamesarr = mysqli_fetch_array($usergamesqy);

		$usergameexp = explode(',',$usergamesarr['games']);

		$userwantgames = "SELECT * FROM userwantgames WHERE user_id='$user1'";
		$userwantgamesqy = mysqli_query($link, $userwantgames);

		$userwantgamesarr = mysqli_fetch_array($userwantgamesqy);

		$userwantgameexp = explode(',',$userwantgamesarr['games']);	

		?>
		<script>
			$('#myTabs a').click(function (e) {
			  e.preventDefault()
			  $(this).tab('show')
			})
		</script>

		<?php	

		echo '	

		<!-- <div class="col-sm-12 reviewhead"><h2><strong>LIBRARY</strong></h2></div> -->

		<div class="container">
			
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#havegames" aria-controls="havegames" role="tab" data-toggle="tab">Library</a></li>
					<li role="presentation"><a href="#wantgames" aria-controls="wantgames" role="tab" data-toggle="tab">Wants</a></li>
				</ul>
			    	
			    	<div class="row tab-content">
						<div role="tabpanel" class="tab-pane fade in active" id="havegames">

							<div class="col-sm-12 lib gameshead">
								<h2><strong>LIBRARY</strong></h2>
							</div>
							<div class="col-sm-12">
								<form action="library.php" method="get" style="margin-bottom: 15px;">
									<input type="submit" name="editlibrary" value="Edit Library">
								</form>
							</div> <!-- edit prof bttn -->';

		foreach($usergameexp as $gameid) {
			if($gameid[0] == NULL) {
					echo "<div class='col-sm-12'><h3><strong>YOUR LIBRARY IS EMPTY</strong></h3></div>";
				} else {
			echo "			<div class='col-sm-4 gmtile'>
								<a href='gamedetail.php?game_id=".$gameid."'>
								<div class='row'>
									<div class='col-sm-12 gmimage'>";
							
						if(empty($gamesarr[$gameid]['image'])) {
							echo '		<img src="images/g-logo-big.gif">';
						} 
						else {
							echo '		<img src="showfile.php?game_id='.$gameid.'">';
						}
							
						echo "		</div> <!-- gmimage -->
									<div class='row col-sm-12 gminfo'>
										<div class='col-sm-12 text-center gmtitle'>";
											print_r($gamesarr[$gameid]['title']);
						echo "			</div> <!-- title -->
										<div class='col-sm-6 gmconsole'>";
											print_r($gamesarr[$gameid]['console']);
						echo "			</div> <!-- console -->
										<div class='col-sm-6 text-right gmyear'>";									
											print_r($gamesarr[$gameid]['yearreleased']);
						echo "			</div> <!-- yearreleased -->";

				echo "				</div> <!-- gminfo row -->
								</div> <!-- main row -->
								</a>
							</div> <!-- main div -->";
					
					} // if game1 null
					
		} // foreach usergamexp

		echo '			</div> <!-- tab divs -->
					';

			echo '	
						<div role="tabpanel" class="tab-pane fade in" id="wantgames">

							<div class="col-sm-12 lib gameshead">
								<h2><strong>WANTS</strong></h2>
							</div>
							<div class="col-sm-12">
								<form action="library.php" method="get" style="margin-bottom: 15px;">
									<input type="submit" name="editwants" value="Edit Wants">
								</form>
							</div> <!-- edit prof bttn -->';

		foreach($userwantgameexp as $gameid1) {
				if($gameid1 == NULL) {
					echo "<div class='col-sm-12'><h3><strong>There Are No Games You Want, Please Add To Your Library</h3></div>";
				} else {

			echo "			<div class='col-sm-4 gmtile'>
								<a href='gamedetail.php?game_id=".$gameid1."'>
								<div class='row'>
									<div class='col-sm-12 gmimage'>";
							
						if(empty($gamesarr[$gameid1]['image'])) {
							echo '		<img src="images/g-logo-big.gif">';
						} 
						else {
							echo '		<img src="showfile.php?game_id='.$gameid1.'">';
						}
							
						echo "		</div> <!-- gmimage -->
									<div class='row col-sm-12 gminfo'>
										<div class='col-sm-12 text-center gmtitle'>";
											print_r($gamesarr[$gameid1]['title']);
						echo "			</div> <!-- title -->
										<div class='col-sm-6 gmconsole'>";
											print_r($gamesarr[$gameid1]['console']);
						echo "			</div> <!-- console -->
										<div class='col-sm-6 text-right gmyear'>";									
											print_r($gamesarr[$gameid1]['yearreleased']);
						echo "			</div> <!-- yearreleased -->";

				echo "				</div> <!-- gminfo row -->
								</div> <!-- main row -->
								</a>
							</div> <!-- main div -->";

						}
				}

				echo '	</div>
					</div>';

	} // else if not edit

	mysqli_free_result($usergamesqy);
	

echo '			
			
		</div>
	</div>
	</div>
	</div>
	</div> <!-- container -->
	';


}
else
{
	?>
    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
    Please Log in!
    <?php
}




?>

<?php include 'footer.php'; ?>