<?php include 'base.php';

$title = "Gamecycler | Send Money to a User";
$description = "After accepting an offer where you owe money, this is how you send them the money you promised.";
$keywords = "gamers, games, xbox, trading games, playstation, nintendo, atari";

include 'header.php'; ?>

<body>

<?php

\Stripe\Stripe::setApiKey(PLATFORM_SECRET_KEY);
$account = \Stripe\Account::retrieve({CONNECTED_STRIPE_ACCOUNT_ID});
$account->tos_acceptance->date = time();
// Assumes you're not using a proxy
$account->tos_acceptance->ip = $_SERVER['REMOTE_ADDR'];
$account->save();


?>


<? include 'footer.php'; ?>