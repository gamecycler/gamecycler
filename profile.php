<?php include 'base.php'; 

$title = "Gamecycler | User Profile";
$description = "User profile of a Gamecycler User";
$keywords = "video games, nintendo, playstation, xbox, xbox one, xbox360, atari, sega, genesis, gamers, trade games";

include 'header.php';

?>

<body">

<?php

$user1 = $_SESSION['user_id'];

if(!empty($_SESSION['LoggedIn']) && !empty($_SESSION['Username'])) {

    if(!empty($_POST['Update_Info'])) {

        $username = mysqli_escape_string($link, $_POST['username']);
        $firstname = mysqli_escape_string($link, $_POST['firstname']);
        $lastname = mysqli_escape_string($link, $_POST['lastname']);
        $email = mysqli_escape_string($link, $_POST['email']);
        $street_add = mysqli_escape_string($link, $_POST['street_add']);
        $street_add2 = mysqli_escape_string($link, $_POST['street_add2']);
        $city = mysqli_escape_string($link, $_POST['city']);
        $state = mysqli_escape_string($link, $_POST['state']);
        $zipcode = mysqli_escape_string($link, $_POST['zipcode']);

        $userud = "UPDATE db_users SET firstname='".$firstname."',lastname='".$lastname."',username='".$username."',street_add='".$street_add."',street_add2='".$street_add2."',city='".$city."',state='".$state."',zipcode='".$zipcode."',email='".$email."' WHERE user_id='".$user1."'";
        if(mysqli_query($link, $userud)) {
            
        } else {
            echo "Houston, we have a problem.";
            printf("Error: %s\n", mysqli_error($link));
        }

    echo '<meta http-equiv="refresh" content="0;profile.php?id='.$user1.'">';

    }

    elseif(isset($_GET['edit'])) {

        include 'nav.php';

        

        // print_r($user1);

        $userinfo = 'SELECT * FROM db_users WHERE user_id="'.$user1.'"';
        $userinfoqy = mysqli_query($link, $userinfo);

        $userdata = mysqli_fetch_array($userinfoqy);

        echo '<div class="col-sm-12 gameshead">
                <h2>EDIT YOUR PROFILE</h2> 
            </div>
            ';

        echo '<div class="col-sm-12" id="profupdate">
                <form action="profile.php" method="post" id="profupform">
                    <label for="username">Username: </label><input type="text" name="username" id="username" value="'.$userdata['username'].'" required><br />
                    <label for="firstname">First: </label><input type="text" name="firstname" id="firstname" value="'.$userdata['firstname'].'" required><br />
                    <label for="lastname">Last: </label><input type="text" name="lastname" id="lastname" value="'.$userdata['lastname'].'" required><br />
                    <label for="email">Email: </label><input type="email" name="email" id="email" value="'.$userdata['email'].'" required><br />
                    <label for="street_add">Address: </label><input type="text" name="street_add" id="street_add" value="'.$userdata['street_add'].'" required><br />
                    <label for="street_add2">Apt/Suite/#: </label><input type="text" name="street_add2" id="street_add2" value="'.$userdata['street_add2'].'"><br />
                    <label for="city">City: </label><input type="text" name="city" id="city" value="'.$userdata['city'].'" required><br />
                    <label for="state">State<small>(use state abbreviation)</small>:</label><input type="text" name="state" maxlength="2" id="state" value="'.$userdata['state'].'" required><br />
                    <label for="zipcode">Zipcode:</label><input type="text" name="zipcode" maxlength="5" id="zipcode" value="'.$userdata['zipcode'].'" required><br />

                    <input type="submit" name="Update Info">

                </form>
                
                    <a href="profile.php?id='.$user1.'""><i class="glyphicon glyphicon-backward" style="margin-right: 5px; margin: 20px 7px 7px 0;"></i>Return to Profile</a>
                
        
                </div>
            </div>
        </div>
    </div>
    </div>';

    mysqli_free_result($userinfoqy);

    }
    else
    {

        include 'nav.php';

        $user1 = $_SESSION['user_id'];
        $profid = $_GET['id'];

        $userprofile = 'SELECT db_users.*, u1want.user_id as u1wantid, u1want.games as u1wgm, u2want.user_id as u2wantid, u2want.games as u2wgm, u1have.user_id as u1haveid, u1have.games as u1hgm, u2have.user_id as u2haveid, u2have.games as u2hgm FROM db_users, userwantgames as u1want, userwantgames as u2want, userhavegames as u1have, userhavegames as u2have WHERE db_users.user_id="'.$profid.'" AND u1want.user_id="'.$user1.'" AND u2have.user_id="'.$profid.'" AND u2want.user_id="'.$profid.'" AND u1have.user_id="'.$user1.'"';
        $userprofqy = mysqli_query($link, $userprofile);

        $review = 'SELECT a.*, b.revsum, b.revcount FROM userreviews a INNER JOIN (SELECT SUM(rating) as revsum, COUNT(rating) as revcount FROM userreviews WHERE reviewee="'.$profid.'") b WHERE reviewee="'.$profid.'"';
        $reviewqy = mysqli_query($link, $review);

        while($reviewfet = mysqli_fetch_array($reviewqy)) {
            $reviewarr[] = $reviewfet;
        }

        $games = 'SELECT * FROM db_games';
        $gamesqy = mysqli_query($link, $games);

        $users = 'SELECT username FROM db_users';
        $usersqy = mysqli_query($link, $users);

        while($userfet = mysqli_fetch_array($usersqy)) {
            $usersarr[] = $userfet;
        }

        while($gamesarr = mysqli_fetch_array($gamesqy)) {
            $gamesarrid = $gamesarr['game_id'];
            $gamestable[$gamesarrid] = $gamesarr;
        }

        echo 
        // '<div class="container">
        //         <div class="row">
                    '<div class="col-sm-12" style="margin-bottom: 15px;"><a href="users.php"><span class="glyphicon glyphicon-backward"></span> Return to All Users</a></div>';

        while($userprofarr = mysqli_fetch_array($userprofqy)) {
            // GRAVATAR CODE
            $default1 = "http://ricdelgado.com/wp-content/uploads/2015/g-logo-big.gif";
            $size1 = 300;
            $email1 = $userprofarr['email'];

            $grav_url1 = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $email1 ) ) ) . "?d=" . urlencode( $default1 ) . "&s=" . $size1;

            if($user1 == $profid) {
            echo '  <div class="col-sm-12">
                    <form action="profile.php" method="get">
                        <input type="checkbox" value="yes" name="edit" hidden checked>
                        <input type="submit" value="Edit Profile">
                    </form>
                    </div>';
            }

            echo '  <div class="col-sm-4" id="profimg" align="right">
                        <img src="'. $grav_url1 .'">
                    </div><!-- col-sm-4 image -->
                    <div class="col-sm-8">
                        <div class="row">';

            print_r(
                    '<div class="col-sm-12 gameshead profhead">
                        <div class="row">
                            <div class="col-sm-6 profun">
                                '.$userprofarr['username']);
                                

            echo '          </div>
                        <div class="col-sm-3" id="profsub">';

                               


            print_r('   </div>
                            <div class="col-sm-3 text-right profcity">
                                '.$userprofarr["city"].', '.$userprofarr["state"].'
                            </div>
                        </div>
                    </div>'
                );


            echo "  <div class='col-sm-12' id='gamematchprof'> <!-- match game section -->
                        <div class='row'> <!-- match game section row -->";

                        // print_r($userprofarr);

            $user1want = explode( ',',$userprofarr['u1wgm']);
            $user2have = explode( ',',$userprofarr['u2hgm']);

            $u1wantu2havecomp = array_intersect($user1want, $user2have);
            $u1wu2himp = implode(',', $u1wantu2havecomp);

            $user1have = explode( ',',$userprofarr['u1hgm']);
            $user2want = explode( ',',$userprofarr['u2wgm']);

            $u1haveu2wantcomp = array_intersect($user1have, $user2want);
            $u1hu2wimp = implode(',', $u1haveu2wantcomp);

            echo "          <div class='col-sm-5 text-center'> <!-- u1w to u2h section -->";
            echo "              <form action='comparegames.php' method='post'>";
            echo "              <h3><strong>YOU WANT</strong></h3>";

            echo '<input id="profcheck" type="checkbox" name="game" value="'.$profid.';'.$u1wu2himp.';'.$u1hu2wimp.'" checked>';
            if(array_intersect($user1want, $user2have) != NULL) {
                foreach($u1wantu2havecomp as $key => $comp) {
                    $comp = $comp - 1;
                    print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$comp]['title'].' - '.$gamestable[$comp]['console'].' | <span style="font-size: 0.7em;">'.$gamestable[$comp]['loose'].'</span><br />');
                }
            } else {
                echo "No Matches";
            }

            echo "          </div> <!-- u1w to u2h section -->
                            <div class='col-sm-2 text-center' style='padding-top: 30px;'>
                                <strong>FOR</strong>
                            </div>
                            <div class='col-sm-5 text-center'> <!-- u2w to u1h -->";

            echo "              <h3><strong>".$userprofarr['username']." WANTS</strong></h3>";

            if($u1haveu2wantcomp != NULL) {
                foreach($u1haveu2wantcomp as $key => $comp) {
                    print_r('<i class="fa fa-arrow-circle-right" style="padding-right: 4px;"></i>'.$gamestable[$comp]['title'].' - '.$gamestable[$comp]['console'].' | <span style="font-size: 0.7em;">'.$gamestable[$comp]['loose'].'</span><br />');
                }
            } else {
                echo "No Matches";
            }

            echo "          </div> <!-- u2w to u1h -->";

            if($u1haveu2wantcomp == NULL && $u1wantu2havecomp == NULL ) {
                echo "";
            } else {
                echo "<div class='col-sm-12 text-center'><input type='submit' value='Initiate Trade'></div>";
            }

            echo '      </form>

                            
                        </div> <!-- match game row -->
                    </div> <!-- match game section -->


                    </div> <!-- row gm info -->
                </div> <!-- col-sm-6 gm info -->
                <div class="clearfix"></div>
                <div class="col-sm-12" id="revprofsec">';
                        // REVIEW SECTION
            ?>              <table class="table">
                        <tbody>
                            <thead>Overall Review: 
                    <?php

                            // print_r($reviewarr);

                            $revsum = $reviewarr[0]['revsum'];
                            $revcount = $reviewarr[0]['revcount'];

                            if($revcount == 0) {
                                $revcount = 0;
                            } else {
                                $reviewtot = round(($revsum/$revcount), 0, PHP_ROUND_HALF_UP);
                            }


                            if($reviewtot == 1) {
                                echo "<span class='glyphicon glyphicon-star'></span>";
                            } elseif($reviewtot == 2 ) {
                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
                            } elseif($reviewtot == 3 ) {
                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>";
                            } elseif($reviewtot == 4 ) {
                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
                            } elseif($reviewtot == 5 ) {   
                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
                            } else {
                                echo "<i>No Ratings Yet.</i>";
                            }
                    ?>

                            </thead>
                            <tr><th>Reviewer</th><th>Rating</th><th>Date</th><th>Review</th></tr>
                    <?php
                        if(count($reviewarr) > 0) {
                            foreach($reviewarr as $revkey => $revdata) {
                                echo "<tr>
                                        <td>";
                                            $revreviewer = $revdata['reviewer'];
                                            print_r('<a href="profile.php?id='.$revreviewer.'">'.$usersarr[$revreviewer]['username'].'</a>');

                                echo "  </td> <!-- reviewer -->
                                        <td>";

                                            $revdatarat = $revdata['rating'];
                                            if($revdatarat == 1) {
                                                echo "<span class='glyphicon glyphicon-star'></span>";
                                            } elseif($revdatarat == 2 ) {
                                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
                                            } elseif($revdatarat == 3 ) {
                                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span><span class='glyphicon glyphicon-star'></span>";
                                            } elseif($revdatarat == 4 ) {
                                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
                                            } elseif($revdatarat == 5 ) {   
                                                echo "<span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span> <span class='glyphicon glyphicon-star'></span>";
                                            }

                                            

                                echo "  </td> <!-- rating -->
                                        <td>";

                                            print_r(date('m-d-Y',strtotime($revdata['timestamp'])));

                                echo "  </td> <!-- date -->
                                        <td>";

                                            print_r($revdata['review']);

                                echo "  </td> <!-- review -->
                                     </tr>";
                            }
                        }
                        else
                        {
                            echo "<tr><td colspan='4' align='center'>No Reviews Yet</td></tr>";
                        }


                    ?>
                        </tbody>
                        </table>

    <?php

            // USER PROFILE HAVE GAMES SECTION

            echo " <form action='comparegames.php' method='post'>
                  <div class='col-sm-12 gameshead'>
                        <h2>Library</h2>
                    </div>";
            $uprofhave = explode(',',$userprofarr['u2hgm']);

            

            if($uprofhave[0] == NULL) {
                echo "<h3><strong>NO GAMES IN LIBRARY</strong></h3>";
            } else {

            foreach($uprofhave as $upkey => $updata) {
                echo "<div class='col-sm-4 gmtile proflib'>
                <a href='gamedetail.php?game_id=".$updata."'>
                        <div class='row'>
                            <div class='col-sm-12 gmimage'>";
                        
                    if(empty($gamestable[$updata]['image'])) {
                        echo '<img src="images/g-logo-big.gif">';
                    } 
                    else {
                        echo '<img src="showfile.php?game_id='.$updata.'">';
                    }
                        
                    echo "  </div> <!-- gmimage -->
                            <div class='row col-sm-12 gminfo'>
                                <div class='col-sm-12 text-center gmtitle'>";
                                    print_r($gamestable[$updata]['title']);
                    echo "      </div> <!-- title -->
                                <div class='col-sm-6 gmconsole'>";
                                    print_r($gamestable[$updata]['console']);
                    echo "      </div> <!-- console -->
                                <div class='col-sm-6 text-right gmyear'>";                                  
                                    print_r($gamestable[$updata]['yearreleased']);
                    echo "      </div> <!-- yearreleased -->";

            echo "  </div> <!-- gminfo row -->
                    </div> <!-- main row -->
                    </a>";
                    // print_r('<input type="checkbox" name="'.$user1.'user'.$updata.'" value="'.$gamestable[$gameu1wu2h]['game_id'].'" rel="'.$gamevalue.'"> <span style="font-size: 1.2em; padding-left: 5px;"><strong>'.$gamestable[$gameu1wu2h]['title'].' | $'.$gamevalue.'</strong></span><br />');
            echo "</div> <!-- main div -->";

            }

        }


            //     echo "<div class='col-sm-4'>";
            //         print_r($gamestable[$updata]['title']);
            //     echo "</div>";
            // }

            // echo '  </div> <!-- col-sm-12 review section -->';
        
        } // while userprofarr

        
        mysqli_free_result($usersqy);
        mysqli_free_result($gamesqy);


        echo '  </div> <!-- row -->
            </div> <!-- container -->
            </div>
            </div>';


    } // if else for edit conditional



} else {
    ?>
    <meta http-equiv="refresh" content="0;login.php?ref=<?php echo $_SERVER['REQUEST_URI'] ?>">
    Please Log in!
    <?php
}

?>

<?php include "footer.php" ?>